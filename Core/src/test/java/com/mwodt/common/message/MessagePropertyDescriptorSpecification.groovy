package com.mwodt.common.message

import spock.lang.Specification
import atg.multisite.Site
import atg.repository.RepositoryItemImpl

class MessagePropertyDescriptorSpecification extends Specification {

    MessagePropertyDescriptor testObj

    RepositoryItemImpl repItemMock = Mock()
    Site siteMock = Mock()

    def messagesMap = [("siteId_" + Locale.default.toString()) : "siteId Message"]

    def "setup"() {
        testObj = Spy(MessagePropertyDescriptor)
    }

    def "case when locale and site is empty"() {
        given:
        repItemMock.getPropertyValue("defaultMessage") >> "Default"
        testObj.getLocale() >> null
        testObj.getCurrentSite() >> null
        when:
        def message = testObj.getPropertyValue(repItemMock, null)
        then:
        message == "Default"
    }

    def "case when locale and site are valid"() {
        given:
        repItemMock.getPropertyValue("defaultMessage") >> "Default"
        repItemMock.getPropertyValue("siteLocaleMessages") >> messagesMap
        testObj.getLocale() >> Locale.default
        testObj.getCurrentSite() >> siteMock
        siteMock.getId() >> "siteId"
        when:
        def message = testObj.getPropertyValue(repItemMock, null)
        then:
        message == "siteId Message"
    }

    def "case when locale and site are valid but no mapping for site"() {
        given:
        repItemMock.getPropertyValue("defaultMessage") >> "Default"
        repItemMock.getPropertyValue("siteLocaleMessages") >> messagesMap
        testObj.getLocale() >> Locale.default
        testObj.getCurrentSite() >> siteMock
        siteMock.getId() >> "siteId2"
        when:
        def message = testObj.getPropertyValue(repItemMock, null)
        then:
        message == "Default"
    }
}

