package com.mwodt.repository.descriptor

import spock.lang.Specification
import atg.repository.RepositoryItem
import atg.repository.RepositoryItemImpl

import com.mwodt.catalog.custom.MWODTCustomCatalogTools

class CategoryItemTypePropertyDescriptorSpecification extends Specification {

    CategoryItemTypePropertyDescriptor testObj

    RepositoryItemImpl repItemMock = Mock()

    RepositoryItem repItemMock1 = Mock()

    MWODTCustomCatalogTools catalogTools = Mock()

    def setup() {
        testObj = Spy(CategoryItemTypePropertyDescriptor)
        testObj.getCatalogTools() >> catalogTools
    }

    def "test if category is family"() {
        given:
        catalogTools.isParentRootCategory(repItemMock) >> false
        when:
        def result = testObj.getPropertyValue(repItemMock, null)
        then:
        result == "FAMILY"
    }

    def "test if category is subcategory"() {
        given:
        repItemMock.getPropertyValue("fixedParentCategories") >> ([repItemMock1] as Set)
        repItemMock.getPropertyValue("childCategories") >> [repItemMock1]
        catalogTools.isParentRootCategory(repItemMock) >> false
        when:
        def result = testObj.getPropertyValue(repItemMock, null)
        then:
        result == "SUBCATEGORY"
    }

    def "test if category is category"() {
        given:
        repItemMock.getPropertyValue("fixedParentCategories") >> null
        repItemMock.getPropertyValue("childCategories") >> [repItemMock1]
        catalogTools.isParentRootCategory(repItemMock) >> true
        when:
        def result = testObj.getPropertyValue(repItemMock, null)
        then:
        result == "CATEGORY"
    }
}
