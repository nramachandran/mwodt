/**
 * <p class="copyright">Copyright 2015 by McFadyen Consulting</p>
 *
 * <div class="vcard">
 *   <div class="fn org">McFadyen Consulting</div>
 *   <div class="adr">
 *     <div>
 *       <span class="locality">Vienna</span>,
 *       <abbr class="region" title="Virginia">VA</abbr> <span class="postal-code">22182</span>
 *     </div>
 *     <div class="country-name"><abbr title="United States of America">USA</abbr></div>
 *   </div>
 * </div>
 *
 * <p class="copyrightRights">All rights reserved.</p>
 *
 * <p class="legal">
 * This software is the confidential and proprietary information of McFadyen Consulting
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms specified by McFadyen Consulting.
 * </p>
 */
package com.mwodt.common.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * @author oracle
 *
 */
public class MessageContentDroplet extends DynamoServlet {
    @Override
    public void service(DynamoHttpServletRequest pReq, DynamoHttpServletResponse pRes) throws ServletException, IOException {
        // TODO Auto-generated method stub
        super.service(pReq, pRes);
    }
}

