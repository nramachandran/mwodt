/**
 * <p class="copyright">Copyright 2015 by McFadyen Consulting</p>
 *
 * <div class="vcard">
 *   <div class="fn org">McFadyen Consulting</div>
 *   <div class="adr">
 *     <div>
 *       <span class="locality">Vienna</span>,
 *       <abbr class="region" title="Virginia">VA</abbr> <span class="postal-code">22182</span>
 *     </div>
 *     <div class="country-name"><abbr title="United States of America">USA</abbr></div>
 *   </div>
 * </div>
 *
 * <p class="copyrightRights">All rights reserved.</p>
 *
 * <p class="legal">
 * This software is the confidential and proprietary information of McFadyen Consulting
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms specified by McFadyen Consulting.
 * </p>
 */
package com.mwodt.common.message;

import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

/**
 * @author oracle
 */
public class MessagePropertyDescriptor extends RepositoryPropertyDescriptor {

    private static final long serialVersionUID = 1L;

    private final String DEFAULT_MESSAGE = "defaultMessage";

    private final String SITE_LOCALE_MESSAGES_PROPERTY = "siteLocaleMessages";

    private final char UNDERSCORE = '_';

    @SuppressWarnings("unchecked")
    @Override
    public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
        if (pItem == null) {
            return null;
        }
        final String defaultMessage = (String) pItem.getPropertyValue(DEFAULT_MESSAGE);

        Site currentSite = getCurrentSite();
        Locale locale = getLocale();

        if (currentSite == null || locale == null) {
            return defaultMessage;
        }
        String siteLocaleKey = getSiteLocaleKey(currentSite, locale);

        if (StringUtils.isBlank(siteLocaleKey)) {
            return defaultMessage;
        }
        final Map<String, String> siteLocaleMessagesMap = (Map<String, String>) pItem.getPropertyValue(SITE_LOCALE_MESSAGES_PROPERTY);

        if (MapUtils.isNotEmpty(siteLocaleMessagesMap) && siteLocaleMessagesMap.containsKey(siteLocaleKey)) {
            return siteLocaleMessagesMap.get(siteLocaleKey);
        } else {
            return defaultMessage;
        }

    }

    private String getSiteLocaleKey(Site pCurrentSite, Locale pLocale) {
        String siteId = pCurrentSite.getId();
        String locale = pLocale.toString();
        return siteId + UNDERSCORE + locale;
    }

    public Site getCurrentSite() {
        return SiteContextManager.getCurrentSite();
    }
}
