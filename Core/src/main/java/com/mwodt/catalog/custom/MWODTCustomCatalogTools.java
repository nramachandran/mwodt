package com.mwodt.catalog.custom;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import atg.commerce.catalog.custom.CustomCatalogTools;
import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.repository.RepositoryItem;

/**
 * @author mcfadyen Utility class containing catalog specific functionality.
 */
public class MWODTCustomCatalogTools extends CustomCatalogTools {

    private static final String PARENT_CATEGORIES_PROPERTY = "fixedParentCategories";

    private static final String ROOT_NAVIGATION_PROPERTY = "rootNavigationCategory";

    private static final String CATALOG_PROPERTY = "catalog";

    private DimensionValueCacheTools dimensionValueCacheTools;

    /**
     * @param categoryId
     * @return Get Dinemsion id by passing category id.
     */
    public String findDimensionIdForCategory(final String categoryId) {
        String dimensionId = null;
        DimensionValueCacheObject cacheObject = null;
        final List<DimensionValueCacheObject> dimensions = getDimensionValueCacheTools().get(categoryId);
        if (CollectionUtils.isNotEmpty(dimensions)) {
            cacheObject = dimensions.get(0);
            dimensionId = cacheObject.getDimvalId();
        }
        return dimensionId;
    }

    /**
     * @param childCategory
     * @return Check if the category passed is assigned to root which is identified from catalog root.
     */
    @SuppressWarnings("unchecked")
    public boolean isParentRootCategory(RepositoryItem childCategory) {
        Set<RepositoryItem> parentNodes = (Set<RepositoryItem>) childCategory.getPropertyValue(PARENT_CATEGORIES_PROPERTY);
        boolean parentRoot = false;
        if (CollectionUtils.isNotEmpty(parentNodes) && parentNodes.size() == 1) {
            for (RepositoryItem category : parentNodes) {
                RepositoryItem catalog = (RepositoryItem) category.getPropertyValue(CATALOG_PROPERTY);
                RepositoryItem rootNavCategory = (RepositoryItem) catalog.getPropertyValue(ROOT_NAVIGATION_PROPERTY);
                if (null != rootNavCategory && category.getRepositoryId().equals(rootNavCategory.getRepositoryId())) {
                    parentRoot = true;
                }
            }
        } else if (CollectionUtils.isEmpty(parentNodes)) {
            parentRoot = true;
        }
        return parentRoot;
    }

    public DimensionValueCacheTools getDimensionValueCacheTools() {
        return dimensionValueCacheTools;
    }

    public void setDimensionValueCacheTools(DimensionValueCacheTools dimensionValueCacheTools) {
        this.dimensionValueCacheTools = dimensionValueCacheTools;
    }

}
