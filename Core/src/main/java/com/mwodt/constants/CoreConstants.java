package com.mwodt.constants;

/**
 * @author mcfadyen
 *  The constants class
 */
public class CoreConstants {

    /**
     *  The category identifier.
     */
    public enum CategoryType {
		CATEGORY("category"), SUBCATEGORY("subcategory"), FAMILY("family");
		private String uri;

		CategoryType(String vUri) {
			uri = vUri;
		}

		public String getUri() {
			return uri;
		}
	};

}
