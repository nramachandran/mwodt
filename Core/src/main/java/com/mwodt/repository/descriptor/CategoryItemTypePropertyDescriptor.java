package com.mwodt.repository.descriptor;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import atg.nucleus.Nucleus;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

import com.mwodt.catalog.custom.MWODTCustomCatalogTools;
import com.mwodt.constants.CoreConstants.CategoryType;

/**
 * @author mcfadyen The property descriptor for category type.
 */
public class CategoryItemTypePropertyDescriptor extends RepositoryPropertyDescriptor {

    private static final long serialVersionUID = -8983632468030327409L;

    private static final String CHILD_CATEGORIES_PROPERTY = "childCategories";

    private static final String CATALOG_TOOLS_CONFIG_PATH = "/atg/commerce/catalog/CatalogTools";

    @SuppressWarnings("unchecked")
    @Override
    public Object getPropertyValue(RepositoryItemImpl item, Object value) {
        CategoryType type = CategoryType.FAMILY;
        List<RepositoryItem> childNodes = (List<RepositoryItem>) item.getPropertyValue(CHILD_CATEGORIES_PROPERTY);
        if (CollectionUtils.isNotEmpty(childNodes)) {
            MWODTCustomCatalogTools catalogTools = getCatalogTools();
            if (catalogTools.isParentRootCategory(item)) {
                type = CategoryType.CATEGORY;
            } else {
                type = CategoryType.SUBCATEGORY;
            }
        }
        return type.name();
    }

    public MWODTCustomCatalogTools getCatalogTools() {
        return (MWODTCustomCatalogTools) Nucleus.getGlobalNucleus().resolveName(CATALOG_TOOLS_CONFIG_PATH);
    }

}
