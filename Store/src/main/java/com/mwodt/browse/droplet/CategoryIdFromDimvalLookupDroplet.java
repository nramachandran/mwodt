/**
 * <p class="copyright">Copyright 2014 by McFadyen Consulting</p>
 * 
 * <div class="vcard">
 *   <div class="fn org">McFadyen Consulting</div>
 *   <div class="adr">
 *     <div>
 *       <span class="locality">Vienna</span>, 
 *       <abbr class="region" title="Virginia">VI</abbr> <span class="postal-code">22182</span>
 *     </div>
 *     <div class="country-name"><abbr title="United States of America">USA</abbr></div>
 *   </div>
 * </div>
 *
 * <p class="copyrightRights">All rights reserved.</p>
 * 
 * <p class="legal">
 * This software is the confidential and proprietary information of McFadyen Consulting
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms specified by McFadyen Consulting.
 * </p>
 */
package com.mwodt.browse.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * @author McFadyen This class returns category id from the dimension value.
 */
public class CategoryIdFromDimvalLookupDroplet extends DynamoServlet {

    private static final String DIMVAL_SEPARATOR = " ";

    private static final String DIMVAL_ID = "N";

    private static final String CATEGORY_ID = "categoryId";

    public static final String OUTPUT = "output";

    private DimensionValueCacheTools dimensionValueCacheTools;

    /**
     * Service method for droplet. Returns category id for a specific dimension value. Uses OOB logic.
     */
    @Override
    public void service(DynamoHttpServletRequest request, DynamoHttpServletResponse response) throws ServletException, IOException {
        String[] dimValIds = StringUtils.split(request.getParameter(DIMVAL_ID), DIMVAL_SEPARATOR);
        if (ArrayUtils.isNotEmpty(dimValIds)) {
            for (String dimValId : dimValIds) {
                DimensionValueCacheObject cacheObject = getDimensionValueCacheTools().getCachedObjectForDimval(dimValId);
                if (cacheObject != null) {
                    String categoryId = cacheObject.getRepositoryId();
                    request.setParameter(CATEGORY_ID, categoryId);
                    break;
                }
            }
        }
        request.serviceLocalParameter(OUTPUT, request, response);
    }

    public DimensionValueCacheTools getDimensionValueCacheTools() {
        return dimensionValueCacheTools;
    }

    public void setDimensionValueCacheTools(DimensionValueCacheTools dimensionValueCacheTools) {
        this.dimensionValueCacheTools = dimensionValueCacheTools;
    }

}
