/**
 * <p class="copyright">Copyright 2013 by McFadyen Consulting</p>
 *
 * <div class="vcard">
 *  <div class="fn org">McFadyen Consulting</div>
 *  <div class="adr">
 *    <div>
 *       <span class="locality">Vienna</span>,
 *       <abbr class="region" title="Virginia">VI</abbr> <span class="postal-code">22182</span>
 *    </div>
 *    <div class="country-name"><abbr title="United States of America">USA</abbr></div>
 *  </div>
 * </div>
 *
 * <p class="copyrightRights">All rights reserved.</p>
 *
 * <p class="legal">
 * This software is the confidential and proprietary information of McFadyen Consulting
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms specified by McFadyen Consulting.
 * </p>
 */
package com.mwodt.browse.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.mwodt.catalog.custom.MWODTCustomCatalogTools;

/**
 * @author McFadyen Extract dimension id from category.
 */
public class ExtractDimensionValueCacheIdDroplet extends DynamoServlet {

    private static final String DIMENSION_VALUE_CACHE_ID = "dimensionValueCacheId";

    private static final String CATEGORY_ID = "categoryId";

    private static final String OUTPUT = "output";

    private static final String EMPTY = "empty";

    private MWODTCustomCatalogTools catalogTools;

    /**
     * Service method for droplet. Returns dimension value for a specific category. Uses OOB logic.
     */
    @Override
    public void service(final DynamoHttpServletRequest request, final DynamoHttpServletResponse response) throws ServletException, IOException {
        final String categoryId = request.getParameter(ExtractDimensionValueCacheIdDroplet.CATEGORY_ID);
        final String dimValueCaheId = this.getCatalogTools().findDimensionIdForCategory(categoryId);
        if (this.isLoggingDebug()) {
            this.vlogDebug("Dimension Value Id retrieved from dimension for category Id <{0}> : <{1}>", categoryId, dimValueCaheId);
        }
        if (StringUtils.isNotBlank(dimValueCaheId)) {
            request.setParameter(ExtractDimensionValueCacheIdDroplet.DIMENSION_VALUE_CACHE_ID, dimValueCaheId);
            request.serviceLocalParameter(ExtractDimensionValueCacheIdDroplet.OUTPUT, request, response);
        } else {
            request.setParameter(ExtractDimensionValueCacheIdDroplet.DIMENSION_VALUE_CACHE_ID, null);
            request.serviceLocalParameter(ExtractDimensionValueCacheIdDroplet.EMPTY, request, response);
        }
    }

    public MWODTCustomCatalogTools getCatalogTools() {
        return catalogTools;
    }

    public void setCatalogTools(MWODTCustomCatalogTools catalogTools) {
        this.catalogTools = catalogTools;
    }

}
