package com.mwodt.browse.droplet

import spock.lang.Specification
import atg.commerce.endeca.cache.DimensionValueCacheObject
import atg.commerce.endeca.cache.DimensionValueCacheTools
import atg.servlet.DynamoHttpServletRequest
import atg.servlet.DynamoHttpServletResponse

class CategoryIdFromDimvalLookupDropletSpecification extends Specification {

    CategoryIdFromDimvalLookupDroplet testObj

    DimensionValueCacheTools dimensionValueMock = Mock()

    DynamoHttpServletRequest requestMock = Mock()

    DynamoHttpServletResponse responseMock = Mock()

    DimensionValueCacheObject cacheMock = Mock()

    def setup() {
        testObj = new CategoryIdFromDimvalLookupDroplet(dimensionValueCacheTools : dimensionValueMock)
        cacheMock.getRepositoryId() >> "Cat"
    }

    def "service method invoked with empty N value"() {
        given:
        requestMock.getParameter("N") >> null
        when:
        testObj.service(requestMock, responseMock)
        then:
        0 * dimensionValueMock.getCachedObjectForDimval(_)
        1 * requestMock.serviceLocalParameter("output", requestMock, _)
    }

    def "service method invoked with valid N value without cache mapping"() {
        given:
        requestMock.getParameter("N") >> "Dim"
        when:
        testObj.service(requestMock, responseMock)
        then:
        1 * dimensionValueMock.getCachedObjectForDimval("Dim") >> null
        0 * requestMock.setParameter("categoryId", null)
        1 * requestMock.serviceLocalParameter("output", requestMock, _)
    }

    def "service method invoked with valid N value with cache mapping"() {
        given:
        requestMock.getParameter("N") >> "Dim Dim2"
        when:
        testObj.service(requestMock, responseMock)
        then:
        1 * dimensionValueMock.getCachedObjectForDimval("Dim") >> null
        1 * dimensionValueMock.getCachedObjectForDimval("Dim2") >> cacheMock
        1 * requestMock.setParameter("categoryId", "Cat")
        1 * requestMock.serviceLocalParameter("output", requestMock, _)
    }
}

