package com.mwodt.browse.droplet

import spock.lang.Specification
import atg.servlet.DynamoHttpServletRequest
import atg.servlet.DynamoHttpServletResponse

import com.mwodt.browse.droplet.ExtractDimensionValueCacheIdDroplet
import com.mwodt.catalog.custom.MWODTCustomCatalogTools;

class ExtractDimensionValueCacheIdDropletSpecification extends Specification {

    ExtractDimensionValueCacheIdDroplet testObj

    MWODTCustomCatalogTools customToolsMock = Mock()

    DynamoHttpServletRequest requestMock = Mock()

    DynamoHttpServletResponse responseMock = Mock()

    def setup() {
        testObj = new ExtractDimensionValueCacheIdDroplet(catalogTools : customToolsMock)
    }

    def "test the service method when dym val returned by category is null"() {
        given:
        requestMock.getParameter(ExtractDimensionValueCacheIdDroplet.CATEGORY_ID) >> "cat"
        when:
        testObj.service(requestMock, responseMock)
        then:
        1 * customToolsMock.findDimensionIdForCategory("cat") >> null
        1 * requestMock.setParameter(ExtractDimensionValueCacheIdDroplet.DIMENSION_VALUE_CACHE_ID, null);
        1 * requestMock.serviceLocalParameter(ExtractDimensionValueCacheIdDroplet.EMPTY, requestMock, _);
    }

    def "test the service method when dym val returned by category is a valid value"() {
        given:
        requestMock.getParameter(ExtractDimensionValueCacheIdDroplet.CATEGORY_ID) >> "cat"
        when:
        testObj.service(requestMock, responseMock)
        then:
        1 * customToolsMock.findDimensionIdForCategory("cat") >> "dim"
        1 * requestMock.setParameter(ExtractDimensionValueCacheIdDroplet.DIMENSION_VALUE_CACHE_ID, "dim");
        1 * requestMock.serviceLocalParameter(ExtractDimensionValueCacheIdDroplet.OUTPUT, requestMock, _);
    }
}

