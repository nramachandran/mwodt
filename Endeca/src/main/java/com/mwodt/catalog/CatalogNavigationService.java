/**
 * <p class="copyright">Copyright 2015 by McFadyen Consulting</p>
 * 
 * <div class="vcard">
 *   <div class="fn org">McFadyen Consulting</div>
 *   <div class="adr">
 *     <div>
 *       <span class="locality">Vienna</span>, 
 *       <abbr class="region" title="Virginia">VI</abbr> <span class="postal-code">22182</span>
 *     </div>
 *     <div class="country-name"><abbr title="United States of America">USA</abbr></div>
 *   </div>
 * </div>
 *
 * <p class="copyrightRights">All rights reserved.</p>
 * 
 * <p class="legal">
 * This software is the confidential and proprietary information of McFadyen Consulting
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms specified by McFadyen Consulting.
 * </p>
 */
package com.mwodt.catalog;

import java.util.ArrayList;
import java.util.List;

import atg.commerce.catalog.custom.CatalogProperties;
import atg.commerce.catalog.custom.CustomCatalogTools;
import atg.endeca.assembler.AssemblerTools;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.collections.validator.CollectionObjectValidator;

/**
 * Helper bean to store user catalog navigation information providing convenience methods for: the current category that user is currently viewing the ancestor categories from the current category to
 * the top level category the full category path from and including the current category to the top level category the top level category The bean is intended to be used anywhere requiring access to
 * the current shopper catalog navigation such as breadcrumbs, continue shopping, and with targeters to specify targeting rules based on the currently viewed category. The store uses this when
 * targeting promotional content to the shopper.
 *
 */
public class CatalogNavigationService {
    

    /**
     * The top level category for the category that shopper is currently viewing.
     */
    protected String mTopLevelCategoryId = "";

    /**
     * The current category that shopper is currently viewing.
     */
    protected String mCurrentCategoryId = "";

    /**
     * The ancestors of the category that shopper is currently viewing.
     */
    protected String[] mAncestorCategoryIds = new String[0];

    /**
     * The full category path from and including the current category to the top level category.
     */
    protected String[] mCategoryNavigationPath = new String[0];

    /**
     * The boolean flag that indicates if current category is valid
     */
    private boolean mCurrentCategoryValid = true;

    /**
     * property: Catalog Tools
     */
    private CustomCatalogTools mCatalogTools;

    /**
     * Validators that will be applied to the category
     */
    private CollectionObjectValidator[] mValidators;

    /**
     * Tracks the shopper's catalog navigation setting the current category being viewed, the ancestor categories of the current category, the full category path from the top level category to the
     * current category, and the top level category.
     *
     * @param pCategoryId
     * the current category.
     * @param pAncestorCategoryIds
     * the ancestors of the current category that user is currently viewing.
     */
    public void navigate(String pCategoryId, List<String> pAncestorCategoryIds) {
        List<String> categoryNavigationPath = new ArrayList<String>((pAncestorCategoryIds == null) ? new ArrayList<String>() : new ArrayList<String>(pAncestorCategoryIds));
        setCurrentCategoryValid(validateCategory(pCategoryId));
        categoryNavigationPath.add(pCategoryId);
        String[] categoryNavigationPathArray = new String[categoryNavigationPath.size()];
        categoryNavigationPathArray = categoryNavigationPath.toArray(categoryNavigationPathArray);
        setCategoryNavigationPath(categoryNavigationPathArray);

        setCurrentCategory(pCategoryId);

        if (pAncestorCategoryIds != null && !pAncestorCategoryIds.isEmpty()) {
            setTopLevelCategory(pAncestorCategoryIds.get(0));
            String[] ancestorCategoryIdsArray = new String[pAncestorCategoryIds.size()];
            ancestorCategoryIdsArray = pAncestorCategoryIds.toArray(ancestorCategoryIdsArray);
            setAncestorCategories(ancestorCategoryIdsArray);
        } else {
            setTopLevelCategory(pCategoryId);
            setAncestorCategories(new String[0]);
        }
    }

    /**
     * This method retrieves category and applies configured validators to it.
     * 
     * @param pCategoryId
     * the id of category to validate
     * @return true if category is valid, false otherwise.
     */
    private boolean validateCategory(String pCategoryId) {
        // There is no validators set, so no filtering is needed.
        boolean isValid = false;
        if (getValidators() == null || getValidators().length == 0) {
            return true;
        }

        Repository catalog = getCatalogTools().getCatalog();
        CatalogProperties catalogProperties = getCatalogTools().getCatalogProperties();
        // Retrieve currentCategoryItem from Catalog
        RepositoryItem currentCategoryItem = null;
        try {
            currentCategoryItem = catalog.getItem(pCategoryId, catalogProperties.getCategoryItemName());
        } catch (RepositoryException re) {
            AssemblerTools.getApplicationLogging().logError("There was a problem retrieving the category from the catalog", re);
            isValid = false;
        }

         isValid = true;
        if (currentCategoryItem != null) {

            for (CollectionObjectValidator validator : getValidators()) {
                if (!validator.validateObject(currentCategoryItem)) {

                    // Item doesn't pass validation. Set isValid to false
                    // and leave the loop as there is no need to check all
                    // others validators.
                    isValid = false;
                    break;
                }
            }
        }

        return isValid;
    }

    /**
     * Clears the current navigation settings.
     */
    public void clear() {
        setCategoryNavigationPath(new String[0]);
        setCurrentCategory("");
        setTopLevelCategory("");
        setAncestorCategories(new String[0]);
    }

    /**
     * Sets the top level category for the category that user is currently viewing.
     *
     * @param pTopLevelCategory
     * the top level category for the category that user is currently viewing.
     */
    public void setTopLevelCategory(String pTopLevelCategoryId) {
        mTopLevelCategoryId = pTopLevelCategoryId;
    }

    /**
     * Returns the top level category for the category that user is currently viewing.
     *
     * @return the top level category for the category that user is currently viewing.
     */
    public String getTopLevelCategory() {
        return mTopLevelCategoryId;
    }

    /**
     * Sets the current category that shopper is currently viewing.
     *
     * @param pCurrentCategoryId
     * the current category that shopper is currently viewing.
     */
    public void setCurrentCategory(String pCurrentCategoryId) {
        mCurrentCategoryId = pCurrentCategoryId;
    }

    /**
     * Returns the current category that user is currently viewing.
     *
     * @return the current category that user is currently viewing.
     */
    public String getCurrentCategory() {
        return mCurrentCategoryId;
    }

    /**
     * Sets ancestors of the category that user is currently viewing.
     *
     * @param pAncestorCategoryIds
     * the ancestors of the category that user is currently viewing.
     */
    public void setAncestorCategories(String[] pAncestorCategoryIds) {
        mAncestorCategoryIds = pAncestorCategoryIds;
    }

    /**
     * Returns the ancestors of the category that user is currently viewing. The first category in the list is the top level category and the last category in the list is the parent category of the
     * category that user is currently viewing.
     *
     * @return the ancestors of the category that user is currently viewing.
     */
    public String[] getAncestorCategories() {
        return mAncestorCategoryIds;
    }

    /**
     * Sets the full category path from and including the current category to the top level category.
     *
     * @param pCategoryNavigationPath
     * the full category path from and including the current category to the top level category.
     */
    public void setCategoryNavigationPath(String[] pCategoryNavigationPath) {
        mCategoryNavigationPath = pCategoryNavigationPath;
    }

    /**
     * Returns full category path from and including the current category to the top level category. The first category in the list is the top level category and the last category in the list is the
     * category that user is currently viewing.
     *
     * @return the full category path from and including the current category to the top level category.
     */
    public String[] getCategoryNavigationPath() {
        return mCategoryNavigationPath;
    }

    /**
     * @return the isCurrentCategoryValid
     */
    public boolean isCurrentCategoryValid() {
        return mCurrentCategoryValid;
    }

    /**
     * @param puCrrentCategoryValid
     * the currentCategoryValid to set
     */
    public void setCurrentCategoryValid(boolean pCurrentCategoryValid) {
        mCurrentCategoryValid = pCurrentCategoryValid;
    }

    /**
     * @return the CatalogTools component.
     */
    public CustomCatalogTools getCatalogTools() {
        return mCatalogTools;
    }

    /**
     * @param pCatalogTools
     * - The CatalogTools component.
     */
    public void setCatalogTools(CustomCatalogTools pCatalogTools) {
        mCatalogTools = pCatalogTools;
    }

    /**
     * @return the validators
     */
    public CollectionObjectValidator[] getValidators() {
        return mValidators;
    }

    /**
     * @param pValidators
     * the validators to set
     */
    public void setValidators(CollectionObjectValidator[] pValidators) {
        mValidators = pValidators;
    }
}