package com.mwodt.assembler.navigation;

import java.util.List;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.endeca.assembler.navigation.NavigationStateProcessor;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.UserState;
import com.endeca.infront.navigation.model.RangeFilter;
import com.mwodt.catalog.CatalogNavigationService;
import com.mwodt.catalog.custom.MWODTCustomCatalogTools;
import com.mwodt.constants.CoreConstants;

/**
 * @author oracle Navigation state processor.
 */
public class MWODTNavigationStateProcessor implements NavigationStateProcessor {

    private static final String CATEGORY_ITEM_TYPE = "categoryItemType";

    private String mUserSegment = null;

    private String mUserSegmentSubCat = null;

    private MWODTCustomCatalogTools mCatalogTools;

    private UserState mUserState = null;

    private DimensionValueCacheTools mDimensionValueCacheTools = null;

    private List<String> mIgnoredRangeFilters = null;

    private CatalogNavigationService mCatalogNavigationService = null;

    /**
     * Process the navigation state to determine if it represents a user catalog navigation and update the details on the catalog navigation tracking component and set the user
     * segment on the user state for routing the user to the category page.
     * 
     * @inheritDoc
     */
    @Override
    public void process(NavigationState pNavigationState) {

        DimensionValueCacheObject cacheObject = null;
        String navigationFilterId = "";
        List<String> navigationFilters = pNavigationState.getFilterState().getNavigationFilters();
        if (navigationFilters.size() == 1) {
            navigationFilterId = navigationFilters.get(0);
        }

        if (!StringUtils.isEmpty(navigationFilterId)) {

            cacheObject = getDimensionValueCacheTools().getCachedObjectForDimval(navigationFilterId);

            try {
                if (null != cacheObject) {
                    RepositoryItem categoryItem = mCatalogTools.findCategory(cacheObject.getRepositoryId());
                    String catType = String.valueOf(categoryItem.getPropertyValue(CATEGORY_ITEM_TYPE));
                    if (CoreConstants.CategoryType.CATEGORY.name().equals(catType)) {
                        addCatalogUserState(pNavigationState, getUserSegment());
                        updateCatalogNavigation(cacheObject.getRepositoryId(), cacheObject.getAncestorRepositoryIds());
                    } else if (CoreConstants.CategoryType.SUBCATEGORY.name().equals(catType)) {
                        addCatalogUserState(pNavigationState, getUserSegmentSubCat());
                        updateCatalogNavigation(cacheObject.getRepositoryId(), cacheObject.getAncestorRepositoryIds());
                    }

                }
            } catch (RepositoryException repException) {
                AssemblerTools.getApplicationLogging().vlogError("Cannot fetch category item", repException);
            }

        } else {
            updateCatalogNavigation(null, null);
        }
    }

    /**
     * Update the catalog navigation component.
     * 
     * @param pCategoryId
     * The categoryId to set as the last browsed category.
     * @param pAncestors
     * The ancestor category ids.
     */
    protected void updateCatalogNavigation(String pCategoryId, List<String> pAncestors) {
        if (getCatalogNavigation() != null) {
            if (pCategoryId != null) {
                getCatalogNavigation().navigate(pCategoryId, pAncestors);
            } else {
                getCatalogNavigation().clear();
            }
        }
    }

    /**
     * <p>
     * Add the CategoryNavigation user segment. We only do this when there are no search filters or range filters. This is because we only want to land on the category page when
     * only a category is selected.
     * </p>
     * <p>
     * The 'ignoredRangeFilters' property used in this method defines a list of range filters to ignore when determining whether to add the CatalogNavigation segment or not.
     * </p>
     * 
     * @param pNavigationState
     * - The NavigationState object that holds the current search/range filters.
     */
    protected void addCatalogUserState(NavigationState pNavigationState, String userSegment) {
        int numIgnoredRangeFiltersInNavState = 0;

        if (getIgnoredRangeFilters() != null) {
            for (RangeFilter navStateRangeFilter : pNavigationState.getFilterState().getRangeFilters()) {
                if (getIgnoredRangeFilters().contains(navStateRangeFilter.getPropertyName())) {
                    numIgnoredRangeFiltersInNavState++;
                }
            }
        }

        if (pNavigationState.getFilterState().getSearchFilters().isEmpty()
                && (pNavigationState.getFilterState().getRangeFilters().isEmpty() || pNavigationState.getFilterState().getRangeFilters().size() == numIgnoredRangeFiltersInNavState)) {

            getUserState().addUserSegments(userSegment);
        }
    }

    /**
     * @param pUserSegment
     * - The user segment to set on the user state and used within Experience Manager to control routing the user to the category page instead of the search results page.
     */
    public void setUserSegment(String pUserSegment) {
        mUserSegment = pUserSegment;
    }

    /**
     * @return the user segment to set on the user state and used within Experience Manager to control routing the user to the category page instead of the search results page.
     */
    public String getUserSegment() {
        return (mUserSegment);
    }

    /**
     * @param pUserSegment
     * - The user segment to set on the user state and used within Experience Manager to control routing the user to the category page instead of the search results page.
     */
    public void setUserSegmentSubCat(String pUserSegment) {
        mUserSegmentSubCat = pUserSegment;
    }

    /**
     * @return the user segment to set on the user state and used within Experience Manager to control routing the user to the category page instead of the search results page.
     */
    public String getUserSegmentSubCat() {
        return (mUserSegmentSubCat);
    }

    /**
     * @param pUserState
     * - The userState object used to hold the user segment for use within Experience Manager to control routing the user to the category page instead of the search results page.
     */
    public void setUserState(UserState pUserState) {
        mUserState = pUserState;
    }

    /**
     * @return the userState object used to hold the user segment for use within Experience Manager to control routing the user to the category page instead of the search results
     * page.
     */
    public UserState getUserState() {
        return (mUserState);
    }

    /**
     * @param pDimensionValueCacheTools
     * - The utility class for access to the ATG<->Endeca catalog cache.
     */
    public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools) {
        mDimensionValueCacheTools = pDimensionValueCacheTools;
    }

    /**
     * @return the utility class for access to the ATG<->Endeca catalog cache.
     */
    public DimensionValueCacheTools getDimensionValueCacheTools() {
        return mDimensionValueCacheTools;
    }

    /**
     * @param pIgnoredRangeFilters
     * - The range filters to ignore when determining whether to add the CatalogNavigation user segment or not.
     */
    public void setIgnoredRangeFilters(List<String> pIgnoredRangeFilters) {
        mIgnoredRangeFilters = pIgnoredRangeFilters;
    }

    /**
     * @return the range filters to ignore when determining whether to add the CatalogNavigation user segment or not.
     */
    public List<String> getIgnoredRangeFilters() {
        return mIgnoredRangeFilters;
    }

    /**
     * @param pCatalogTools
     * - The CatalogTools object to use when looking up products, categories and skus.
     */

    public void setCatalogTools(MWODTCustomCatalogTools pCatalogTools) {
        mCatalogTools = pCatalogTools;
    }

    /**
     * @return the CatalogTools object to use when looking up products, categories and skus.
     */
    public MWODTCustomCatalogTools getCatalogTools() {
        return mCatalogTools;
    }

    /**
     * @param pCatalogNavigationService
     * - The component used to track users catalog navigation.
     */
    public void setCatalogNavigation(CatalogNavigationService pCatalogNavigationService) {
        mCatalogNavigationService = pCatalogNavigationService;
    }

    /**
     * @return the component used to track users catalog navigation.
     */
    public CatalogNavigationService getCatalogNavigation() {
        return (mCatalogNavigationService);
    }

}
