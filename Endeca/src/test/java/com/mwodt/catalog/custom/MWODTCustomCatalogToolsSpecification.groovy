package com.mwodt.catalog.custom

import spock.lang.Specification
import atg.commerce.endeca.cache.DimensionValueCacheObject
import atg.commerce.endeca.cache.DimensionValueCacheTools
import atg.repository.RepositoryItem

class MWODTCustomCatalogToolsSpecification extends Specification {

    MWODTCustomCatalogTools testObj

    DimensionValueCacheTools dimensionCacheMock = Mock()
    DimensionValueCacheObject cacheMock = Mock()

    RepositoryItem repItemMock = Mock()
    RepositoryItem categoryItemMock = Mock()
    RepositoryItem categoryItemMock1 = Mock()
    RepositoryItem catalogItemMock = Mock()
    RepositoryItem rootCategoryItemMock = Mock()

    def setup() {
        testObj = new TRUCustomCatalogTools(dimensionValueCacheTools:dimensionCacheMock)
        cacheMock.getDimvalId() >> "D01"
        rootCategoryItemMock.getRepositoryId() >> "root"
    }

    def "findDimensionIdForCategory passing valid category id"() {
        given:
        dimensionCacheMock.get("cat01") >> [cacheMock]
        when:
        def result = testObj.findDimensionIdForCategory("cat01")
        then:
        result == "D01"
    }

    def "findDimensionIdForCategory passing category id which does not have a mapping"() {
        given:
        dimensionCacheMock.get("cat02") >> null
        when:
        def result = testObj.findDimensionIdForCategory("cat02")
        then:
        result == null
    }

    def "check if parent category is root node by passing catalog item without parent"() {
        given:
        repItemMock.getPropertyValue(TRUCustomCatalogTools.PARENT_CATEGORIES_PROPERTY) >> null
        when:
        def result = testObj.isParentRootCategory(repItemMock)
        then:
        result
    }

    def "check if parent category is root node by passing sub category node with multiple parents"() {
        given:
        categoryItemMock.getPropertyValue(TRUCustomCatalogTools.CATALOG_PROPERTY) >> catalogItemMock
        repItemMock.getPropertyValue(TRUCustomCatalogTools.PARENT_CATEGORIES_PROPERTY) >> ([
            categoryItemMock,
            categoryItemMock1
        ] as Set)
        when:
        def result = testObj.isParentRootCategory(repItemMock)
        then:
        !result
    }

    def "check if parent category is root node by passing catalog item with parent as some other category"() {
        given:
        categoryItemMock.getPropertyValue(TRUCustomCatalogTools.CATALOG_PROPERTY) >> catalogItemMock
        repItemMock.getPropertyValue(TRUCustomCatalogTools.PARENT_CATEGORIES_PROPERTY) >> ([
            categoryItemMock
        ] as Set)
        when:
        def result = testObj.isParentRootCategory(repItemMock)
        then:
        !result
    }

    def "check if parent category is root node by passing catalog item with parent as some other category case 2"() {
        given:
        categoryItemMock.getPropertyValue(TRUCustomCatalogTools.CATALOG_PROPERTY) >> catalogItemMock
        catalogItemMock.getPropertyValue(TRUCustomCatalogTools.ROOT_NAVIGATION_PROPERTY) >> rootCategoryItemMock
        repItemMock.getPropertyValue(TRUCustomCatalogTools.PARENT_CATEGORIES_PROPERTY) >> ([
            categoryItemMock
        ] as Set)
        categoryItemMock.getRepositoryId() >> "child"
        when:
        def result = testObj.isParentRootCategory(repItemMock)
        then:
        !result
    }

    def "check if parent category is root node by passing catalog item which is a root node"() {
        given:
        categoryItemMock.getPropertyValue(TRUCustomCatalogTools.CATALOG_PROPERTY) >> catalogItemMock
        catalogItemMock.getPropertyValue(TRUCustomCatalogTools.ROOT_NAVIGATION_PROPERTY) >> rootCategoryItemMock
        repItemMock.getPropertyValue(TRUCustomCatalogTools.PARENT_CATEGORIES_PROPERTY) >> ([
            categoryItemMock
        ] as Set)
        categoryItemMock.getRepositoryId() >> "root"
        when:
        def result = testObj.isParentRootCategory(repItemMock)
        then:
        result
    }
}

