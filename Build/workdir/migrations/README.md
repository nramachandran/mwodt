# Flyway
### Database Migrations Made Easy.

![alt text](http://flywaydb.org/assets/logo/flyway-logo-tm.png "Flyway")

#### Evolve your database schema easily and reliably across all your instances.
Simple, focused and powerful.

#### Works on
Windows, Mac OSX, Linux, Java and Android

#### Supported build tools
Maven, Gradle, Ant and SBT

#### Supported databases
Oracle, SQL Server, SQL Azure, DB2, DB2 z/OS, MySQL, MariaDB, Google Cloud SQL, PostgreSQL, Redshift, Vertica, H2, Hsql, Derby, SQLite

#### Third party plugins
Grails, Play!, Grunt, Griffon, Ninja, ...

## Documentation
http://flywaydb.org

### [How to write the SQL files](http://flywaydb.org/documentation/migration/sql.html)

*prefix*: Configurable, default: V
*version*: Dots or underscores separate the parts, you can use as many parts as you like
*separator*: Configurable, default: __ (two underscores)
*description*: Underscores or spaces separate the words
*suffix*: Configurable, default: .sql

#### Example:
V1\_1__some_cool_description.sql