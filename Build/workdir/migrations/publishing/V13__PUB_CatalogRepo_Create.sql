
CREATE TABLE tru_catalog (
	catalog_id 		varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	root_nav_cat 		varchar2(254)	NULL,
	PRIMARY KEY(catalog_id, asset_version)
);
