CREATE TABLE MWODT_MESSAGES(
	ASSET_VERSION      	INT           	NOT NULL,
	BRANCH_ID          	VARCHAR(40)   	NOT NULL,
	IS_HEAD            	NUMERIC(1)    	NOT NULL,
	VERSION_DELETED    	NUMERIC(1)    	NOT NULL,
	VERSION_EDITABLE   	NUMERIC(1)     	NOT NULL,
	WORKSPACE_ID       	VARCHAR(40)    	NOT NULL,
	PRED_VERSION       	INT            	NULL,
	CHECKIN_DATE       	TIMESTAMP      	NULL,
	ID	VARCHAR2(254)   NOT NULL, 
	KEY VARCHAR2(254)   NOT NULL,
	DEFAULT_MESSAGE VARCHAR2(254) NULL,
	MESSAGE_TYPE INTEGER NOT NULL,
	CONSTRAINT MWODT_MESSAGES_P PRIMARY KEY(ID,ASSET_VERSION),
	CONSTRAINT MWODT_MESSAGES_U UNIQUE(KEY)
); 

CREATE TABLE MWODT_SITE_LOCALE_MESSAGES (
	ID                       VARCHAR2(254)   NOT NULL, 
	SEQUENCE_NUM             VARCHAR2(254)   NOT NULL,
	ASSET_VERSION      		 INT           	NOT NULL,
	SITE_LOCALE_MESSAGES   VARCHAR2(254) NULL,         
	CONSTRAINT MWODT_SITE_LOCALE_MESSAGES_P PRIMARY KEY(ID,SEQUENCE_NUM,ASSET_VERSION)
);