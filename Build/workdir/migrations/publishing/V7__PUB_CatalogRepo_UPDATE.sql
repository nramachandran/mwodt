drop table tru_product_safety_warning_rel;
drop table tru_product_safety_warning;
drop table tru_sku_car_seat_feature_rel;
drop table tru_sku_car_seat_feature;
drop table tru_sku_car_seat_type_rel;
drop table tru_sku_car_seat_type;
drop table tru_sku_car_seat_whatisimp_rel;
drop table tru_sku_car_seat_whatisimp;
drop table tru_sku_crib_type_rel;
drop table tru_sku_crib_type;
drop table tru_sku_crib_style_rel;
drop table tru_sku_crib_style;
drop table tru_sku_crib_whatisimp_rel;
drop table tru_sku_crib_whatisimp;
drop table tru_sku_crib_material_type_rel;
drop table tru_sku_crib_material_type;
drop table tru_sku_strl_extra_rel;
drop table tru_sku_strl_extra;
drop table tru_sku_strl_type_rel;
drop table tru_sku_strl_type;
drop table tru_sku_strl_best_use_rel;
drop table tru_sku_strl_best_use;
drop table tru_sku_strl_whatisimp_rel;
drop table tru_sku_strl_whatisimp;

create table tru_product_safety_warning (
	product_id 		varchar2(40)	not null,
	asset_version 		integer	not null,
	sequence_num 		integer	not null,
	safety_warning		varchar2(360)	null,
	constraint tru_prdct_safety_wrng_rel_pk primary key(product_id, asset_version, sequence_num)
);

create table tru_sku_car_seat_feature (
	sku_id 		varchar2(40)	not null,
	asset_version 		integer	not null,
	sequence_num 		integer	not null,
	car_seat_feature	varchar2(360)	null,
	constraint tru_sku_car_st_ftre_rel_pk primary key(sku_id, asset_version, sequence_num)
);

create table tru_sku_car_seat_type (
	sku_id 		varchar2(40)	not null,
	asset_version 		integer	not null,
	sequence_num 		integer	not null,
	car_seat_type		varchar2(360)	null,
	constraint tru_sku_car_seat_type_rel_pk primary key(sku_id, asset_version, sequence_num)
);
create table tru_sku_car_seat_whatisimp (
	sku_id 		varchar2(40)	not null,
	asset_version 		integer	not null,
	sequence_num 		integer	not null,
	car_seat_whatisimp	varchar2(360)	null,
	constraint tru_sku_car_st_wtisimp_rel_pk primary key(sku_id, asset_version, sequence_num)
);

create table tru_sku_crib_type (
	sku_id 		varchar2(40)	not null,
	asset_version 		integer	not null,
	sequence_num 		integer	not null,
	crib_type	varchar2(360)	null,
	constraint tru_sku_crib_type_rel_pk primary key(sku_id, asset_version, sequence_num)
);

create table tru_sku_crib_style (
	sku_id 		varchar2(40)	not null,
	asset_version 		integer	not null,
	sequence_num 		integer	not null,
	crib_style	varchar2(360)	null,
	constraint tru_sku_crib_style_rel_pk primary key(sku_id, asset_version, sequence_num)
);
create table tru_sku_crib_material_type (
	sku_id 		varchar2(40)	not null,
	asset_version 		integer	not null,
	sequence_num 		integer	not null,
	crib_material_type	varchar2(360)	null,
	constraint tru_sku_crib_mtrl_type_rel_pk primary key(sku_id, asset_version, sequence_num)
);

create table tru_sku_crib_whatisimp (
	sku_id 		varchar2(40)	not null,
	asset_version 		integer	not null,
	sequence_num 		integer	not null,
	crib_whatisimp	varchar2(360)	null,
	constraint tru_sku_crib_whatisimp_rel_pk primary key(sku_id, asset_version, sequence_num)
);
create table tru_sku_strl_best_use (
	sku_id 		varchar2(40)	not null,
	asset_version 		integer	not null,
	sequence_num 		integer	not null,
	strl_best_use	varchar2(360)	null,
	constraint tru_sku_strl_best_use_rel_pk primary key(sku_id, asset_version, sequence_num)
);

create table tru_sku_strl_type (
	sku_id 		varchar2(40)	not null,
	asset_version 		integer	not null,
	sequence_num 		integer	not null,
	strl_type	varchar2(360)	null,
	constraint tru_sku_strl_type_rel_pk primary key(sku_id, asset_version, sequence_num)
);

create table tru_sku_strl_extra (
	sku_id 		varchar2(40)	not null,
	asset_version 		integer	not null,
	sequence_num 		integer	not null,
	strl_extra	varchar2(360)	null,
	constraint tru_sku_strl_extra_rel_pk primary key(sku_id, asset_version, sequence_num)
);

create table tru_sku_strl_whatisimp (
	sku_id 		varchar2(40)	not null,
	asset_version 		integer	not null,
	sequence_num 		integer	not null,
	strl_whatisimp	varchar2(360)	null,
	constraint tru_sku_strl_whatisimp_rel_pk primary key(sku_id, asset_version, sequence_num)
);