ALTER TABLE "TRU_PRODUCT_REFERENCE" 
ADD ( 
branch_id          VARCHAR(40)   ,
checkin_date       TIMESTAMP     NULL
);


ALTER TABLE "TRU_VIDEO_GAME_INFO" 
DROP ( 
is_head          ,
version_deleted  ,
version_editable ,
pred_version     
);
