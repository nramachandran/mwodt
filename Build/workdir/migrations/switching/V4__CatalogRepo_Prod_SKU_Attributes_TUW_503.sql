
ALTER TABLE TRU_PRODUCT 
ADD ( FLEXIBLE_SHIPPING_PLAN	VARCHAR(360),
      EWASTE_SURCHARGE_STATE	VARCHAR(40),
      EWASTE_SURCHARGE_SKU	VARCHAR(40)

    );
    
ALTER TABLE "TRU_SKU" 
ADD ( STUDIO	VARCHAR(40),
      NO_OF_DISKS	VARCHAR(360),
      RUN_TIME	VARCHAR(360),
      DVD_REGION	VARCHAR(360),
      PUBLISHER	VARCHAR(360),
      CATALOG_NUMBER	VARCHAR(360),
      DATE_PUBLISHED	DATE,
      NO_OF_PAGES	VARCHAR(360),
      PARENTAL_ADVISORY	VARCHAR(50),
      MPAA_RATING	VARCHAR(360),
      FORMAT	VARCHAR(360),
      DIRECTOR	VARCHAR(40),
      UL_APPROVED	NUMBER(1),
      CUSTOMER_PURCHASE_LIMIT	VARCHAR(360),
      NMWA_PERCENTAGE	NUMBER(5,2),
      ASSEMBLY_REQUIRED	NUMBER(1),
      ASSEMBLED_DEPTH	VARCHAR(360),
      ASSEMBLED_HEIGHT	VARCHAR(360),
      ASSEMBLED_WIDTH	VARCHAR(360),
      ASSEMBLED_WEIGHT	VARCHAR(360),
      ITEM_WEIGHT	VARCHAR(360),
      ITEM_WIDTH	VARCHAR(360),
      ITEM_HEIGHT	VARCHAR(360),
      ITEM_DEPTH	VARCHAR(360),
      SHIP_IN_ORIG_CONTAINER NUMBER(1),
      CAN_BE_GIFT_WRAPPED	NUMBER(1),
      LOWER48_FIXED_SURCHARGE	VARCHAR(40),
      LOWER48_STANDARD_F_S_DOLLAR	VARCHAR(40),
      LOWER48_2_DAY_F_S_DOLLAR	VARCHAR2(40),
      LOWER48_OVERNIGHT_F_S_DOLLAR	VARCHAR2(40),
      AK_HI_FIXED_SURCHARGE	VARCHAR2(40),
      AK_HI_STD_F_S_DOLLAR	VARCHAR2(40),
      AK_HI_2_DAY_F_S_DOLLAR	VARCHAR2(40),
      AK_HI_OVERNIGHT_F_S_DOLLAR	VARCHAR2(40),
      OTHER_FIXED_SURCHARGE	VARCHAR2(40),
      OTHER_STD_F_S_DOLLAR	VARCHAR2(40), 
      SAFETY_STOCK	VARCHAR2(40), 
      SPECIAL_ASSEMBLY_INSTRUCTIONS	VARCHAR2(360),
      SYSTEM_REQUIREMENTS	VARCHAR2(40),
      INCREMENTAL_SAFETY_STOCK_UNITS	VARCHAR(40),
      PRESELL_QUANTITY_UNITS	VARCHAR2(40),
      COLOR_CODE	VARCHAR2 (255),
      STYLE_DESCRIPTION	VARCHAR2 (255),
      SIZE_CHART_NAME	VARCHAR2 (255)

      --PriceDisplayOnCart	

 );
 
 --Stores the video game attributes
CREATE TABLE TRU_VIDEO_GAME_INFO (
      SKU_ID VARCHAR2(40) NOT NULL REFERENCES TRU_SKU(SKU_ID),
      VIDEO_GAME_GENRE	VARCHAR(40),
      VIDEO_GAME_PLATFORM	VARCHAR(40),
      VIDEO_GAME_ESRB	VARCHAR(40),
      CONSTRAINT "TRU_VIDEO_GAME_INFO_PK" PRIMARY KEY ("SKU_ID")
 );
 
   
--The product cross reference relationship table
CREATE TABLE TRU_PRODUCT_REFERENCE_REL (
      SKU_ID VARCHAR2(40) NOT NULL REFERENCES TRU_SKU(SKU_ID),
      REF_SKU_ID	VARCHAR(40),
       CONSTRAINT "TRU_PRODUCT_REFERENCE_REL_PK" PRIMARY KEY (SKU_ID,REF_SKU_ID)
 );
            
--Stores the product cross reference data
CREATE TABLE TRU_PRODUCT_REFERENCE (
      REF_SKU_ID VARCHAR2(40) NOT NULL REFERENCES TRU_SKU(SKU_ID),
      "TYPE"	VARCHAR(40),
      "QUANTITY"	INTEGER,
      CONSTRAINT "TRU_PRODUCT_REFERENCE_PK" PRIMARY KEY (REF_SKU_ID)
 );
            
--The Classification reference relationship table
CREATE TABLE TRU_CLASSIFY_REFERENCE_REL (
      SKU_ID VARCHAR2(40) NOT NULL REFERENCES TRU_SKU(SKU_ID),
      REF_ID	VARCHAR(40),
       CONSTRAINT "TRU_CLASSIFY_REFERENCE_REL_PK" PRIMARY KEY (SKU_ID,REF_ID)
 );