alter table tru_product
rename column drop_ship_id to drop_ship_flag;

alter table tru_product 
rename column registry_wish_list_eligibility to registry_eligibility;