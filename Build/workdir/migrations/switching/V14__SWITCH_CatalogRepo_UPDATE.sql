CREATE TABLE tru_product_site_eligibility 
(product_id  VARCHAR2(40) NOT NULL,
site_eligibility  VARCHAR2(40),
sequence_num number(38) not null,
	CONSTRAINT TRU_PRODUCT_SITEELIGIBILITY_PK PRIMARY KEY (site_eligibility,sequence_num));
	
	create index tru_product_site_elig_idx on tru_product_site_eligibility(product_id);

ALTER TABLE TRU_SKU 
drop(
can_be_gift_wrapped,
ship_in_orig_container,
assembly_required,
ul_approved
);

alter table tru_sku
ADD(
 SUPRESS_IN_SEARCH NUMBER(1,0),
 BATTERY_REQUIRED VARCHAR2(1),
 BATTERY_INCLUDED VARCHAR2(1),
 CRIB_STORAGE VARCHAR2(1),
 CRIB_BEDRAILS VARCHAR(1),
 CRIB_CONVERSION_KIT_INCLUDED VARCHAR2(1),
 CRIB_HARDWARE_VISIBLE VARCHAR2(1),
 STRL_CAR_SEAT_COMPATIBLE VARCHAR2(1),
 UL_APPROVED VARCHAR2(1),
 ASSEMBLY_REQUIRED VARCHAR2(1),
 SHIP_IN_ORIG_CONTAINER VARCHAR2(1),
 CAN_BE_GIFT_WRAPPED VARCHAR2(1)
);

