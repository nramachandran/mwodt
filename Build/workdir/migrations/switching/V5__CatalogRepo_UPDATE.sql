alter table tru_product
  add (
	mfr_style_number varchar2(360),
	cost varchar2(40),
	drop_ship_id number(1,0),
	registry_wish_list_eligibility number(1,0),
	vendors_product_demo_url varchar2(255),
	legal_notice clob
);

alter table tru_sku
  add (registry_warning_indicator varchar2(40),
		product_awards clob,
		furniture_finish varchar2(360),
		street_date timestamp,
		color_upc_level varchar2(360),
		juvenile_product_size varchar2(360),
		back_order_status number(1,0),
		web_disp_flag number(1,0),
		browse_hidden_keyword varchar2(255),
		outlet number(1,0),
		online_collection_name varchar2(255),
		max_target_age_tru_websites varchar2(360),
		min_target_age_tru_websites varchar2(360),
		mfr_suggested_age_min varchar2(360),
		mfr_suggested_age_max varchar2(360),
		child_weight_min varchar2(360),
		child_weight_max varchar2(360),
		promotional_sticker_display varchar2(360),
		car_seat_easeofuse_rating varchar2(360),
		car_seat_pattern varchar2(255),
		car_seat_side_imp_protect varchar2(360),
		strl_children_capacity varchar2(360),
		strl_pattern varchar2(255),
		primary_category varchar2(360),
		interest varchar2(360),
		skills varchar2(360),
		play_type varchar2(360),
		what_is_important varchar2(360)		
);

create table tru_product_safety_warning (
	safety_warning_id 		varchar2(40)	not null,
	product_safety_warning	varchar2(360)	null,
	constraint tru_product_safety_warning_pk primary key(safety_warning_id)
);
  
create table tru_product_safety_warning_rel (
	product_id 		varchar2(40)	not null,
	sequence_num 	integer	not null,
	safety_warning_id	varchar2(40)	null,
	constraint tru_prdt_safety_wrng_rel_pk primary key(product_id,sequence_num),
	constraint tru_prdt_safety_wrng_rel_fk1 foreign key(product_id)references tru_product(product_id),
	constraint tru_prdt_safety_wrng_rel_fk2 foreign key(safety_warning_id)references tru_product_safety_warning(safety_warning_id)

);

create table tru_sku_car_seat_feature (
	car_seat_feature_id 		varchar2(40)	not null,
	car_seat_feature	varchar2(360)	null,
	constraint tru_sku_car_seat_feature_pk primary key(car_seat_feature_id)
);

create table tru_sku_car_seat_feature_rel (
	sku_id 		varchar2(40)	not null,
	sequence_num 	integer	not null,
	car_seat_feature_id	varchar2(40)	null,
	constraint tru_sku_car_st_feature_rel_pk primary key(sku_id,sequence_num),
	constraint tru_sku_car_st_feature_rel_fk1 foreign key(sku_id)references tru_sku(sku_id),
	constraint tru_sku_car_st_feature_rel_fk2 foreign key(car_seat_feature_id)references tru_sku_car_seat_feature(car_seat_feature_id)

);

create table tru_sku_car_seat_type (
	car_seat_type_id 		varchar2(40)	not null,
	car_seat_type	varchar2(360)	null,
	constraint tru_sku_car_seat_type_pk primary key(car_seat_type_id)
);
create table tru_sku_car_seat_type_rel (
	sku_id 		varchar2(40)	not null,
	sequence_num 	integer	not null,
	car_seat_type_id	varchar2(40)	null,
	constraint tru_sku_car_seat_type_rel_pk primary key(sku_id,sequence_num),
	constraint tru_sku_car_seat_type_rel_fk1 foreign key(sku_id)references tru_sku(sku_id),
	constraint tru_sku_car_seat_type_rel_fk2 foreign key(car_seat_type_id)references tru_sku_car_seat_type(car_seat_type_id)

);

create table tru_sku_car_seat_whatisimp (
	car_seat_whatisimp_id 		varchar2(40)	not null,
	car_seat_whatisimp	varchar2(360)	null,
	constraint tru_sku_car_seat_whatisimp_pk primary key(car_seat_whatisimp_id)
);

create table tru_sku_car_seat_whatisimp_rel (
	sku_id 		varchar2(40)	not null,
	sequence_num 	integer	not null,
	car_seat_whatisimp_id	varchar2(40)	null,
	constraint tru_sku_car_st_wtisimp_rel_pk primary key(sku_id,sequence_num),
	constraint tru_sku_car_st_wtisimp_rel_fk1 foreign key(sku_id)references tru_sku(sku_id),
	constraint tru_sku_car_st_wtisimp_rel_fk2 foreign key(car_seat_whatisimp_id)references tru_sku_car_seat_whatisimp(car_seat_whatisimp_id)

);

create table tru_sku_crib_type (
	crib_type_id 		varchar2(40)	not null,
	crib_type	varchar2(360)	null,
	constraint tru_sku_crib_type_pk primary key(crib_type_id)
);
create table tru_sku_crib_type_rel (
	sku_id 		varchar2(40)	not null,
	sequence_num 	integer	not null,
	crib_type_id	varchar2(40)	null,
	constraint tru_sku_crib_type_rel_pk primary key(sku_id,sequence_num),
	constraint tru_sku_crib_type_rel_fk1 foreign key(sku_id)references tru_sku(sku_id),
	constraint tru_sku_crib_type_rel_fk2 foreign key(crib_type_id)references tru_sku_crib_type(crib_type_id)

);

create table tru_sku_crib_style (
	crib_style_id 		varchar2(40)	not null,
	crib_style	varchar2(360)	null,
	constraint tru_sku_crib_style_pk primary key(crib_style_id)
);
create table tru_sku_crib_style_rel (
	sku_id 		varchar2(40)	not null,
	sequence_num 	integer	not null,
	crib_style_id	varchar2(40)	null,
	constraint tru_sku_crib_style_rel_pk primary key(sku_id,sequence_num),
	constraint tru_sku_crib_style_rel_fk1 foreign key(sku_id)references tru_sku(sku_id),
	constraint tru_sku_crib_style_rel_fk2 foreign key(crib_style_id)references tru_sku_crib_style(crib_style_id)

);

create table tru_sku_crib_material_type (
	crib_material_type_id 		varchar2(40)	not null,
	crib_material_type	varchar2(360)	null,
	constraint tru_sku_crib_material_type_pk primary key(crib_material_type_id)
);
create table tru_sku_crib_material_type_rel (
	sku_id 		varchar2(40)	not null,
	sequence_num 	integer	not null,
	crib_material_type_id	varchar2(40)	null,
	constraint tru_sku_crib_mtrl_type_rel_pk primary key(sku_id,sequence_num),
	constraint tru_sku_crib_mtrl_type_rel_fk1 foreign key(sku_id)references tru_sku(sku_id),
	constraint tru_sku_crib_mtrl_type_rel_fk2 foreign key(crib_material_type_id)references tru_sku_crib_material_type(crib_material_type_id)

);


create table tru_sku_crib_whatisimp (
	crib_whatisimp_id 		varchar2(40)	not null,
	crib_whatisimp	varchar2(360)	null,
	constraint tru_sku_crib_whatisimp_pk primary key(crib_whatisimp_id)
);
create table tru_sku_crib_whatisimp_rel (
	sku_id 		varchar2(40)	not null,
	sequence_num 	integer	not null,
	crib_whatisimp_id	varchar2(40)	null,
	constraint tru_sku_crib_whatisimp_rel_pk primary key(sku_id,sequence_num),
	constraint tru_sku_crib_whatisimp_rel_fk1 foreign key(sku_id)references tru_sku(sku_id),
	constraint tru_sku_crib_whatisimp_rel_fk2 foreign key(crib_whatisimp_id)references tru_sku_crib_whatisimp(crib_whatisimp_id)

);

create table tru_sku_strl_best_use (
	strl_best_use_id 		varchar2(40)	not null,
	strl_best_use	varchar2(360)	null,
	constraint tru_sku_strl_best_use_pk primary key(strl_best_use_id)
);
create table tru_sku_strl_best_use_rel (
	sku_id 		varchar2(40)	not null,
	sequence_num 	integer	not null,
	strl_best_use_id	varchar2(40)	null,
	constraint tru_sku_strl_best_use_rel_pk primary key(sku_id,sequence_num),
	constraint tru_sku_strl_best_use_rel_fk1 foreign key(sku_id)references tru_sku(sku_id),
	constraint tru_sku_strl_best_use_rel_fk2 foreign key(strl_best_use_id)references tru_sku_strl_best_use(strl_best_use_id)

);


create table tru_sku_strl_type (
	strl_type_id 		varchar2(40)	not null,
	strl_type	varchar2(360)	null,
	constraint tru_sku_strl_type_pk primary key(strl_type_id)
);

create table tru_sku_strl_type_rel (
	sku_id 		varchar2(40)	not null,
	sequence_num 	integer	not null,
	strl_type_id	varchar2(40)	null,
	constraint tru_sku_strl_type_rel_pk primary key(sku_id,sequence_num),
	constraint tru_sku_strl_type_rel_fk1 foreign key(sku_id)references tru_sku(sku_id),
	constraint tru_sku_strl_type_rel_fk2 foreign key(strl_type_id)references tru_sku_strl_type(strl_type_id)

);

create table tru_sku_strl_extra (
	strl_extra_id 		varchar2(40)	not null,
	strl_extra	varchar2(360)	null,
	constraint tru_sku_strl_extra_pk primary key(strl_extra_id)
);
create table tru_sku_strl_extra_rel (
	sku_id 		varchar2(40)	not null,
	sequence_num 	integer	not null,
	strl_extra_id	varchar2(40)	null,
	constraint tru_sku_strl_extra_rel_pk primary key(sku_id,sequence_num),
	constraint tru_sku_strl_extra_rel_fk1 foreign key(sku_id)references tru_sku(sku_id),
	constraint tru_sku_strl_extra_rel_fk2 foreign key(strl_extra_id)references tru_sku_strl_extra(strl_extra_id)

);


create table tru_sku_strl_whatisimp (
	strl_whatisimp_id 		varchar2(40)	not null,
	strl_whatisimp	varchar2(360)	null,
	constraint tru_sku_strl_whatisimp_pk primary key(strl_whatisimp_id)
);
create table tru_sku_strl_whatisimp_rel (
	sku_id 		varchar2(40)	not null,
	sequence_num 	integer	not null,
	strl_whatisimp_id	varchar2(40)	null,
	constraint tru_sku_strl_whatisimp_rel_pk primary key(sku_id,sequence_num),
	constraint tru_sku_strl_whatisimp_rel_fk1 foreign key(sku_id)references tru_sku(sku_id),
	constraint tru_sku_strl_whatisimp_rel_fk2 foreign key(strl_whatisimp_id)references tru_sku_strl_whatisimp(strl_whatisimp_id)

);