ALTER TABLE TRU_SKU
DROP ( 
	product_feature_1,
	product_feature_2,
	product_feature_3,
	product_feature_4,
	product_feature_5
);

create table tru_sku_product_features (
	sku_id 		varchar2(40)	not null,
	sequence_num 	integer	not null,
	product_feature	varchar2(255)	null,
	constraint tru_sku_prdt_features_pk primary key(sku_id,sequence_num),
	constraint tru_sku_prdt_features_rel_fk foreign key(sku_id)references tru_sku(sku_id)
);

ALTER TABLE TRU_BATTERIES_INFO
DROP ( 
	BAT_UID
);

create table tru_sku_battery_uids (
	sku_id 		varchar2(40)	not null,
	sequence_num 		integer	not null,
	battery_uid	varchar2(40)	null,
	constraint tru_sku_battery_uids_pk primary key(sku_id,sequence_num),
	constraint tru_sku_battery_uids_rel_fk foreign key(sku_id)references tru_sku(sku_id)
);