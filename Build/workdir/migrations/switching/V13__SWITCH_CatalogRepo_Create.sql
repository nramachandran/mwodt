
CREATE TABLE tru_catalog (
	catalog_id 		varchar2(254)	NOT NULL REFERENCES dcs_catalog(catalog_id),
	root_nav_cat 		varchar2(254)	NULL REFERENCES dcs_category(category_id),
	PRIMARY KEY(catalog_id)
);