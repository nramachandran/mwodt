<!-- Begin contacts Gear display -->
<%@ taglib uri="/contacts-taglib" prefix="contacts" %>
<%@ taglib uri="/core-taglib" prefix="core" %>
<%@ taglib uri="/paf-taglib" prefix="paf" %>
<%@ taglib uri="/dsp" prefix="dsp" %>

<dsp:page>
<card id="about" title="About">
  <p>This will be used for help information about the contacts gear.</p>
</card>
</dsp:page>
<%-- @version $Id: //app/portal/version/11.1/contacts/contacts.war/wml/aboutContacts.jsp#1 $$Change: 875535 $--%>
