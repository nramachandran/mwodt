<%@ page language="java" import="java.io.*,java.util.*,org.w3c.dom.*" %>
<%@ page import="atg.servlet.*" %>
<%
/*<ORACLECOPYRIGHT>
 * Copyright (C) 1994-2014 Oracle and/or its affiliates. All rights reserved.
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates. 
 * Other names may be trademarks of their respective owners.
 * UNIX is a registered trademark of The Open Group.
 *
 * This software and related documentation are provided under a license agreement 
 * containing restrictions on use and disclosure and are protected by intellectual property laws. 
 * Except as expressly permitted in your license agreement or allowed by law, you may not use, copy, 
 * reproduce, translate, broadcast, modify, license, transmit, distribute, exhibit, perform, publish, 
 * or display any part, in any form, or by any means. Reverse engineering, disassembly, 
 * or decompilation of this software, unless required by law for interoperability, is prohibited.
 *
 * The information contained herein is subject to change without notice and is not warranted to be error-free. 
 * If you find any errors, please report them to us in writing.
 *
 * U.S. GOVERNMENT RIGHTS Programs, software, databases, and related documentation and technical data delivered to U.S. 
 * Government customers are "commercial computer software" or "commercial technical data" pursuant to the applicable 
 * Federal Acquisition Regulation and agency-specific supplemental regulations. 
 * As such, the use, duplication, disclosure, modification, and adaptation shall be subject to the restrictions and 
 * license terms set forth in the applicable Government contract, and, to the extent applicable by the terms of the 
 * Government contract, the additional rights set forth in FAR 52.227-19, Commercial Computer Software License 
 * (December 2007). Oracle America, Inc., 500 Oracle Parkway, Redwood City, CA 94065.
 *
 * This software or hardware is developed for general use in a variety of information management applications. 
 * It is not developed or intended for use in any inherently dangerous applications, including applications that 
 * may create a risk of personal injury. If you use this software or hardware in dangerous applications, 
 * then you shall be responsible to take all appropriate fail-safe, backup, redundancy, 
 * and other measures to ensure its safe use. Oracle Corporation and its affiliates disclaim any liability for any 
 * damages caused by use of this software or hardware in dangerous applications.
 *
 * This software or hardware and documentation may provide access to or information on content, 
 * products, and services from third parties. Oracle Corporation and its affiliates are not responsible for and 
 * expressly disclaim all warranties of any kind with respect to third-party content, products, and services. 
 * Oracle Corporation and its affiliates will not be responsible for any loss, costs, 
 * or damages incurred due to your access to or use of third-party content, products, or services.
 </ORACLECOPYRIGHT>*/


 /**
  *  This page handles the display for shared page gear views.
  *  It only displays headlines.
  **/

%>

<%@ taglib uri="/paf-taglib" prefix="paf" %>
<%@ taglib uri="/core-taglib" prefix="core" %>
<%@ taglib uri="/dsp" prefix="dsp" %>
<%@ taglib uri="/xmlprotocoltaglib" prefix="mt" %>
<%@ taglib uri="/jakarta-i18n-1.0" prefix="i18n" %>

<dsp:page>
<dsp:importbean bean="/atg/dynamo/droplet/xml/XMLTransform"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>


<paf:InitializeGearEnvironment id="pafEnv">

<i18n:bundle baseName="atg.portal.gear.xmlprotocol.ContentResource" localeAttribute="userLocale" changeResponseLocale="false" />

<%
   /*
    XMLTransform uses getOutputStream() directly, and the J2EE spec says that you
    are not supposed to mix it with getWriter().  Explicitly disable it.
   */
   DynamoHttpServletRequest dynRequest = ServletUtil.getDynamoRequest(request);
   DynamoHttpServletResponse dynResponse = dynRequest.getResponse();
   dynResponse.setStrictOutputAccess(false);

   /**
    * Represents the Url to this instance of the controller.  We pass this value to the 
    * headlines stylesheet which preprends it to each article reference.  
    * The value is calculated below.
    **/
   
   String articleGearUrl="";   
   
   /**
    * The stylesheet to render headlines...
    **/
   String headlinesStylesheetUrl=pafEnv.getGearInstanceParameter("sharedHeadlinesStylesheetUrl");

%>

<core:CreateUrl id="theUrl" url="<%=pafEnv.getOriginalRequestURI()%>">
 <core:UrlParam param="xmlprotocol_action" value="getarticle"/>
 <core:UrlParam param="paf_dm" value="full"/>
 <core:UrlParam param="paf_gear_id" value="<%=pafEnv.getGear().getId()%>"/>
 <% articleGearUrl=theUrl.getNewUrl();%>
</core:CreateUrl>

<mt:XMLHeadlines id="theObj" pafEnv="<%=pafEnv%>" categoryFilter="true">
  <dsp:param name="xmlInput" value="<%=theObj.getXMLHeadlines()%>"/>
  <dsp:droplet name="XMLTransform">
    <dsp:param name="input" param="xmlInput"/>
    <dsp:param name="validate" value="false"/>
    <dsp:param name="template" value="<%=headlinesStylesheetUrl%>"/>
    <dsp:param name="passParams" value="local"/>
    <dsp:param name="articleGearUrl" value="<%=articleGearUrl%>"/>
    <dsp:param name="headlinesUrl" value="this was passed as a parameter!"/>
    <dsp:oparam name="failure">
      <i18n:message key="error_xform_docs"/>
    </dsp:oparam>
  </dsp:droplet>
</mt:XMLHeadlines>

<core:CreateUrl id="fullGearUrl" url="<%=pafEnv.getOriginalRequestURI()%>">
 <core:UrlParam param="paf_dm" value="full"/>
 <core:UrlParam param="paf_gear_id" value="<%=pafEnv.getGear().getId()%>"/>
 <a href="<%= fullGearUrl.getNewUrl() %>"><i18n:message key="full_screen_link" /></a>
</core:CreateUrl>

<core:CreateUrl id="fullGearUrl" url="<%=pafEnv.getOriginalRequestURI()%>">
 <core:UrlParam param="xmlprotocol_action" value="getcategories"/>
 <core:UrlParam param="paf_dm" value="full"/>
 <core:UrlParam param="paf_gear_id" value="<%=pafEnv.getGear().getId()%>"/>
 <a href="<%= fullGearUrl.getNewUrl() %>"><i18n:message key="news_cat_link" /></a>
</core:CreateUrl>

</paf:InitializeGearEnvironment>
</dsp:page>
<%-- @version $Id: //app/portal/version/11.1/xmlprotocol/xmlprotocol.war/sharedfeed.jsp#2 $$Change: 878657 $--%>
