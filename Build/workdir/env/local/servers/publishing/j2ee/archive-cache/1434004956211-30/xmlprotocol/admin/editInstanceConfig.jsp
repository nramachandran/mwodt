<%@ page language="java" import="java.io.*,java.util.*,org.w3c.dom.*,java.util.ArrayList" %>
 
<%
/*<ORACLECOPYRIGHT>
 * Copyright (C) 1994-2014 Oracle and/or its affiliates. All rights reserved.
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates. 
 * Other names may be trademarks of their respective owners.
 * UNIX is a registered trademark of The Open Group.
 *
 * This software and related documentation are provided under a license agreement 
 * containing restrictions on use and disclosure and are protected by intellectual property laws. 
 * Except as expressly permitted in your license agreement or allowed by law, you may not use, copy, 
 * reproduce, translate, broadcast, modify, license, transmit, distribute, exhibit, perform, publish, 
 * or display any part, in any form, or by any means. Reverse engineering, disassembly, 
 * or decompilation of this software, unless required by law for interoperability, is prohibited.
 *
 * The information contained herein is subject to change without notice and is not warranted to be error-free. 
 * If you find any errors, please report them to us in writing.
 *
 * U.S. GOVERNMENT RIGHTS Programs, software, databases, and related documentation and technical data delivered to U.S. 
 * Government customers are "commercial computer software" or "commercial technical data" pursuant to the applicable 
 * Federal Acquisition Regulation and agency-specific supplemental regulations. 
 * As such, the use, duplication, disclosure, modification, and adaptation shall be subject to the restrictions and 
 * license terms set forth in the applicable Government contract, and, to the extent applicable by the terms of the 
 * Government contract, the additional rights set forth in FAR 52.227-19, Commercial Computer Software License 
 * (December 2007). Oracle America, Inc., 500 Oracle Parkway, Redwood City, CA 94065.
 *
 * This software or hardware is developed for general use in a variety of information management applications. 
 * It is not developed or intended for use in any inherently dangerous applications, including applications that 
 * may create a risk of personal injury. If you use this software or hardware in dangerous applications, 
 * then you shall be responsible to take all appropriate fail-safe, backup, redundancy, 
 * and other measures to ensure its safe use. Oracle Corporation and its affiliates disclaim any liability for any 
 * damages caused by use of this software or hardware in dangerous applications.
 *
 * This software or hardware and documentation may provide access to or information on content, 
 * products, and services from third parties. Oracle Corporation and its affiliates are not responsible for and 
 * expressly disclaim all warranties of any kind with respect to third-party content, products, and services. 
 * Oracle Corporation and its affiliates will not be responsible for any loss, costs, 
 * or damages incurred due to your access to or use of third-party content, products, or services.
 </ORACLECOPYRIGHT>*/

 
 /** 
  * Second step in instance configuration of the gear
  **/
 
%> 
<%@ taglib uri="/paf-taglib" prefix="paf" %>
<%@ taglib uri="/core-taglib" prefix="core" %>
<%@ taglib uri="/dsp" prefix="dsp" %>
<%@ taglib uri="/xmlprotocoltaglib" prefix="mt" %>
<%@ taglib uri="/jakarta-i18n-1.0" prefix="i18n" %>

<dsp:page>
<dsp:importbean bean="/atg/portal/gear/xmlprotocol/XmlProtocolInstanceFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>


<paf:InitializeGearEnvironment id="pafEnv">
<i18n:bundle baseName="atg.portal.gear.xmlprotocol.ContentResource" localeAttribute="userLocale" changeResponseLocale="false" />


<%
  String successUrl=null;
  String errorUrl=null;
  String cancelUrl=null;
  String thisFormUrl=null;
  String origURI= pafEnv.getOriginalRequestURI();
  String gearID = pafEnv.getGear().getId();
  String pageID = request.getParameter("pageId");
  String pageURL = request.getParameter("pageURL");
  String communityID = request.getParameter("communityId");
  
  //set up all of our instance variables...
  
  String instanceUserID = pafEnv.getGearInstanceParameter("instanceUserID");
  String instancePassword = pafEnv.getGearInstanceParameter("instancePassword");
  String authenticationUrl = pafEnv.getGearInstanceParameter("authenticationUrl");
  String articleUrl = pafEnv.getGearInstanceParameter("articleUrl");
  			
  String headlinesUrl = pafEnv.getGearInstanceParameter("headlinesUrl");
  String categoriesUrl = pafEnv.getGearInstanceParameter("categoriesUrl");
  String feedAdaptor = pafEnv.getGearInstanceParameter("feedAdaptor");
  String fullArticleStylesheetUrl = pafEnv.getGearInstanceParameter("fullArticleStylesheetUrl");
  String fullHeadlinesStylesheetUrl = pafEnv.getGearInstanceParameter("fullHeadlinesStylesheetUrl");
  String fullCategoriesStylesheetUrl = pafEnv.getGearInstanceParameter("fullCategoriesStylesheetUrl");
  String sharedHeadlinesStylesheetUrl = pafEnv.getGearInstanceParameter("sharedHeadlinesStylesheetUrl");
  String sharedCategoriesStylesheetUrl = pafEnv.getGearInstanceParameter("sharedCategoriesStylesheetUrl");
  
%>

<i18n:message id="submitButton" key="submit_button" />
<i18n:message id="cancelButton" key="cancel_button" />

  <core:CreateUrl id="theUrl" url="<%= pafEnv.getOriginalRequestURI() %>">
    <core:UrlParam param="paf_gear_id" value="<%= pafEnv.getGear().getId() %>"/>
    <core:UrlParam param="paf_community_id" value='<%= request.getParameter("paf_community_id") %>'/>
    <core:UrlParam param="paf_dm" value="<%=pafEnv.getDisplayMode() %>"/>
    <core:UrlParam param="paf_gm" value="instanceConfig"/>
    <core:UrlParam param="msg" value="success" />
    <%successUrl=theUrl.getNewUrl();%>
  </core:CreateUrl>

  <core:CreateUrl id="theUrl3" url="<%= pafEnv.getOriginalRequestURI() %>">
    <core:UrlParam param="paf_gear_id" value="<%= pafEnv.getGear().getId() %>"/>
    <core:UrlParam param="paf_community_id" value='<%= request.getParameter("paf_community_id") %>'/>
    <core:UrlParam param="paf_dm" value="<%=pafEnv.getDisplayMode() %>"/>
    <core:UrlParam param="paf_gm" value="instanceConfig"/>
    <core:UrlParam param="msg" value="cancel" />
    <%cancelUrl=theUrl3.getNewUrl();%>
  </core:CreateUrl>


  <core:CreateUrl id="theUrl2" url="<%= pafEnv.getOriginalRequestURI() %>">
    <core:UrlParam param="paf_gear_id" value="<%= pafEnv.getGear().getId() %>"/>
    <core:UrlParam param="paf_community_id" value='<%= request.getParameter("paf_community_id") %>'/>
    <core:UrlParam param="paf_page_id" value='<%= request.getParameter("paf_page_id") %>'/>
    <core:UrlParam param="paf_page_url" value='<%= request.getParameter("paf_page_url") %>'/>
    <core:UrlParam param="paf_dm" value="<%=pafEnv.getDisplayMode() %>"/>
    <core:UrlParam param="paf_gm" value="instanceConfig"/>
    <core:UrlParam param="config_page" value="DisplayText"/>
    <core:UrlParam param="msg" value="error" />
    <%errorUrl=theUrl2.getNewUrl();%>
  </core:CreateUrl>


<dsp:setvalue bean="XmlProtocolInstanceFormHandler.instanceUserID" value="<%= instanceUserID %>"/>
<dsp:setvalue bean="XmlProtocolInstanceFormHandler.instancePassword" value="<%= instancePassword %>"/>

<dsp:setvalue bean="XmlProtocolInstanceFormHandler.authenticationUrl" value="<%= authenticationUrl %>"/>
<dsp:setvalue bean="XmlProtocolInstanceFormHandler.articleUrl" value="<%= articleUrl %>"/>

<dsp:setvalue bean="XmlProtocolInstanceFormHandler.headlinesUrl" value="<%= headlinesUrl %>"/>
<dsp:setvalue bean="XmlProtocolInstanceFormHandler.categoriesUrl" value="<%= categoriesUrl %>"/>
<dsp:setvalue bean="XmlProtocolInstanceFormHandler.feedAdaptor" value="<%= feedAdaptor %>"/>
<dsp:setvalue bean="XmlProtocolInstanceFormHandler.fullHeadlinesStylesheetUrl" value="<%= fullHeadlinesStylesheetUrl %>"/>
<dsp:setvalue bean="XmlProtocolInstanceFormHandler.fullArticleStylesheetUrl" value="<%= fullArticleStylesheetUrl %>"/>
<dsp:setvalue bean="XmlProtocolInstanceFormHandler.fullCategoriesStylesheetUrl" value="<%= fullCategoriesStylesheetUrl %>"/>
<dsp:setvalue bean="XmlProtocolInstanceFormHandler.sharedHeadlinesStylesheetUrl" value="<%= sharedHeadlinesStylesheetUrl %>"/>
<dsp:setvalue bean="XmlProtocolInstanceFormHandler.sharedCategoriesStylesheetUrl" value="<%= sharedCategoriesStylesheetUrl %>"/>


  <dsp:form enctype="multipart/form-data" method="post" action="<%=origURI%>">

  
  <dsp:input type="hidden" bean="XmlProtocolInstanceFormHandler.successUrl" value="<%= successUrl %>" /> 
  

  <input type="hidden" name="paf_gear_id" value="<%= pafEnv.getGear().getId() %>"/>
  <input type="hidden" name="paf_dm" value="<%=pafEnv.getDisplayMode() %>"/>
  <input type="hidden" name="paf_gm" value="<%=pafEnv.getGearMode() %>"/>
  <input type="hidden" name="paf_community_id" value="<%= communityID %>"/>
  

  <table>  

    <dsp:droplet name="ErrorMessageForEach">
      <dsp:param name="exceptions" bean="XmlProtocolInstanceFormHandler.formExceptions"/>
      <dsp:oparam name="outputStart"><tr></dsp:oparam>
      <dsp:oparam name="output">
        <td><font style="color: red"><dsp:valueof param="message">No
        message</dsp:valueof></font></td>
      </dsp:oparam>
      <dsp:oparam name="outputEnd"></tr></dsp:oparam>
    </dsp:droplet>

    <tr>
      <td><b><i18n:message key="instance_config_label" /></b></td>
    </tr>
    <tr/>
    <tr/>
    <tr>
      <td NOWRAP align="left"><font size="-1" ><b><i18n:message key="inst_auth" /></b><font></td>
      <td/>
    <tr>
      <td NOWRAP align="left"><font size="-1" ><i18n:message key="user_id" /><font></td>
      <td><font size="-1"><dsp:input type="text" size="20" bean="XmlProtocolInstanceFormHandler.instanceUserID"/></font></td>
    </tr>
    <tr>
      <td NOWRAP align="left"><font size="-1" ><i18n:message key="password" /><font></td>
      <td><font size="-1"><dsp:input type="text" size="20" bean="XmlProtocolInstanceFormHandler.instancePassword"/></font></td>
    </tr>
    <tr/>
    <tr>
      <td NOWRAP align="left"><font size="-1" ><b><i18n:message key="service_config" /></b><font></td>
      <td/>
    </tr>

    <tr>
      <td NOWRAP align="left"><font size="-1" ><i18n:message key="service_provider" /><font></td>
    
      
          <td><font size="-1">
       	  <dsp:input type="text"  bean="XmlProtocolInstanceFormHandler.feedAdaptor" />
       	  
          </td>
      
     </tr>
    <tr>
      <td NOWRAP align="left"><font size="-1" ><i18n:message key="auth_url" /><font></td>
      <td><font size="-1"><dsp:input type="text" size="60" bean="XmlProtocolInstanceFormHandler.authenticationUrl"/></font></td>
    </tr>

    <tr>
      <td NOWRAP align="left"><font size="-1" ><i18n:message key="cat_url" /><font></td>
      <td><font size="-1"><dsp:input type="text" size="60" bean="XmlProtocolInstanceFormHandler.categoriesUrl"/></font></td>
    </tr>
    <tr>
      <td NOWRAP align="left"><font size="-1" ><i18n:message key="headlines_url" /><font></td>
      <td><font size="-1"><dsp:input type="text" size="60" bean="XmlProtocolInstanceFormHandler.headlinesUrl"/></font></td>
    </tr>
    <tr/>
    <tr>
      <td NOWRAP align="left"><font size="-1" ><i18n:message key="article_url" /><font></td>
      <td><font size="-1"><dsp:input type="text" size="60" bean="XmlProtocolInstanceFormHandler.articleUrl"/></font></td>
    </tr>
    <tr/>
    <tr>
      <td NOWRAP align="left"><font size="-1" ><b><i18n:message key="feed_disp_config" /></b><font></td>
      <td/>
    </tr>
    <tr>
      <td NOWRAP align="left"><font size="-1" ><i18n:message key="full_cat_style" /><font></td>
      <td><font size="-1"><dsp:input type="text" size="60" bean="XmlProtocolInstanceFormHandler.fullCategoriesStylesheetUrl"/></font></td>
    </tr>
    <tr>
      <td NOWRAP align="left"><font size="-1" ><i18n:message key="full_article_style" /><font></td>
      <td><font size="-1"><dsp:input type="text" size="60" bean="XmlProtocolInstanceFormHandler.fullArticleStylesheetUrl"/></font></td>
    </tr>
    <tr>
      <td NOWRAP align="left"><font size="-1" ><i18n:message key="full_headlines_style" /><font></td>
      <td><font size="-1"><dsp:input type="text" size="60" bean="XmlProtocolInstanceFormHandler.fullHeadlinesStylesheetUrl"/></font></td>
    </tr>
    <tr>
      <td NOWRAP align="left"><font size="-1" ><i18n:message key="shared_cat_style" /><font></td>
      <td><font size="-1"><dsp:input type="text" size="60" bean="XmlProtocolInstanceFormHandler.sharedCategoriesStylesheetUrl"/></font></td>
    </tr>
    <tr>
      <td NOWRAP align="left"><font size="-1" ><i18n:message key="shared_headlines_style" /><font></td>
      <td><font size="-1"><dsp:input type="text" size="60" bean="XmlProtocolInstanceFormHandler.sharedHeadlinesStylesheetUrl"/></font></td>
    </tr>

    
    <tr>
      <td>
	  <%-- Set up the gear id. --%>
	  <dsp:input type="hidden" bean="XmlProtocolInstanceFormHandler.gearId" value="<%= pafEnv.getGear().getId() %>"/>

	  <%-- Set up the error url. --%>
          
          <dsp:input type="hidden" bean="XmlProtocolInstanceFormHandler.errorUrl" value="<%= errorUrl %>"/>

	  <dsp:input type="hidden" bean="XmlProtocolInstanceFormHandler.cancelURL" value="<%= cancelUrl %>"/>
          
	  </td><td>
	  <dsp:input type="submit" bean="XmlProtocolInstanceFormHandler.submit" value="<%= submitButton %>"/>
	  <dsp:input type="submit" bean="XmlProtocolInstanceFormHandler.cancel" value="<%= cancelButton %>"/>
	</dsp:form>
    </td>
  </tr>
  </table>


</paf:InitializeGearEnvironment>
</dsp:page>
<%-- @version $Id: //app/portal/version/11.1/xmlprotocol/xmlprotocol.war/admin/editInstanceConfig.jsp#2 $$Change: 878657 $--%>
