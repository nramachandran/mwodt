<%-- @version $Id: //app/portal/version/11.1/paf/settings.war/gears.jsp#1 $$Change: 875535 $ --%>
<%@ taglib uri="/paf-taglib" prefix="paf" %>
<%@ taglib uri="/core-taglib" prefix="core" %>
<%@ taglib uri="/admin-taglib" prefix="admin" %>
<%@ taglib uri="/dsp" prefix="dsp" %>

<%@ taglib uri="/jakarta-i18n-1.0" prefix="i18n" %>

<paf:setFrameworkLocale />
<dsp:page>
<paf:RegisteredUserBarrier/>
<%-- this file is for backward compatibility --%>

<%@include file="user.jsp" %>

</dsp:page>
<%-- @version $Id: //app/portal/version/11.1/paf/settings.war/gears.jsp#1 $$Change: 875535 $--%>
