<%@ taglib uri="/dspTaglib" prefix="dsp" %>
<%@ page import="atg.servlet.*" %>
<dsp:page>

<dsp:importbean bean="/atg/dynamo/Configuration"/>

<head><title>Dynamo Test Child JSP Page</title></head>
<body>
<h1>Dynamo Test Child JSP Page</h1>
<p>
<%
  DynamoHttpServletRequest drequest = ServletUtil.getDynamoRequest(request);

  // get profile
  Object o = drequest.resolveName("/atg/userprofiling/Profile");
  if (o == null) out.print("DPS is not installed!");
  else out.println("Your profile is: " + o + "<br>");

  // get session ID
  out.println("Your session ID is: " + session.getId());
%>
</dsp:page>
<%-- @version $Id: //product/DAS/version/11.1/templates/DAF/j2ee-apps/sshare/test.jsp#1 $$Change: 875535 $--%>
