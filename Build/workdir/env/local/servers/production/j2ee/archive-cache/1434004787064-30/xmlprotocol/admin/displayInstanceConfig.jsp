<%@ page language="java" %>
<%
/*<ORACLECOPYRIGHT>
 * Copyright (C) 1994-2014 Oracle and/or its affiliates. All rights reserved.
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates. 
 * Other names may be trademarks of their respective owners.
 * UNIX is a registered trademark of The Open Group.
 *
 * This software and related documentation are provided under a license agreement 
 * containing restrictions on use and disclosure and are protected by intellectual property laws. 
 * Except as expressly permitted in your license agreement or allowed by law, you may not use, copy, 
 * reproduce, translate, broadcast, modify, license, transmit, distribute, exhibit, perform, publish, 
 * or display any part, in any form, or by any means. Reverse engineering, disassembly, 
 * or decompilation of this software, unless required by law for interoperability, is prohibited.
 *
 * The information contained herein is subject to change without notice and is not warranted to be error-free. 
 * If you find any errors, please report them to us in writing.
 *
 * U.S. GOVERNMENT RIGHTS Programs, software, databases, and related documentation and technical data delivered to U.S. 
 * Government customers are "commercial computer software" or "commercial technical data" pursuant to the applicable 
 * Federal Acquisition Regulation and agency-specific supplemental regulations. 
 * As such, the use, duplication, disclosure, modification, and adaptation shall be subject to the restrictions and 
 * license terms set forth in the applicable Government contract, and, to the extent applicable by the terms of the 
 * Government contract, the additional rights set forth in FAR 52.227-19, Commercial Computer Software License 
 * (December 2007). Oracle America, Inc., 500 Oracle Parkway, Redwood City, CA 94065.
 *
 * This software or hardware is developed for general use in a variety of information management applications. 
 * It is not developed or intended for use in any inherently dangerous applications, including applications that 
 * may create a risk of personal injury. If you use this software or hardware in dangerous applications, 
 * then you shall be responsible to take all appropriate fail-safe, backup, redundancy, 
 * and other measures to ensure its safe use. Oracle Corporation and its affiliates disclaim any liability for any 
 * damages caused by use of this software or hardware in dangerous applications.
 *
 * This software or hardware and documentation may provide access to or information on content, 
 * products, and services from third parties. Oracle Corporation and its affiliates are not responsible for and 
 * expressly disclaim all warranties of any kind with respect to third-party content, products, and services. 
 * Oracle Corporation and its affiliates will not be responsible for any loss, costs, 
 * or damages incurred due to your access to or use of third-party content, products, or services.
 </ORACLECOPYRIGHT>*/

 
 /** 
  * Main entry pooint for instance configuration of the gear
  **/
 
%> 
<%@ taglib uri="/dsp" prefix="dsp" %>
<%@ taglib uri="/core-taglib" prefix="core" %>
<%@ taglib uri="/paf-taglib" prefix="paf" %>
<%@ taglib uri="/jakarta-i18n-1.0" prefix="i18n" %>

<dsp:page>
<paf:InitializeGearEnvironment id="pafEnv">

<i18n:bundle baseName="atg.portal.gear.xmlprotocol.ContentResource" localeAttribute="userLocale" changeResponseLocale="false" />

<% String origURI= pafEnv.getOriginalRequestURI();
   String gearID = pafEnv.getGear().getId();
   String pageID = request.getParameter("paf_page_id");
   String pageURL = request.getParameter("paf_page_url");
   String communityID = request.getParameter("paf_community_id");

   String clearGif = response.encodeURL("images/clear.gif");
   String infoGif = response.encodeURL("images/info.gif");
 %>

  <TABLE WIDTH="667" BORDER="0" CELLSPACING="0" CELLPADDING="0">
    
    <tr>
     <td/>
    </tr>
 
    
<tr><td>
<dsp:droplet name="/atg/dynamo/droplet/Switch">
  <dsp:param name="value" value='<%= request.getParameter("msg") %>'/>
  <dsp:oparam name="success"><font class="info"><img src="<%= infoGif %>">&nbsp;&nbsp;<i18n:message key="changes_updated" /></font></dsp:oparam>
  <dsp:oparam name="cancel"><font class="info"><img src="<%= infoGif %>">&nbsp;&nbsp;<i18n:message key="changes_not_saved" /></font></dsp:oparam>
  <dsp:oparam name="error"><font class="error"><i18n:message key="changes_error" /></font></dsp:oparam>
</dsp:droplet>
</td></tr>

    <tr/>
    <tr/>
    <tr/>
    <tr>
      <td colspan=2>
      <core:CreateUrl id="editGearUrl" url="<%= origURI %>">
        <core:UrlParam param="paf_dm" value="full"/>
        <core:UrlParam param="paf_gm" value="instanceConfig"/>
	<core:UrlParam param="config_page" value="DisplayText"/>
        <core:UrlParam param="paf_gear_id" value="<%= gearID %>"/>
        <core:UrlParam param="paf_page_id" value="<%= pageID %>"/>
        <core:UrlParam param="paf_page_url" value="<%= pageURL %>"/>
        <core:UrlParam param="paf_community_id" value="<%= communityID %>"/>
        <a href="<%= editGearUrl.getNewUrl() %>"><font size="2"><i18n:message key="edit_gear_config" /></font></a>
      </core:CreateUrl>
      </td>
    </tr>
    <tr>
      <td><font size="-2"><i18n:message key="edit_gear_config_helper" /></font></td>
      <td/>
    </tr> 
    <tr/>
    <tr>
      <td colspan=2>
      <core:CreateUrl id="editGearUrl" url="<%= origURI %>">
        <core:UrlParam param="paf_dm" value="full"/>
        <core:UrlParam param="paf_gm" value="instanceConfig"/>
	<core:UrlParam param="config_page" value="DefaultConfig"/>
        <core:UrlParam param="paf_gear_id" value="<%= gearID %>"/>
        <core:UrlParam param="paf_gear_id" value="<%= pageID %>"/>
        <core:UrlParam param="paf_page_url" value="<%= pageURL %>"/>
        <core:UrlParam param="paf_community_id" value="<%= communityID %>"/>
        <a href="<%= editGearUrl.getNewUrl() %>"><font size="2"><i18n:message key="edit_user_def" /></font></a>
      </core:CreateUrl>
      </td>
    </tr>
    <tr>
      <td><font size="-2"><i18n:message key="edit_user_def_helper" /></font></td>
      <td/>
    </tr> 
    <tr/>
    


  </TABLE>


</paf:InitializeGearEnvironment>
</dsp:page>
<%-- @version $Id: //app/portal/version/11.1/xmlprotocol/xmlprotocol.war/admin/displayInstanceConfig.jsp#2 $$Change: 878657 $--%>
