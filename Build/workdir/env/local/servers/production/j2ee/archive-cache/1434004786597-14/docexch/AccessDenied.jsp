<%@ taglib uri="/dsp" prefix="dsp" %>
<%@ taglib uri="/core-taglib" prefix="core" %>
<%@ taglib uri="/paf-taglib" prefix="paf" %>

<dsp:page>
<paf:InitializeGearEnvironment id="pafEnv">
Access Denied.  Please use your browser's back button to return to the portals.
</paf:InitializeGearEnvironment>
</dsp:page>
<%-- @version $Id: //app/portal/version/11.1/docexch/docexch.war/AccessDenied.jsp#1 $$Change: 875535 $--%>
