<%@ page import="java.io.*,java.util.*,atg.portal.servlet.*" errorPage="/error.jsp" %>
<%@ taglib prefix="dsp"     uri="http://www.atg.com/taglibs/daf/dspjspELTaglib1_0" %>

<dsp:page>

<%
 //Obtain request/response
 GearServletResponse gearServletResponse = 
     (GearServletResponse)request.getAttribute(Attribute.GEARSERVLETRESPONSE);
 GearServletRequest gearServletRequest = 
     (GearServletRequest)request.getAttribute(Attribute.GEARSERVLETREQUEST);
%>
<p><center>Servlet Dump Portlet</center></p>
<p><center>Copyright (C) 1994-2014, Oracle and/or its affiliates. All rights reserved.</center></p>

</dsp:page>

<%-- @version $Id: //app/portal/version/11.1/paf/portal.war/portlets/servlet-dump/about/html/about.jsp#2 $$Change: 878657 $--%>
