package _dcsadmin_3._atg._commerce._admin._en._maintenance;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import atg.nucleus.*;
import atg.naming.*;
import atg.service.filecache.*;
import atg.servlet.*;
import atg.droplet.*;
import atg.servlet.pagecompile.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import atg.servlet.jsp.*;

public class _startService
extends atg.servlet.jsp.DynamoJspPageServlet implements atg.servlet.pagecompile.AttrCompiledServlet {
  public String getFileCacheAttributeName() {
    return "__002fatg_002fcommerce_002fadmin_002fen_002fmaintenance_002fstartService_xjhtml";
  }
  
  public static final long SOURCE_MODIFIED_TIME = 1401377606000L;
  static final atg.droplet.PropertyName _beanName0 = atg.droplet.PropertyName.createPropertyName("/atg/commerce/catalog/RunServiceFormHandler.repositoryPath");
  static final atg.droplet.PropertyName _beanName1 = atg.droplet.PropertyName.createPropertyName("/atg/commerce/catalog/RunServiceFormHandler.runProcessSuccessURL");
  static final atg.droplet.PropertyName _beanName2 = atg.droplet.PropertyName.createPropertyName("/atg/commerce/catalog/RunServiceFormHandler.runProcessErrorURL");
  static final atg.droplet.PropertyName _beanName3 = atg.droplet.PropertyName.createPropertyName("/atg/commerce/catalog/RunServiceFormHandler.requestedProcess");
  static final atg.droplet.PropertyName _beanName4 = atg.droplet.PropertyName.createPropertyName("/atg/commerce/catalog/RunServiceFormHandler.runProcess");
  public class _Param_0_UpdateCatalogProcess extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 14-16 */
            __fileData.writeBytes (556, 23, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_0_UpdateCatalogProcess m_Param_0_UpdateCatalogProcess = this. new _Param_0_UpdateCatalogProcess();
  {
    m_Param_0_UpdateCatalogProcess.setParent(this);
  }
  public class _Param_1_BasicMaintProcess extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 17-19 */
            __fileData.writeBytes (623, 34, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_1_BasicMaintProcess m_Param_1_BasicMaintProcess = this. new _Param_1_BasicMaintProcess();
  {
    m_Param_1_BasicMaintProcess.setParent(this);
  }
  public class _Param_2_VerifyProcess extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 20-22 */
            __fileData.writeBytes (697, 23, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_2_VerifyProcess m_Param_2_VerifyProcess = this. new _Param_2_VerifyProcess();
  {
    m_Param_2_VerifyProcess.setParent(this);
  }
  public class _SubServlet_3 extends PageSubServlet {
    {
      this.setParameter("UpdateCatalogProcess", m_Param_0_UpdateCatalogProcess);
      this.setParameter("BasicMaintProcess", m_Param_1_BasicMaintProcess);
      this.setParameter("VerifyProcess", m_Param_2_VerifyProcess);
    }
  }

  _SubServlet_3 m_SubServlet_3 = this. new _SubServlet_3();
  {
    m_SubServlet_3.setParent(this);
  }
  public class _Param_4_UpdateCatalogProcess extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 26-28 */
            __fileData.writeBytes (866, 28, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_4_UpdateCatalogProcess m_Param_4_UpdateCatalogProcess = this. new _Param_4_UpdateCatalogProcess();
  {
    m_Param_4_UpdateCatalogProcess.setParent(this);
  }
  public class _Param_5_BasicMaintProcess extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 29-31 */
            __fileData.writeBytes (938, 39, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_5_BasicMaintProcess m_Param_5_BasicMaintProcess = this. new _Param_5_BasicMaintProcess();
  {
    m_Param_5_BasicMaintProcess.setParent(this);
  }
  public class _Param_6_VerifyProcess extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 32-34 */
            __fileData.writeBytes (1017, 28, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_6_VerifyProcess m_Param_6_VerifyProcess = this. new _Param_6_VerifyProcess();
  {
    m_Param_6_VerifyProcess.setParent(this);
  }
  public class _SubServlet_7 extends PageSubServlet {
    {
      this.setParameter("UpdateCatalogProcess", m_Param_4_UpdateCatalogProcess);
      this.setParameter("BasicMaintProcess", m_Param_5_BasicMaintProcess);
      this.setParameter("VerifyProcess", m_Param_6_VerifyProcess);
    }
  }

  _SubServlet_7 m_SubServlet_7 = this. new _SubServlet_7();
  {
    m_SubServlet_7.setParent(this);
  }
  public class _Param_8_false extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 42-43 */
            __fileData.writeBytes (1243, 2, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_8_false m_Param_8_false = this. new _Param_8_false();
  {
    m_Param_8_false.setParent(this);
  }
  public class _Param_9_output extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 50-52 */
            __fileData.writeBytes (1559, 37, out);
            if (!request.serviceParameter("message", request, response, PageCompileServlet.getEscapeHTMLTagConverter(), null)) {
            }
            /*** lines: 52-54 */
            __fileData.writeBytes (1622, 19, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_9_output m_Param_9_output = this. new _Param_9_output();
  {
    m_Param_9_output.setParent(this);
  }
  public class _SubServlet_10 extends PageSubServlet {
    {
      this.setParameter("output", m_Param_9_output);
    }
  }

  _SubServlet_10 m_SubServlet_10 = this. new _SubServlet_10();
  {
    m_SubServlet_10.setParent(this);
  }
  public class _Param_11_true extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 44-48 */
            __fileData.writeBytes (1276, 120, out);
            try {
              request.pushFrame();
              request.setParameter("exceptions", DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/RunServiceFormHandler.formExceptions", true, null, null));
              m_SubServlet_10.serviceByName(request, response, "/atg/dynamo/droplet/ErrorMessageForEach", 48);
            }
            finally {
              request.popFrame();
            }
            /*** lines: 55-57 */
            __fileData.writeBytes (1664, 18, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_11_true m_Param_11_true = this. new _Param_11_true();
  {
    m_Param_11_true.setParent(this);
  }
  public class _SubServlet_12 extends PageSubServlet {
    {
      this.setParameter("false", m_Param_8_false);
      this.setParameter("true", m_Param_11_true);
    }
  }

  _SubServlet_12 m_SubServlet_12 = this. new _SubServlet_12();
  {
    m_SubServlet_12.setParent(this);
  }
  public class _Param_13_VerifyProcess extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
            /*** lines: 64-64 */
            __fileData.writeBytes (1855, 19, out);
            out.print(request.encodeURL ("/nucleus/atg/commerce/catalog/custom/CatalogVerificationService", true, true, false, true));
            request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
            /*** lines: 64-72 */
            __fileData.writeBytes (1937, 234, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.ancestorCategoriesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 72-73 */
            __fileData.writeBytes (2259, 25, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.allRootCategoriesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 73-74 */
            __fileData.writeBytes (2371, 25, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.directAncestorCatalogsAndSelfPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 74-75 */
            __fileData.writeBytes (2495, 25, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.indirectAncestorCatalogsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 75-81 */
            __fileData.writeBytes (2614, 133, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.relatedCategoriesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 81-82 */
            __fileData.writeBytes (2834, 22, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.categoryInfosPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 82-83 */
            __fileData.writeBytes (2939, 22, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.parentCategoryPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 83-89 */
            __fileData.writeBytes (3045, 128, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.relatedProductsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 89-90 */
            __fileData.writeBytes (3258, 20, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.catalogsRelatedProductsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 90-91 */
            __fileData.writeBytes (3371, 20, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.productInfosPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 91-92 */
            __fileData.writeBytes (3473, 20, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.parentCategoryPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 92-98 */
            __fileData.writeBytes (3577, 124, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.replacementProductsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 98-99 */
            __fileData.writeBytes (3790, 20, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.catalogsReplacementProductsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 99-100 */
            __fileData.writeBytes (3907, 20, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.skuInfosPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 100-105 */
            __fileData.writeBytes (4005, 79, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_13_VerifyProcess m_Param_13_VerifyProcess = this. new _Param_13_VerifyProcess();
  {
    m_Param_13_VerifyProcess.setParent(this);
  }
  public class _Param_14_equal extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
            /*** lines: 117-117 */
            __fileData.writeBytes (4641, 48, out);
            out.print(request.encodeURL ("/nucleus/atg/commerce/catalog/custom/AncestorGeneratorService", true, true, false, true));
            request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
            /*** lines: 117-125 */
            __fileData.writeBytes (4750, 239, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.catalogsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 125-126 */
            __fileData.writeBytes (5067, 15, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.ancestorCategoriesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 126-127 */
            __fileData.writeBytes (5170, 15, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.parentCategoriesForCatalogPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 127-128 */
            __fileData.writeBytes (5281, 15, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.sitesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 128-135 */
            __fileData.writeBytes (5371, 126, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.catalogsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 135-136 */
            __fileData.writeBytes (5575, 15, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.ancestorCategoriesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 136-137 */
            __fileData.writeBytes (5678, 15, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.parentCategoriesForCatalogPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 137-138 */
            __fileData.writeBytes (5789, 15, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.sitesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 138-145 */
            __fileData.writeBytes (5879, 122, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.catalogsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 145-146 */
            __fileData.writeBytes (6079, 15, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.sitesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 146-151 */
            __fileData.writeBytes (6169, 49, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_14_equal m_Param_14_equal = this. new _Param_14_equal();
  {
    m_Param_14_equal.setParent(this);
  }
  public class _SubServlet_15 extends PageSubServlet {
    {
      this.setParameter("obj2", "true");
      this.setParameter("equal", m_Param_14_equal);
    }
  }

  _SubServlet_15 m_SubServlet_15 = this. new _SubServlet_15();
  {
    m_SubServlet_15.setParent(this);
  }
  public class _Param_16_equal extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
            /*** lines: 157-157 */
            __fileData.writeBytes (6433, 51, out);
            out.print(request.encodeURL ("/nucleus/atg/commerce/catalog/custom/CatalogVerificationService", true, true, false, true));
            request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
            /*** lines: 157-165 */
            __fileData.writeBytes (6547, 234, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.ancestorCategoriesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 165-166 */
            __fileData.writeBytes (6869, 15, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.allRootCategoriesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 166-167 */
            __fileData.writeBytes (6971, 15, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.directAncestorCatalogsAndSelfPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 167-168 */
            __fileData.writeBytes (7085, 15, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.indirectAncestorCatalogsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 168-175 */
            __fileData.writeBytes (7194, 134, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.relatedCategoriesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 175-176 */
            __fileData.writeBytes (7415, 16, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.categoryInfosPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 176-177 */
            __fileData.writeBytes (7514, 16, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.parentCategoryPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 177-184 */
            __fileData.writeBytes (7614, 133, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.relatedProductsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 184-185 */
            __fileData.writeBytes (7832, 16, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.catalogsRelatedProductsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 185-186 */
            __fileData.writeBytes (7941, 16, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.productInfosPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 186-187 */
            __fileData.writeBytes (8039, 16, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.parentCategoriesForCatalogPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 187-194 */
            __fileData.writeBytes (8151, 129, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.replacementProductsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 194-195 */
            __fileData.writeBytes (8369, 16, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.catalogsReplacementProductsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 195-196 */
            __fileData.writeBytes (8482, 16, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.skuInfosPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 196-200 */
            __fileData.writeBytes (8576, 44, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_16_equal m_Param_16_equal = this. new _Param_16_equal();
  {
    m_Param_16_equal.setParent(this);
  }
  public class _SubServlet_17 extends PageSubServlet {
    {
      this.setParameter("obj2", "true");
      this.setParameter("equal", m_Param_16_equal);
    }
  }

  _SubServlet_17 m_SubServlet_17 = this. new _SubServlet_17();
  {
    m_SubServlet_17.setParent(this);
  }
  public class _Param_18_equal extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 111-114 */
            __fileData.writeBytes (4424, 33, out);
            try {
              request.pushFrame();
              request.setParameter("obj1", DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/RunServiceFormHandler.isAGSConfiguredToRun", true, null, null));
              m_SubServlet_15.serviceByName(request, response, "/atg/dynamo/droplet/Compare", 114);
            }
            finally {
              request.popFrame();
            }
            /*** lines: 152-154 */
            __fileData.writeBytes (6241, 5, out);
            try {
              request.pushFrame();
              request.setParameter("obj1", DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/RunServiceFormHandler.isCVSConfiguredToRun", true, null, null));
              m_SubServlet_17.serviceByName(request, response, "/atg/dynamo/droplet/Compare", 154);
            }
            finally {
              request.popFrame();
            }
            /*** lines: 201-204 */
            __fileData.writeBytes (8643, 31, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_18_equal m_Param_18_equal = this. new _Param_18_equal();
  {
    m_Param_18_equal.setParent(this);
  }
  public class _Param_19_default extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
            /*** lines: 205-205 */
            __fileData.writeBytes (8712, 73, out);
            out.print(request.encodeURL ("/nucleus/atg/commerce/catalog/AncestorGeneratorService", true, true, false, true));
            request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
            /*** lines: 205-213 */
            __fileData.writeBytes (8839, 285, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/AncestorGeneratorService.ancestorCategoriesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 213-218 */
            __fileData.writeBytes (9228, 161, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/AncestorGeneratorService.ancestorCategoriesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 218-223 */
            __fileData.writeBytes (9493, 157, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/AncestorGeneratorService.ancestorCategoriesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 223-227 */
            __fileData.writeBytes (9754, 74, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_19_default m_Param_19_default = this. new _Param_19_default();
  {
    m_Param_19_default.setParent(this);
  }
  public class _SubServlet_20 extends PageSubServlet {
    {
      this.setParameter("obj2", "custom");
      this.setParameter("equal", m_Param_18_equal);
      this.setParameter("default", m_Param_19_default);
    }
  }

  _SubServlet_20 m_SubServlet_20 = this. new _SubServlet_20();
  {
    m_SubServlet_20.setParent(this);
  }
  public class _Param_21_BasicMaintProcess extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 106-108 */
            __fileData.writeBytes (4130, 102, out);
            try {
              request.pushFrame();
              request.setParameter("obj1", DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/RunServiceFormHandler.catalogType", true, null, null));
              m_SubServlet_20.serviceByName(request, response, "/atg/dynamo/droplet/Compare", 108);
            }
            finally {
              request.popFrame();
            }
            /*** lines: 228-230 */
            __fileData.writeBytes (9852, 5, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_21_BasicMaintProcess m_Param_21_BasicMaintProcess = this. new _Param_21_BasicMaintProcess();
  {
    m_Param_21_BasicMaintProcess.setParent(this);
  }
  public class _Param_22_UpdateCatalogProcess extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
            /*** lines: 231-231 */
            __fileData.writeBytes (9906, 18, out);
            out.print(request.encodeURL ("/nucleus/atg/commerce/catalog/custom/CatalogUpdateService", true, true, false, true));
            request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
            request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
            /*** lines: 231-231 */
            __fileData.writeBytes (9981, 85, out);
            out.print(request.encodeURL ("/nucleus/atg/commerce/catalog/custom/CatalogCompletionService/?propertyName=enabled", true, true, false, true));
            request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
            /*** lines: 231-244 */
            __fileData.writeBytes (10149, 514, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.sitesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 244-250 */
            __fileData.writeBytes (10738, 151, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.sitesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 250-251 */
            __fileData.writeBytes (10964, 25, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.ancestorCategoriesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 251-252 */
            __fileData.writeBytes (11077, 25, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.allRootCategoriesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 252-253 */
            __fileData.writeBytes (11189, 25, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.directAncestorCatalogsAndSelfPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 253-254 */
            __fileData.writeBytes (11313, 25, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.indirectAncestorCatalogsPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 254-260 */
            __fileData.writeBytes (11432, 157, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/CatalogTools.catalogProperties.sitesPropertyName", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 260-265 */
            __fileData.writeBytes (11664, 84, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_22_UpdateCatalogProcess m_Param_22_UpdateCatalogProcess = this. new _Param_22_UpdateCatalogProcess();
  {
    m_Param_22_UpdateCatalogProcess.setParent(this);
  }
  public class _SubServlet_23 extends PageSubServlet {
    {
      this.setParameter("VerifyProcess", m_Param_13_VerifyProcess);
      this.setParameter("BasicMaintProcess", m_Param_21_BasicMaintProcess);
      this.setParameter("UpdateCatalogProcess", m_Param_22_UpdateCatalogProcess);
    }
  }

  _SubServlet_23 m_SubServlet_23 = this. new _SubServlet_23();
  {
    m_SubServlet_23.setParent(this);
  }
  public class _Param_24_equal extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 275-275 */
            __fileData.writeBytes (12090, 24, out);
            {
              String _pn = "/atg/commerce/catalog/RunServiceFormHandler.repositoryPath";
              out.print(" value=\"");
              out.print(DropletDescriptor.getPropertyHtmlStringValue(request, response, _beanName0, true, null, null));
              out.print('"');
              out.print(" name=\"");
              out.print(_pn);
              out.print('"');
              /*** lines: 275-275 */
              __fileData.writeBytes (12157, 1, out);
              if (_form != null) _form.addTag(request, response, null, _pn, null, null, DropletConstants.PRIORITY_DEFAULT, null, null, null, false);
            }
            /*** lines: 275-277 */
            __fileData.writeBytes (12158, 10, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_24_equal m_Param_24_equal = this. new _Param_24_equal();
  {
    m_Param_24_equal.setParent(this);
  }
  public class _Param_25_default extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/maintenance/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 278-279 */
            __fileData.writeBytes (12206, 7, out);
            {
              Object _t = DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/RunServiceFormHandler.repositoryPath", true, PageCompileServlet.getEscapeHTMLTagConverter(), null);
              if (_t == null) {
              }
                out.print(ServletUtil.toString(_t));
            }
            /*** lines: 279-280 */
            __fileData.writeBytes (12276, 10, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_25_default m_Param_25_default = this. new _Param_25_default();
  {
    m_Param_25_default.setParent(this);
  }
  public class _SubServlet_26 extends PageSubServlet {
    {
      this.setParameter("obj2", "custom");
      this.setParameter("equal", m_Param_24_equal);
      this.setParameter("default", m_Param_25_default);
    }
  }

  _SubServlet_26 m_SubServlet_26 = this. new _SubServlet_26();
  {
    m_SubServlet_26.setParent(this);
  }
  
  public static final String[] INCLUDED_SOURCE_URIS = null;
  public static final long[] INCLUDED_SOURCE_MODIFIED_TIMES = null;
  public static String[] _jspDynGetSourceURIs() {
    return INCLUDED_SOURCE_URIS;
  }
  
  public final static String _JSP_ENCODING =   null  ;
  
  public static String _jspGetEncoding() {
    return _JSP_ENCODING;
  }
  

  //-------------------------------
  {
  
    DropletImports _imports = new DropletImports();
    this. setParameter("_imports", _imports);
    _imports.addImport("/atg/commerce/catalog/CatalogTools");
    _imports.addImport("/atg/commerce/catalog/RunServiceFormHandler");
  }
  
  //-------------- The _jspService method
  public void _jspService (DynamoHttpServletRequest request,
                       DynamoHttpServletResponse response)
      throws ServletException, IOException
  {
    ByteFileData __fileData = null;
    try {
      __fileData = (ByteFileData)       request.getAttribute(getFileCacheAttributeName())      ;
      JspFactory _jspFactory = DynamoJspFactory.getDynamoJspFactory();
      DynamoJspPageContext pageContext = (DynamoJspPageContext)_jspFactory.getPageContext(
        this, request, response, 
        null,true, JspWriter.DEFAULT_BUFFER, true);
        ServletConfig config = getServletConfig();
        ServletContext application = config.getServletContext();
        HttpSession session = pageContext.getSession();
        Object page = this;
      
      ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
      
      int _jspTempReturn;
      
      try {

        FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
        DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
        /*** lines: 1-1 */
        __fileData.writeBytes (0, 59, out);
        out.print(request.encodeURL ("/atg/dynamo/admin/admin.css", false, true, true, true));
        /*** lines: 1-6 */
        __fileData.writeBytes (86, 50, out);
        /*** lines: 7-7 */
        __fileData.writeBytes (190, 1, out);
        request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
        /*** lines: 8-8 */
        __fileData.writeBytes (254, 126, out);
        out.print(request.encodeURL ("/", true, true, false, true));
        request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
        request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
        /*** lines: 8-8 */
        __fileData.writeBytes (381, 21, out);
        out.print(request.encodeURL ("../index.jhtml", true, true, false, true));
        request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
        /*** lines: 8-12 */
        __fileData.writeBytes (416, 15, out);
        try {
          request.pushFrame();
          request.setParameter("value", request.getObjectParameter("process"));
          m_SubServlet_3.serviceByName(request, response, "/atg/dynamo/droplet/Switch", 12);
        }
        finally {
          request.popFrame();
        }
        /*** lines: 23-24 */
        __fileData.writeBytes (740, 1, out);
        try {
          request.pushFrame();
          request.setParameter("value", request.getObjectParameter("process"));
          m_SubServlet_7.serviceByName(request, response, "/atg/dynamo/droplet/Switch", 24);
        }
        finally {
          request.popFrame();
        }
        /*** lines: 35-40 */
        __fileData.writeBytes (1065, 45, out);
        try {
          request.pushFrame();
          request.setParameter("value", DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/RunServiceFormHandler.formError", true, null, null));
          m_SubServlet_12.serviceByName(request, response, "/atg/dynamo/droplet/Switch", 40);
        }
        finally {
          request.popFrame();
        }
        /*** lines: 58-62 */
        __fileData.writeBytes (1702, 31, out);
        try {
          request.pushFrame();
          request.setParameter("value", request.getObjectParameter("process"));
          m_SubServlet_23.serviceByName(request, response, "/atg/dynamo/droplet/Switch", 62);
        }
        finally {
          request.popFrame();
        }
        _form = ((DropletEventServlet)request.getAttribute(DropletConstants.DROPLET_EVENT_ATTRIBUTE)).addForm("/atg/commerce/admin/en/maintenance/startService.jhtml.8");
        if (_form != null) {
         String actionURI= ServletUtil.getRequestURI(request, ServletUtil.toString(request.getRequestURIWithQueryString()));
        _form.addActionURL(actionURI);
        }
        request.setParameter("_form", _form);
        request.addQueryParameter("_DARGS", "/atg/commerce/admin/en/maintenance/startService.jhtml.8");
        request.addQueryParameter("_dynSessConf", Long.toString(request.getSessionConfirmationNumber()));        /*** lines: 266-266 */
        __fileData.writeBytes (11770, 45, out);
        out.print(request.encodeURL (ServletUtil.toString(request.getRequestURIWithQueryString()), true, true, false, true, false));
        /*** lines: 266-272 */
        __fileData.writeBytes (11855, 52, out);
        try {
          request.pushFrame();
          request.setParameter("obj1", DropletDescriptor.getPropertyValue(request, response, "/atg/commerce/catalog/RunServiceFormHandler.catalogType", true, null, null));
          m_SubServlet_26.serviceByName(request, response, "/atg/dynamo/droplet/Compare", 272);
        }
        finally {
          request.popFrame();
        }
        /*** lines: 281-281 */
        __fileData.writeBytes (12310, 26, out);
        /*** lines: 281-281 */
        __fileData.writeBytes (12385, 25, out);
        {
          String _pn = "/atg/commerce/catalog/RunServiceFormHandler.runProcessSuccessURL";
          out.print(" name=\"");
          out.print(_pn);
          out.print('"');
          /*** lines: 281-281 */
          __fileData.writeBytes (12410, 1, out);
          if (_form != null) _form.addTag(request, response, null, _pn, "hidden", null, DropletConstants.PRIORITY_DEFAULT, null, null, null, false);
        }
        /*** lines: 281-281 */
        __fileData.writeBytes (12411, 25, out);
        /*** lines: 281-281 */
        __fileData.writeBytes (12483, 8, out);
        out.print(ServletUtil.toString(request.getRequestURIWithQueryString()));
        /*** lines: 281-281 */
        __fileData.writeBytes (12531, 1, out);
        {
          String _pn = "/atg/commerce/catalog/RunServiceFormHandler.runProcessErrorURL";
          out.print(" name=\"");
          out.print(_pn);
          out.print('"');
          /*** lines: 281-281 */
          __fileData.writeBytes (12532, 1, out);
          if (_form != null) _form.addTag(request, response, null, _pn, "hidden", null, DropletConstants.PRIORITY_DEFAULT, null, null, null, false);
        }
        /*** lines: 281-281 */
        __fileData.writeBytes (12533, 25, out);
        /*** lines: 281-281 */
        __fileData.writeBytes (12603, 8, out);
        {
          String _t = request.getParameter("process");
          if (_t != null)
            out.print(ServletUtil.escapeHtmlString(_t));
        }
        /*** lines: 281-281 */
        __fileData.writeBytes (12624, 1, out);
        {
          String _pn = "/atg/commerce/catalog/RunServiceFormHandler.requestedProcess";
          out.print(" name=\"");
          out.print(_pn);
          out.print('"');
          /*** lines: 281-281 */
          __fileData.writeBytes (12625, 1, out);
          if (_form != null) _form.addTag(request, response, null, _pn, "hidden", null, DropletConstants.PRIORITY_DEFAULT, null, null, null, false);
        }
        /*** lines: 281-281 */
        __fileData.writeBytes (12626, 29, out);
        /*** lines: 281-281 */
        __fileData.writeBytes (12694, 22, out);
        {
          String _pn = "/atg/commerce/catalog/RunServiceFormHandler.runProcess";
          out.print(" name=\"");
          out.print(_pn);
          out.print('"');
          /*** lines: 281-281 */
          __fileData.writeBytes (12716, 1, out);
          if (_form != null) _form.addTag(request, response, null, _pn, "submit", null, DropletConstants.SUBMIT_PRIORITY_DEFAULT, null, null, null, false);
        }
        /*** lines: 281-281 */
        __fileData.writeBytes (12717, 36, out);
        out.print(request.encodeURL ("../../images/bluedot.gif", false, true, true, true));
        /*** lines: 281-295 */
        __fileData.writeBytes (12777, 43, out);
/* @version $Id: //product/DCS/version/11.1/release/DCS/admin/atg/commerce/admin/en/maintenance/startService.jhtml#1 $$Change: 875535 $*/        /*** lines: 295-295 */
        __fileData.writeBytes (12970, 1, out);
      } catch (Exception e) {
        if (!(e instanceof EndOfPageException)) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
      finally {
        pageContext.cleanupPoppedBodyContent();
        out.close();
        _jspFactory.releasePageContext(pageContext);
      }
    }
    finally {
      if (__fileData != null) __fileData.close();
    }
  }
}
