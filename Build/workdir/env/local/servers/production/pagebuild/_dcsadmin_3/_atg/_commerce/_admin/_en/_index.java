package _dcsadmin_3._atg._commerce._admin._en;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import atg.nucleus.*;
import atg.naming.*;
import atg.service.filecache.*;
import atg.servlet.*;
import atg.droplet.*;
import atg.servlet.pagecompile.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import atg.servlet.jsp.*;

public class _index
extends atg.servlet.jsp.DynamoJspPageServlet implements atg.servlet.pagecompile.AttrCompiledServlet {
  public String getFileCacheAttributeName() {
    return "__002fatg_002fcommerce_002fadmin_002fen_002findex_xjhtml";
  }
  
  public static final long SOURCE_MODIFIED_TIME = 1401377606000L;
  public class _Param_0_Fulfillment extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
            /*** lines: 33-33 */
            __fileData.writeBytes (1043, 43, out);
            out.print(request.encodeURL ("fulfillment/index.jhtml", true, true, false, true));
            request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
            /*** lines: 33-41 */
            __fileData.writeBytes (1109, 276, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_0_Fulfillment m_Param_0_Fulfillment = this. new _Param_0_Fulfillment();
  {
    m_Param_0_Fulfillment.setParent(this);
  }
  public class _Param_1_AbandonedOrderServices extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
            /*** lines: 42-42 */
            __fileData.writeBytes (1440, 43, out);
            out.print(request.encodeURL ("abandonment/index.jhtml", true, true, false, true));
            request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
            /*** lines: 42-50 */
            __fileData.writeBytes (1506, 189, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_1_AbandonedOrderServices m_Param_1_AbandonedOrderServices = this. new _Param_1_AbandonedOrderServices();
  {
    m_Param_1_AbandonedOrderServices.setParent(this);
  }
  public class _SubServlet_2 extends PageSubServlet {
    {
      this.setParameter("Fulfillment", m_Param_0_Fulfillment);
      this.setParameter("AbandonedOrderServices", m_Param_1_AbandonedOrderServices);
    }
  }

  _SubServlet_2 m_SubServlet_2 = this. new _SubServlet_2();
  {
    m_SubServlet_2.setParent(this);
  }
  public class _Param_3_output extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 29-31 */
            __fileData.writeBytes (810, 95, out);
            try {
              request.pushFrame();
              request.setParameter("value", request.getObjectParameter("module.moduleName"));
              m_SubServlet_2.serviceByName(request, response, "/atg/dynamo/droplet/Switch", 31);
            }
            finally {
              request.popFrame();
            }
            /*** lines: 51-52 */
            __fileData.writeBytes (1721, 5, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_3_output m_Param_3_output = this. new _Param_3_output();
  {
    m_Param_3_output.setParent(this);
  }
  public class _SubServlet_4 extends PageSubServlet {
    {
      this.setParameter("elementName", "module");
      this.setParameter("output", m_Param_3_output);
    }
  }

  _SubServlet_4 m_SubServlet_4 = this. new _SubServlet_4();
  {
    m_SubServlet_4.setParent(this);
  }
  public class _Param_5_CustomCatalogMigration extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
            /*** lines: 62-62 */
            __fileData.writeBytes (2182, 44, out);
            out.print(request.encodeURL ("catalog/StandardMigration.jhtml", true, true, false, true));
            request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
            /*** lines: 62-70 */
            __fileData.writeBytes (2257, 210, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_5_CustomCatalogMigration m_Param_5_CustomCatalogMigration = this. new _Param_5_CustomCatalogMigration();
  {
    m_Param_5_CustomCatalogMigration.setParent(this);
  }
  public class _SubServlet_6 extends PageSubServlet {
    {
      this.setParameter("CustomCatalogMigration", m_Param_5_CustomCatalogMigration);
    }
  }

  _SubServlet_6 m_SubServlet_6 = this. new _SubServlet_6();
  {
    m_SubServlet_6.setParent(this);
  }
  public class _Param_7_output extends PageSubServlet {
    
    //-------------- The service method
    public void service (DynamoHttpServletRequest request,
                         DynamoHttpServletResponse response)
        throws ServletException, IOException
    {
      ByteFileData __fileData = null;
      {
        __fileData = (ByteFileData)         request.getAttribute(getFileCacheAttributeName())        ;
        DynamoJspPageContext pageContext = (DynamoJspPageContext)request.getAttribute(DynamoJspPageContext.REQUEST_PAGE_CONTEXT_ATTR);
        
        ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
        
        int _jspTempReturn;
        
        try {
          String _saveBaseDir = null;
          try {
            if ((_saveBaseDir = request.getBaseDirectory()) != null)
              request.setBaseDirectory("/atg/commerce/admin/en/");
            FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
            DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
            /*** lines: 58-60 */
            __fileData.writeBytes (1942, 91, out);
            try {
              request.pushFrame();
              request.setParameter("value", request.getObjectParameter("module.moduleName"));
              m_SubServlet_6.serviceByName(request, response, "/atg/dynamo/droplet/Switch", 60);
            }
            finally {
              request.popFrame();
            }
            /*** lines: 71-72 */
            __fileData.writeBytes (2493, 5, out);
          }
          finally {
            if (_saveBaseDir != null) request.setBaseDirectory(_saveBaseDir);
          }
        } catch (Exception e) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
    }
  }
  _Param_7_output m_Param_7_output = this. new _Param_7_output();
  {
    m_Param_7_output.setParent(this);
  }
  public class _SubServlet_8 extends PageSubServlet {
    {
      this.setParameter("elementName", "module");
      this.setParameter("output", m_Param_7_output);
    }
  }

  _SubServlet_8 m_SubServlet_8 = this. new _SubServlet_8();
  {
    m_SubServlet_8.setParent(this);
  }
  
  public static final String[] INCLUDED_SOURCE_URIS = null;
  public static final long[] INCLUDED_SOURCE_MODIFIED_TIMES = null;
  public static String[] _jspDynGetSourceURIs() {
    return INCLUDED_SOURCE_URIS;
  }
  
  public final static String _JSP_ENCODING =   null  ;
  
  public static String _jspGetEncoding() {
    return _JSP_ENCODING;
  }
  

  //-------------------------------
  {
  
    DropletImports _imports = new DropletImports();
    this. setParameter("_imports", _imports);
  }
  
  //-------------- The _jspService method
  public void _jspService (DynamoHttpServletRequest request,
                       DynamoHttpServletResponse response)
      throws ServletException, IOException
  {
    ByteFileData __fileData = null;
    try {
      __fileData = (ByteFileData)       request.getAttribute(getFileCacheAttributeName())      ;
      JspFactory _jspFactory = DynamoJspFactory.getDynamoJspFactory();
      DynamoJspPageContext pageContext = (DynamoJspPageContext)_jspFactory.getPageContext(
        this, request, response, 
        null,true, JspWriter.DEFAULT_BUFFER, true);
        ServletConfig config = getServletConfig();
        ServletContext application = config.getServletContext();
        HttpSession session = pageContext.getSession();
        Object page = this;
      
      ByteBufferedBodyContent out = (ByteBufferedBodyContent)pageContext.getOut();
      
      int _jspTempReturn;
      
      try {

        FormTag _form = (FormTag) request.getObjectParameter(ServletUtil.FORM_NAME);
        DropletImports _imports = (DropletImports) request.getObjectParameter(ServletUtil.IMPORTS_NAME);
        /*** lines: 1-1 */
        __fileData.writeBytes (0, 59, out);
        out.print(request.encodeURL ("/atg/dynamo/admin/admin.css", false, true, true, true));
        request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
        /*** lines: 1-1 */
        __fileData.writeBytes (86, 181, out);
        out.print(request.encodeURL ("/", true, true, false, true));
        request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
        request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
        /*** lines: 1-1 */
        __fileData.writeBytes (268, 92, out);
        out.print(request.encodeURL ("inventory/index.jhtml", true, true, false, true));
        request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
        /*** lines: 1-25 */
        __fileData.writeBytes (381, 169, out);
/* fulfillment stuff depends on Fillfillment module */        /*** lines: 25-26 */
        __fileData.writeBytes (617, 3, out);
        try {
          request.pushFrame();
          request.setParameter("array", DropletDescriptor.getPropertyValue(request, response, "/atg/modules/ModuleManager.modules", true, null, null));
          m_SubServlet_4.serviceByName(request, response, "/atg/dynamo/droplet/ForEach", 26);
        }
        finally {
          request.popFrame();
        }
        /*** lines: 53-55 */
        __fileData.writeBytes (1748, 4, out);
        try {
          request.pushFrame();
          request.setParameter("array", DropletDescriptor.getPropertyValue(request, response, "/atg/modules/ModuleManager.modules", true, null, null));
          m_SubServlet_8.serviceByName(request, response, "/atg/dynamo/droplet/ForEach", 55);
        }
        finally {
          request.popFrame();
        }
        request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
        /*** lines: 73-73 */
        __fileData.writeBytes (2520, 146, out);
        out.print(request.encodeURL ("catalog/CopyCatalog.jhtml", true, true, false, true));
        request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
        request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
        /*** lines: 73-73 */
        __fileData.writeBytes (2691, 179, out);
        out.print(request.encodeURL ("catalog/PrepareToSwitchProductCatalog.jhtml", true, true, false, true));
        request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
        request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
        request.addQueryParameter("process", "UpdateCatalogProcess");
        /*** lines: 73-73 */
        __fileData.writeBytes (2913, 392, out);
        out.print(request.encodeURL ("maintenance/startService.jhtml", true, true, false, true));
        /*** lines: 73-102 */
        __fileData.writeBytes (3335, 11, out);
        request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
        request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
        /*** lines: 103-103 */
        __fileData.writeBytes (3397, 228, out);
        out.print(request.encodeURL ("/nucleus/atg/commerce/catalog/custom/CatalogCompletionService/?propertyName=enabled", true, true, false, true));
        request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
        request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
        request.addQueryParameter("process", "VerifyProcess");
        /*** lines: 103-103 */
        __fileData.writeBytes (3708, 429, out);
        out.print(request.encodeURL ("maintenance/startService.jhtml", true, true, false, true));
        /*** lines: 103-114 */
        __fileData.writeBytes (4167, 11, out);
        request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
        request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
        request.addQueryParameter("process", "BasicMaintProcess");
        /*** lines: 115-115 */
        __fileData.writeBytes (4222, 193, out);
        out.print(request.encodeURL ("maintenance/startService.jhtml", true, true, false, true));
        /*** lines: 115-122 */
        __fileData.writeBytes (4445, 8, out);
        request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
        request.setParameterDelimiter(ServletUtil.getParamDelimiter(request));
        /*** lines: 123-123 */
        __fileData.writeBytes (4501, 182, out);
        out.print(request.encodeURL ("maintenance/viewStatus.jhtml", true, true, false, true));
        request.setParameterDelimiter(DynamoHttpServletRequest.DEFAULT_PARAMETER_DELIMITER);
        /*** lines: 123-123 */
        __fileData.writeBytes (4711, 169, out);
        out.print(request.encodeURL ("../images/bluedot.gif", false, true, true, true));
        /*** lines: 123-140 */
        __fileData.writeBytes (4901, 43, out);
/* @version $Id: //product/DCS/version/11.1/release/DCS/admin/atg/commerce/admin/en/index.jhtml#1 $$Change: 875535 $*/        /*** lines: 140-141 */
        __fileData.writeBytes (5075, 2, out);
      } catch (Exception e) {
        if (!(e instanceof EndOfPageException)) {
          // out.clear();
          out.clearBuffer();
          pageContext.handlePageException(e);
        }
      }
      finally {
        pageContext.cleanupPoppedBodyContent();
        out.close();
        _jspFactory.releasePageContext(pageContext);
      }
    }
    finally {
      if (__fileData != null) __fileData.close();
    }
  }
}
