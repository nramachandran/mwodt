
#!/bin/sh
#  -- Update USER_MEM_ARGS - added by CIM

/home/oracle/Oracle/Middleware/Oracle_Home/user_projects/domains/tru_domain/bin/setDomainEnv.sh

if [ "${JAVA_USE_64BIT}" ="true" ] ; then 
  USER_MEM_ARGS="-Xms1152m -Xmx2048m -XX:NewSize=128m -XX:MaxNewSize=256m -XX:PermSize=128m -XX:MaxPermSize=256m"; export USER_MEM_ARGS
else
  USER_MEM_ARGS="-Xms1152m -Xmx1152m -XX:NewSize=128m -XX:MaxNewSize=256m -XX:PermSize=128m -XX:MaxPermSize=256m"; export USER_MEM_ARGS
fi

#  -- End USER_MEM_ARGS block - added by CIM
"/home/oracle/Oracle/Middleware/Oracle_Home/user_projects/domains/tru_domain//bin/startManagedWebLogic.sh" production t3://localhost:7001/ $*
