/**
 * @function
 * @description Wrapper function for MWODT -- MWODT.js
 * @param window
 * @param document
 * @param $
 * @ignore
 */

function formsubmit() {
			var searchForm = document.getElementById('searchForm');
			searchForm.submit();
		}

(function (window, document, $, undef) {

    "use strict";

    /**
     * @namespace
     */
    var MWODT    = window.MWODT || {};

    // ================================== //
    // ==     Javascript polyfills     == //
    // ================================== //
    if(!Array.prototype.indexOf){
        Array.prototype.indexOf = function(obj, start) {
            for (var i = (start || 0), j = this.length; i < j; i++) {
                if (this[i] === obj) { return i; }
            }
            return -1;
        };
    }
    // == End Polyfills == //

    // Polyfills
    if(typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function() {
            return this.replace(/^\s+|\s+$/g, '');
        };
    }


    /*
 _______  __   __  _______  _______
|       ||  |_|  ||       ||       |
|    ___||       ||    ___||       |
|   |___ |       ||   |___ |       |
|    ___| |     | |    ___||      _|
|   |___ |   _   ||   |___ |     |_
|_______||__| |__||_______||_______|

*/



    /**
     * @namespace
     * @description Comprehensive DOM-ready execution
     */
    MWODT.exec = {



        /*
     _______  _______  __   __  __   __  _______  __    _
    |       ||       ||  |_|  ||  |_|  ||       ||  |  | |
    |       ||   _   ||       ||       ||   _   ||   |_| |
    |       ||  | |  ||       ||       ||  | |  ||       |
    |      _||  |_|  ||       ||       ||  |_|  ||  _    |
    |     |_ |       || ||_|| || ||_|| ||       || | |   |
    |_______||_______||_|   |_||_|   |_||_______||_|  |__|

    */



        /**
         * @namespace CONTROLLER: Fires on every page
         */
        common : {

            /** @function */
            init : function () {
            	 $('.slider-image-block-container').show();
            	 $('.row-strollers').show();
            },
		},
        /**
         * @namespace CONTROLLER: stories
         */
        productDetail : {
            /** @function */
            addToCart : function () {
            	$("#btnAddToCart").on("click", function(e){
            		e.preventDefault();
           			$("#txtQuantity").val($(".sticky-section").val());
            		$("#frmAddToCart").get(0).submit();
            	});	
            	if(typeof(isAddtoCart) !== "undefined" && isAddtoCart == 'true' 
            		&& typeof(addedProductId) !== "undefined" && addedProductId != '') {
            		$("#invokerSCModal").trigger("click");
            	}
            }
        }
    }; // MWODT.exec


    /*
 ___   __    _  ___   _______
|   | |  |  | ||   | |       |
|   | |   |_| ||   | |_     _|
|   | |       ||   |   |   |
|   | |  _    ||   |   |   |
|   | | | |   ||   |   |   |
|___| |_|  |__||___|   |___|

*/



    /**
     * @namespace
     * @description Fires the proper modules based on data-controller and data-action attributes.
     */
    MWODT.init = (function () {

        /** @function */
        function fire(controller, act, args) {
            var module = MWODT.exec,
            action = (act === undef) ? ['init'] : act,
            i = 0;

            if (controller !== '' && module[controller]) {
                i = action.length;
                while (i--) {
                    if (typeof module[controller][action[i]] === 'function') {
                        //log('MWODT.init.fire is calling: MWODT.exec.' + controller + '.' + action[i]);
                        module[controller][action[i]](args);
                    }
                }
            }
        }

        /** @function */
        function bootstrap() {
            var exec = $('[data-controller]'),
            i = exec.length,
            magazine = {},
            controller,
            controllers,
            cLen;

            fire('common');

            // execute each module definition as it occurs, whether it is defined on the body or elsewhere
            while (i--) {
                controller = exec[i].getAttribute('data-controller');
                magazine[controller] = magazine[controller] || {};
                magazine[controller][exec[i].getAttribute('data-action')] = 1;
            }
            for (controllers = Object.keys(magazine), i = 0, cLen = controllers.length; i < cLen; i++) {
                fire(controllers[i]);
                fire(controllers[i], Object.keys(magazine[controllers[i]]));
            }

            $(document).trigger('cleanup');
			return true;
        }

        $(document).ready(bootstrap);

        return true;
    })();
    return (window.MWODT = MWODT);

})(this, this.document, this.jQuery);