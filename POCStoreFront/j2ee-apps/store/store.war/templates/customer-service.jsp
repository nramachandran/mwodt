<div class="container-fluid customer-service-template defaut-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				{{global-nav}}
				{{explore-modal}}
			</div>
        </div>
    </div>
     <div class="template-content">
     	{{customer-service-breadcrumb}}
     	<div class="row">
     		<div class="col-md-9">
	    		{{customer-service-header}}
	    		<hr>
	    		{{customer-service-tabs}}
	    		{{customer-service-general-help}}
	    		{{customer-service-accordian}}
     		</div>
     		<div class="col-md-3">
     			{{customer-service-help}}
     		</div>
     	</div>
    </div>
    {{footer-grid}}
    {{quick-help-modal}}
</div>