<div class="container-fluid egift-card-landing-form-template default-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				{{global-nav}}
				{{explore-modal}}
			</div>
        </div>
    </div>
    <div class="template-content">
        {{egift-card-breadcrumb}}
        {{gift-card-landing-description '{"data": "egiftcardheader"}'}}
        <hr>
        {{egift-card-form-step}}
        <hr>
        {{egift-card-form}}
        <br>
        <br>
        <br>
        {{egift-card-form}}
        {{egift-card-form-footer}}
        {{terms-conditions}}
	</div>

    {{footer-grid}}
    {{my-account-delete-cancel-modal '{"data": "remove-confirmation"}'}}
</div>