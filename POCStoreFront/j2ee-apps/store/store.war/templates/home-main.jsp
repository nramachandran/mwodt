<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container-fluid home-template min-width">
    <div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				<c:import url="/structures/global-nav.jsp"></c:import>
				<c:import url="/structures/explore-modal.jsp"></c:import>
            </div>
        </div>
    </div>
    <div class="row row-no-padding carousel-margin">
		<div class="col-md-12 col-no-padding">
			<c:import url="/components/carousel.jspf"></c:import>
		</div>
    </div>
     <div class="default-margin"> 
    <div class="max-width">

    <div class="row">
        <div class="col-md-12">
			<c:import url="/components/ad-zone-970-90.jspf"></c:import>
		</div>
    </div>

    <div class="row-center">
		<div class="col-md-4 block-3-column">
			<c:import url="/components/content-block-3.jspf"></c:import>
		</div>
		<div class="col-md-4 block-3-column text-center">
			<c:import url="/components/content-block-3.jspf"></c:import>
		</div>
		<div class="col-md-4 block-3-column text-right">
			<c:import url="/components/content-block-3.jspf"></c:import>
		</div>
    </div>
    <div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
			<c:import url="/components/MyStoreHeader.jspf"></c:import>
		</div>
    </div>
    <div class="row block-2-row">
        <div class="col-md-3 block-2-column">
			<c:import url="/components/content-block-2.jspf"></c:import>
		</div>
        <div class="col-md-3 block-2-column text-center">
			<c:import url="/components/content-block-2.jspf"></c:import>
		</div>
        <div class="col-md-3 block-2-column text-center">
			<c:import url="/components/content-block-2.jspf"></c:import>
		</div>
        <div class="col-md-3 block-2-column text-right">
			<c:import url="/components/content-block-2.jspf"></c:import>
		</div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr class="padded-hr">
				<c:import url="/components/ad-zone-970-90.jspf"></c:import>
            <hr>
        </div>
    </div>
	<c:import url="/components/email-sign-up-overlay.jspf"></c:import>
</div>
</div>
	<c:import url="/structures/footer-grid.jsp"></c:import>
</div> 
</dsp:page>