<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>

<dsp:include page="/global/getSiteContextPath.jsp" />
 
<div class="offcanvas gift-finder-menu gift-finder-template" id="gift-finder-menu-container">
	<c:import url="/structures/gift-finder-menu.jsp"></c:import>
</div>
<div class="fade-to-black" id="product-disabled-modal"></div>

  <dsp:droplet name="ProductLookup">
    <dsp:param name="id" param="productId"/>
    <dsp:param name="elementName" value="product" />
    <dsp:oparam name="output">
    	<dsp:getvalueof var="prodItem" param="product"/>
    </dsp:oparam>
  </dsp:droplet>
  
  <dsp:getvalueof var="skuItem" value="${prodItem.childSKUs[0]}" />
  <dsp:getvalueof var="skuLimit" value="${skuItem.customerPurchaseLimit }" />

<div class="container-fluid product-details-template">
	<c:import url="/structures/global-nav.jsp"></c:import>
	<c:import url="/structures/explore-modal.jsp"></c:import>
	<dsp:include page="/structures/quickview-modal.jsp">
		<dsp:param name="skuLimit" value="${skuLimit}" />
	</dsp:include>
	<c:import url="/components/ad-zone-777-34.jspf"></c:import>
    <div class="print-header">
        <img id="logo-swap-lg" src="/assets/images/global-nav/toysrus-logo-large.png" alt="mwodt logo">
    </div>
    <div class="pdp-header-block">
        <div class="header">
            <div class="fixed-product-title">
                <div class="mwodt-new-product-title"></div>
                <div class="product-title-row row">
					<dsp:include page="/components/product-title.jspf">
						<dsp:param name="displayName" value="${prodItem.displayName}"/>
					</dsp:include>
                </div>
                <div class="row product-review-row">
                    <div class="inline">
						<c:import url="/components/star-rating-img.jspf"></c:import>
                    </div>
						<c:import url="/components/product-reviews-menu.jspf"></c:import>
                </div>
            </div>
            <div class="row product-breadcrumb-row">
                <div class="col-md-8 product-breadcrumb-col">
					<c:import url="/components/product-breadcrumb.jspf"></c:import>
                </div>
                <div class="col-md-4">
					<c:import url="/components/back-to-wishlist.jspf"></c:import>
                </div>
            </div>
        </div>
        <div class="toys-template">
            <div class="sticky-price-template">
				<dsp:include page="/structures/sticky-price.jsp">
					<dsp:param name="product" value="${prodItem}"/>
					<dsp:param name="sku" value="${sku}"/>
					<dsp:param name="skuLimit" value="${skuLimit}" />
				</dsp:include>
            </div>
            <div class="inline">
				<c:import url="/components/gift-finder-tab.jspf"></c:import>
            </div>
            <div class="product-hero-area">
				<c:import url="/structures/product-hero-selector.jsp"></c:import>
            </div>
        </div>
    </div>
    <div id="scroll-detection"></div>
    <div class="pdp-product-details-block">
		<dsp:include page="/structures/product-details.jsp">
			<dsp:param name="product" value="${prodItem}"/>
			<dsp:param name="sku" value="${skuItem}"/>
		</dsp:include>
    </div>
    <div class="from-the-manufacturer">
		<c:import url="/structures/pdp-from-the-manufacturer.jsp"></c:import>
    </div>
    <div class="product-review-section">
        <div class="reviews">
			<c:import url="/structures/product-reviews.jsp"></c:import>
        </div>
        <hr />
    </div>
    <div class="buy-together-and-save">
        <div class="large-blue">
            related products
        </div>
        <div class="row row-no-padding">
            <div class="col-md-3 col-no-padding product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
            <div class="col-md-3 col-no-padding product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
            <div class="col-md-3 col-no-padding product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
            <div class="col-md-3 col-no-padding product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
        </div>
    </div>
	<c:import url="/components/ad-zone-728-90.jspf"></c:import>
    <div class="customers-also-purchased">
        <div>
            <div class="large-blue">
                recommended for you
            </div>
            <div class="row row-no-padding">
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="no-select" />
					</c:import>
				</div>
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="no-select" />
					</c:import>
				</div>
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="no-select" />
					</c:import>
				</div>
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="no-select" />
					</c:import>
				</div>
            </div>
        </div>
    </div>
	<c:import url="/components/fao-factoid.jspf"></c:import>
	<c:import url="/structures/footer-grid.jsp"></c:import>
	<c:import url="/structures/find-in-store-modal.jsp"></c:import>
	<c:import url="/structures/shopping-cart-overlay.jsp"></c:import>
</div>
</dsp:page>