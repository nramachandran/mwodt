<div class="container-fluid my-account-sign-in-template default-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				<c:import url="/structures/global-nav.jsp"></c:import>
				<c:import url="/structures/explore-modal.jsp"></c:import>
			</div>
        </div>
    </div>
    <div class="template-content">
        <c:import url="/structures/my-account-sign-in-top-container.jsp"></c:import>
    	<c:import url="/structures/recommended-for-you.jsp"></c:import>
	</div>
	<c:import url="/structures/my-account-product-carousel.jsp"></c:import>
    <c:import url="/structures/footer-grid.jsp"></c:import>
    <c:import url="/components/quick-help-modal.jspf"></c:import>
    <c:import url="/structures/sign-in-modal.jsp"></c:import>
</div>