<div class="container-fluid gift-finder-results-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
				{{global-nav}}
				{{explore-modal}}
        </div>
    </div>
    {{ad-zone-777-34}}
    {{gift-finder-results-header}}
     <div class="row default-margin">
            <div class="col-md-12 col-no-padding">
    {{ad-zone-970-90}}
</div>
</div>

    <div>
	    <div class="row default-margin">
	        <div class="col-md-12 col-no-padding">
				{{gift-finder-results-breadcrumb}}
			</div>
		</div>
	</div>
    <div class="row default-margin">
        <div class="col-mid-12 col-no-padding">
            {{gift-finder-results-select-zone}}
        </div>
    </div>
    <div id="narrow-by-scroll" class="product-content">
        <div class=" fixed-narrow-menu">
            <div class="row default-margin">
                <div class="col-md-6 col-no-padding col-narrow-by">
                    {{narrow-by-button}}{{showing-results}}
                </div>
                <div class="col-md-6 text-right col-narrow-by">	
                    {{sort-by}} 
                </div>
            </div>
            <div class="row-center">
                <div class="col-md-12 col-no-padding default-margin">
                    {{narrow-by-filter-list}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12"><hr class="thick-hr"/></div>
            </div>

            <div class="offcanvas compare-menu sub-cat-container">
                {{sub-category-filter-window}}
            </div>
        </div>
         <div class="sticky-menu-placeholder">
        </div>
        <div id="filtered-products">
            <div class="fade-to-black" id="product-disabled-modal"></div>
            <div class="row-footer-margin">
            <div class="row row-no-padding">
			
               
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "new-flag-mwodt"}}</div>
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "top-seller-flag-mwodt"}}</div>
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "exclusive-flag-mwodt"}}</div>
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block}}</div>
              
            </div>

            <div class="row row-no-padding">
                
                <div class="col-md-3 col-no-padding product-block-padding">
                    {{product-block "no-select"}}
                </div>
                <div class="col-md-3 col-no-padding product-block-padding">
                    {{product-block "no-select"}}
                </div>
                <div class="col-md-3 col-no-padding product-block-padding">
                    {{product-block "no-select"}}
                </div>
                <div class="col-md-3 col-no-padding product-block-padding">
                    {{product-block "no-select"}}
                </div>
             
            </div>
            <div class="row row-no-padding">
                {{#iterate 4}}
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "no-select"}}</div>
                {{/iterate}}
            </div>
		
            <div class="load-more-products">
                {{#iterate 8}}
                <div class="row row-no-padding">
                    {{#iterate 4}}
                    <div class="col-md-3 col-no-padding product-block-padding">{{product-block "no-select"}}</div>
                    {{/iterate}}
                </div>
                {{/iterate}}
            </div>
            {{family-show-more}}
              </div>
        </div>
         <div class="row default-margin">
            <div class="col-md-12 col-no-padding">
        {{ad-zone-970-90}}
        {{gift-finder-results-footer}}
    </div>
</div>
    
{{footer-grid}}
</div>