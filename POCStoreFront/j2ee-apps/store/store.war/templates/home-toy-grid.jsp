





<div class="container-fluid home-template min-width">
    <div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
                {{global-nav}}
                {{explore-modal}}
            </div>
        </div>
    </div>
    <div class="row row-no-padding carousel-margin">
    <div class="col-md-12 col-no-padding">{{carousel}}</div>
    </div>
     <div class="default-margin"> 
    <div class="max-width">

    <div class="row">
        <div class="col-md-12">{{ad-zone-970-90}}</div>
    </div>

    {{toy-grid-3}}
    <div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">{{MyStoreHeader}}</div>
    </div>
    <div class="row block-2-row">
        <div class="col-md-3 block-2-column">{{content-block-2}}</div>
        <div class="col-md-3 block-2-column text-center">{{content-block-2}}</div>
        <div class="col-md-3 block-2-column text-center">{{content-block-2}}</div>
        <div class="col-md-3 block-2-column text-right">{{content-block-2}}</div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr class="padded-hr">
            {{ad-zone-970-90}}
            <hr>
        </div>
    </div>
    {{email-sign-up-overlay}}
</div>
</div>

{{footer-grid}}

</div> 






