<div class="container-fluid layaway-template default-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				{{global-nav}}
				{{explore-modal}}
			</div>
        </div>
    </div>
    <div class="template-content">
        {{layaway-breadcrumb}}
    	<div class="row">
    		<div class="col-md-9">
                {{layaway-header}}
                <hr/>
                {{layaway-tabs-order-detail}}
                <div class="tab-content">
                    {{layaway-how-does}}
                    {{layaway-faq}}
                    <div id="orderDetail" class="tab-pane">
                        {{layaway-guest-details}}
                        {{layaway-order-product}}
                    </div>
                </div>
    		</div>
    		<div class="col-md-3">
    			{{customer-service-help}}
    		</div>
    	</div>
    </div>
    {{footer-grid}}
</div>