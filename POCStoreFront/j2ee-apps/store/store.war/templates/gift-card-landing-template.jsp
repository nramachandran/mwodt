<div class="container-fluid gift-card-landing-template default-template">
    <div class="fixed-nav">
		{{global-nav}}
		{{explore-modal}}
	</div>
    <div class="template-content">
    	{{egift-card-breadcrumb}}
        {{gift-card-landing-description}}
    	<hr>
    	{{gift-card-landing-options}}
    	<hr>
    	{{gift-card-landing-cards-zone}}
    	{{gift-card-landing-corporate}}
           {{terms-conditions 'gift-card-landing-corporate'}}
    </div>
    {{footer-grid}}
    {{gift-card-balance-overlay}}

</div>