<div class="container-fluid help-customer-service-template default-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				{{global-nav}}
				{{explore-modal}}
			</div>
        </div>
    </div>
    <div class="top-ad-zone">
        {{ad-zone-777-34}}
    </div>
    <div class="template-content">
    	{{help-breadcrumb}}
    	{{help-customer-service-header}}
    	<hr />
        <div class="row">
            <div class="col-md-3 col-no-padding">
                {{help-categories}}
            </div>
            <div class="col-md-9">
                {{help-customer-service-content}}
            </div>
        </div>
    </div>
    {{ad-zone-970-90}}
    {{footer-grid}}
</div>