﻿<div class="container-fluid checkout-pickup-template">
	{{checkout-nav}}
	<div class="checkout-pickup-container">
		<div class="row">
			<div class="col-md-9">
				{{store-pickup-header}}
                {{store-pickup-info-header}}
				{{store-pickup-form}}
				{{store-pickup-items-accordion}}
                <div class="spacer"></div>
				{{store-pickup-info-header}}
				{{store-pickup-info}}
				{{store-pickup-items-accordion}}
                <div class="spacer"></div>
			</div>
			<div class="col-md-3 checkout-order-summary">
				{{checkout-order-summary}}
			</div>
		</div>
	</div>
	{{shopping-cart-footer}}
</div>