﻿<div class="container-fluid checkout-template checkout-multiple-shipping">
	{{checkout-shipping-modal}}
	{{checkout-nav}}
	<div class="checkout-multiple-shipping-content">
		<div class="row checkout-sticky-top">
			<div class="col-md-8">
                {{checkout-shipping-default-header}}
                <a href="#" class="single-address-link">ship to a single address</a>
	            {{h-divider}}
                {{#iterate 2}}
					{{checkout-confirmation-block}}
					<hr />
				{{/iterate}}
				{{checkout-confirmation-block}}
				{{h-divider}}
			</div>
			<div class="col-md-3 pull-right">
				{{checkout-order-summary}}
			</div>
		</div>
		{{checkout-shipping-gifting}}
	</div>
	{{ad-zone-728-90}}
	{{shopping-cart-footer}}
</div>