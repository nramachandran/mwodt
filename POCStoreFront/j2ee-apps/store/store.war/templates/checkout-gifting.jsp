<div class="container-fluid checkout-gifting-template default-template">
	{{checkout-nav}}
	<div class="default-margin">
		<div class="row">
			<div class="col-md-9">
				{{checkout-gifting-header}}
				{{h-divider}}
				{{checkout-gifting-message-zone}}
				<br>
				{{checkout-gifting-registry-product}}
				{{h-divider}}
				{{checkout-gifting-registry-product}}
				{{h-divider}}
				{{checkout-gifting-add-gift-options}}
				{{h-divider}}
				{{checkout-gifting-items-not-giftwrap}}
			</div>
			<div class="col-md-3 checkout-order-summary">
				{{checkout-order-summary}}
			</div>
		</div>
	</div>
	{{shopping-cart-footer}}
</div>