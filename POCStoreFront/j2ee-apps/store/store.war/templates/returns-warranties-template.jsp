<div class="container-fluid returns-warranties-template">
	{{global-nav}}
	{{explore-modal}}
	<div class="returns-warranties-template-content">
		{{returns-warranties}}
		<hr>
	</div>
	{{footer-grid}}
</div>