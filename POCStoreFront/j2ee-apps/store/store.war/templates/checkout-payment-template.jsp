<div class="container-fluid checkout-payment-template default-template">
	{{checkout-nav}}
	<div class="default-margin">
		{{checkout-payment}}
	</div>
	{{shopping-cart-footer}}
	{{checkout-exit-confirmation-modal}}
    {{my-account-add-credit-card-modal}}
</div>