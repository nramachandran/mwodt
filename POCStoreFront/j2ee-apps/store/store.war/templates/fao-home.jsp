<div class="offcanvas gift-finder-menu gift-finder-template" id="gift-finder-menu-container">
    {{gift-finder-menu}}
</div>
<div class="fade-to-black" id="product-disabled-modal"></div>
<div class="container-fluid fao-home-template default-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				{{global-nav}}
				{{explore-modal}}
			</div>
        </div>
    </div>
    <div class="template-content">
        <div class="inline toy-advisor-tab">
            {{gift-finder-tab}}
        </div>
        <div id="scroll-detection"></div>
        <div class="scroll-section" id="section-1">
            <div class="section-bg">
                <div>
                    {{fao-home-text-block '{"data":"section-1"}'}}
                </div>
            </div>
        </div>
        <div class="scroll-section" id="section-2">
            {{fao-factoid}}
            <div class="section-bg">
                <div class="section-md">
                    {{fao-home-text-block '{"data":"section-2"}'}}
                   
                    <img class="schweetz-mid-image" src="toolkit/images/fao/home/schweetz-mid.png" />
                </div>
            </div>

        </div>
        <div class="scroll-section" id="section-3">
            {{fao-factoid}}
            <div class="section-bg">
                <div class="section-md">
                    {{fao-home-text-block '{"data":"section-3"}'}}
                </div>
            </div>

        </div>
        <div class="scroll-section" id="section-4">
            {{fao-factoid}}
            <div class="section-bg">
                <div class="section-md">
                    {{fao-home-text-block '{"data":"section-4"}'}}
<img src="toolkit/images/animal.png" class="muppet-bottom-left" />
<img src="toolkit/images/fozzie.png" class="muppet-bottom-right" />
<img src="toolkit/images/gonzo.png" class="muppet-top-right" />
<img src="toolkit/images/kermit.png" class="muppet-bottom-center" />
<img src="toolkit/images/mspiggy.png" class="muppet-top-left" />

                </div>
            </div>

        </div>
        <div class="scroll-section" id="section-5">
            {{fao-factoid}}
            <div class="section-bg">
                <div class="section-md">
                    {{fao-home-text-block '{"data":"section-5"}'}}
                </div>
            </div>

        </div>
	</div>
    <div class="scroll-section" id="section-6" style="width:100%;">
        {{footer-grid}}

        </div>
 
</div>
</div>
