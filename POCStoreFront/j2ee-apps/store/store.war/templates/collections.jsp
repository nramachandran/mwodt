﻿<div class="container-fluid collections-template">
    	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				{{global-nav}}
				{{explore-modal}}
			</div>
        </div>
    </div>
    <div class="template-content">
        <div class="fade-to-black" id="product-disabled-modal"></div>
        <div class="row">
            <div class="col-md-12">
                {{collection-header}}
            </div>
        </div>
        <div class="small-spacer"></div>
        {{collection-hero}}
        <div class="small-spacer"></div>
        <div class="small-spacer"></div>
        <div class="row">
            <div class="col-md-8">
                <h2>choose your items</h2>
                <div class="small-spacer"></div>
                {{collection-product-block}}
                <hr />
                {{collection-product-block}}
                <hr />
                {{collection-product-block}}
            </div>
            <div class="col-md-4">
                {{collections-sticky-price}}
            </div>
        </div>
        <hr />
        {{my-account-product-carousel}}
        {{recently-viewed-section}}
    </div>
    {{footer-grid}}
    {{size-chart-modal}}
    {{gallery-overlay-modal}}
        {{shopping-cart-overlay}}
    <div class="collections-tooltip" style="display: none">
        {{collections-tooltip}}
    </div>
</div>