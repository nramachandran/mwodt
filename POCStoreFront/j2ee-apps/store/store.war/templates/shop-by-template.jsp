<div class="container-fluid shop-by-template default-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				{{global-nav}}
				{{explore-modal}}
			</div>
        </div>
    </div>
    <div class="template-content">
        {{shop-by-breadcrumb}}
        {{shop-by-header}}
        {{shop-by-character-grid}}
            {{ad-zone-970-90}}
        {{shop-by-character-list}}

	</div>
    {{footer-grid}}
</div>