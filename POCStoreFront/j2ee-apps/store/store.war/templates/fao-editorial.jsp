<div class="container-fluid fao-editorial-template default-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				{{global-nav}}
				{{explore-modal}}
			</div>
        </div>
    </div>
    <div class="template-content">
        {{fao-editorial-scroll}}
    	{{fao-editorial-header}}
    	{{fao-editorial-fact-block}}
    	{{fao-editorial-highlights}}
        {{fao-editorial-quote-block}}
    	{{fao-editorial-special-services}}
        {{fao-editorial-store-locator}}
	</div>
    {{footer-grid}}
</div>
{{fao-store-details}}
{{gallery-overlay-modal}}