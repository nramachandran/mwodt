<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container-fluid default-template shopping-cart-template">
	<c:import url="/structures/shopping-cart-nav.jsp"></c:import>
	<div class="default-margin">
	<div class="col-md-6 custom-cms-spot">
		<div class="custom-cms-header">hassle-free returns!</div>
		<div class="custom-cms-subtxt">Do you need to return an item? <a href="" class="custom-cms-more">learn more</a></div>
	</div>	
	<c:import url="/components/shopping-cart-error-state.jspf"></c:import>
		<div class="row order-summary-top">
			<div class="col-md-8 cart-products">
				<c:import url="/components/toys-for-tots.jspf"></c:import>
					<c:import url="/structures/shopping-cart-product-information-error.jsp"></c:import>
					<hr>
					<c:import url="/structures/shopping-cart-product-information.jsp"></c:import>
					<hr>
					<c:import url="/structures/shopping-cart-product-information.jsp"></c:import>
					<hr>
					<c:import url="/structures/shopping-cart-product-information.jsp"></c:import>
					<hr>
					<c:import url="/structures/shopping-cart-product-information.jsp"></c:import>
					<hr>
					<c:import url="/structures/shopping-cart-product-information.jsp"></c:import>
					<hr>
					<c:import url="/structures/gift-card.jsp">
						<c:param name="classnames" value="partial" />
					</c:import>
					
					<hr>
					<c:import url="/structures/egift-card.jsp">
						<c:param name="classnames" value="partial e-gift-card" />
					</c:import>
					<hr>
			</div>
			<div id="order-summary-block" class="col-md-4 pull-right">
				<c:import url="/components/summary-block.jspf"></c:import>
				<c:import url="/components/shopping-cart-promo-code.jspf"></c:import>
			</div>
		</div>
	</div>
	<c:import url="/components/ad-zone-728-90.jspf"></c:import>
	
	
	

	
	<div class="shopping-cart-grey">
		<dsp:include page="/structures/customers-also-purchased.jsp">
			<dsp:param name="prodItem" value="${prodItem}"/>
		</dsp:include>
	</div>
	<c:import url="/components/shopping-cart-footer.jspf"></c:import>
</div>
</dsp:page>