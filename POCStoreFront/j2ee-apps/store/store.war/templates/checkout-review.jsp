<div class="container-fluid checkout-review-template default-template">
	{{checkout-nav}}
	<div class="default-margin">
		<div class="row">
			<div class="col-md-9 checkout-sticky-top">
				{{checkout-review-header}}
				{{h-divider}}
				{{checkout-review-payment-method}}
				<br>
				{{checkout-review-items-shipping-to}}
				{{checkout-review-product-block "gift-wrapped"}}
				{{h-divider}}
				{{checkout-review-product-block "purchased-protection-plan"}}
				{{h-divider}}
				{{checkout-review-product-block "purchased-protection-plan"}}
				<br>
				{{checkout-review-items-pickup}}
				<br>
				{{checkout-review-product-block "store-pickup"}}
				<br>
				{{checkout-review-digital-items '{"className":"donation", "data":"donations"}'}}
				<br>
				{{checkout-review-product-block '{"className":"partial", "data":"donation"}'}}
				<br>
				{{checkout-review-digital-items}}
				<br>
				{{checkout-review-product-block '{"className":"partial egift-card", "data":"egift-card"}'}}
				{{h-divider}}
				{{checkout-review-product-block '{"className":"partial egift-card", "data":"egift-card"}'}}
			</div>
			<div class="col-md-3 checkout-order-summary">
				{{checkout-order-summary}}
			</div>
		</div>
	</div>
	{{shopping-cart-footer}}
	{{checkout-review-processing-overlay}}
	{{remove-item-confirmation-modal}}
</div>