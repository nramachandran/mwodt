<div class="container-fluid my-account-template default-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				{{global-nav}}
				{{explore-modal}}
			</div>
        </div>
    </div>
    <div class="template-content">
    	<div>
			{{my-account-welcome-back}}
    	</div>
	    <div class="row">
	    	<div class="my-account-left-col">
				<hr>
				{{my-account-your-info}}
				<hr>
		    	{{my-account-order-history-zone}}
				{{my-account-features-zone}}
	    	</div>
	    	<div class="my-account-right-col">
	    		{{my-account-quick-help-zone}}
	    		{{my-account-gift-card-balance}}
	    		<div class="my-account-norton">
					<a href="http://www.symantec.com/page.jsp?id=seal-transition" target="_blank"><img src="toolkit/images/checkout/Global_Norton-Logo.jpg"></a>
	    		</div>
	    	</div>
	    </div>
	</div>
	{{fao-factoid}}
	{{my-account-product-carousel}}
    {{footer-grid}}
    {{quick-help-modal}}
    {{my-account-add-address-modal}}
    {{my-account-update-password-modal}}
    {{my-account-add-credit-card-modal}}
    {{my-account-delete-cancel-modal}}
    {{my-account-edit-credit-card-modal}}
    {{address-doctor-modal}}
    {{my-account-wish-list-modal}}
    {{order-history-detail-modal}}
</div>