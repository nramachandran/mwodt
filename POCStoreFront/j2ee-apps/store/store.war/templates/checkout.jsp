<div class="container-fluid default-template checkout-template min-width">
	{{my-account-add-address-modal}}
	{{checkout-nav}}
	<div class="checkout-shipping-content default-margin">
		<div class="row checkout-sticky-top">
			<div class="col-md-8">
				{{checkout-shipping-default}}
			</div>
			<div class="col-md-3 pull-right">
				{{checkout-order-summary}}
			</div>
		</div>
	</div>
	{{ad-zone-728-90}}
	{{shopping-cart-footer}}
</div>