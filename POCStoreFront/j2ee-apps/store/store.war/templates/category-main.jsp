<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container-fluid default-template category-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				<c:import url="/structures/global-nav.jsp"></c:import>
				<c:import url="/structures/explore-modal.jsp"></c:import>
			</div>
        </div>
    </div>
    <div class="row row-no-padding row-category-header">
        <div class="col-md-12 col-no-padding">	
			<c:import url="/components/category-hero-zone.jspf"></c:import>
        </div>
    </div>
    <div class="default-margin">
        <hr>
	   <c:import url="/components/ad-zone-970-90.jspf"></c:import>
        <hr>
    </div>
    <div class="default-margin">
        <div class="row row-no-padding">
            <div class="col-md-12 col-no-padding">
				<c:import url="/components/category-breadcrumb.jspf"></c:import>
    		</div>
    	</div>
    </div>
    <div class="row-max-width">
        <div class="row block-3-row row-strollers">
        	<div class="col-md-4 block-3-column"><c:import url="/components/category-image-link-block.jspf"></c:import></div>
        	<div class="col-md-4 block-3-column"><c:import url="/components/category-image-link-block.jspf"></c:import></div>
        	<div class="col-md-4 block-3-column"><c:import url="/components/category-image-link-block.jspf"></c:import></div>
        </div>
        <div class="brand-block-row">
			<c:import url="/components/category-brand-block.jspf"></c:import>
        </div>
    </div>	
    <div class="row-footer-margin">
        <div class="row row-no-padding">
            <div class="col-md-12 col-no-padding">
                <h2 class="now-trending-header">now trending</h2>
            </div>
        </div>
        <div class="row block-2-row">
            <div class="col-md-3 block-2-column"><c:import url="/structures/product-block.jsp"></c:import></div>
            <div class="col-md-3 block-2-column"><c:import url="/structures/product-block.jsp"></c:import></div>
            <div class="col-md-3 block-2-column"><c:import url="/structures/product-block.jsp"></c:import></div>
            <div class="col-md-3 block-2-column"><c:import url="/structures/product-block.jsp"></c:import></div>
        </div>
    </div>
    <div class="row default-margin">
        <div class="col-md-6 right-vr"><c:import url="/components/category-content-column.jspf"></c:import></div>
        <div class="col-md-6"><c:import url="/components/category-content-column.jspf"></c:import></div>
    </div>
    <div class="row default-margin">
        <div class="col-md-6 right-vr"><c:import url="/components/category-content-column.jspf"></c:import></div>
        <div class="col-md-6"><c:import url="/components/category-content-column.jspf"></c:import></div>
    </div>
    <div class="default-margin">
        <div class="row category-ad-row">
            <div class="col-md-3"><c:import url="/components/seo-block.jspf"></c:import></div>
            <div class="col-md-9">
				<c:import url="/components/ad-zone-728-90.jspf"></c:import>
            </div>
        </div>
        <hr class="bottom-hr">
    </div>
	<c:import url="/structures/footer-grid.jsp"></c:import>
</div>
</dsp:page>