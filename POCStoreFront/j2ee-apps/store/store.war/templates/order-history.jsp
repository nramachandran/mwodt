<div class="container-fluid order-history-template default-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				{{global-nav}}
				{{explore-modal}}
			</div>
        </div>
    </div>
    <div class="template-content">
    	<div>
    		{{order-history-breadcrumb}}
    		{{order-history-header}}
    		<hr>
    	</div>
	    <div class="row">
	    	<div class="order-history-left-col">
	    		{{order-history-table}}
	    	</div>
	    	<div class="order-history-right-col">
	    		{{my-account-quick-help-zone}}
	    		<div class="my-account-norton">
					<img src="toolkit/images/checkout/Global_Norton-Logo.jpg">
	    		</div>
	    	</div>
	    </div>
	</div>
	{{my-account-product-carousel}}
    {{footer-grid}}
    {{quick-help-modal}}
    {{order-history-detail-modal}}
</div>