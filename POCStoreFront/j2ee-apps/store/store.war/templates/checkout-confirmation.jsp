<div class="offcanvas pdp-shopping-cart-overlay-container">
    {{shopping-cart-overlay}}
</div>
<div class="fade-to-black" id="product-disabled-modal"></div>
<div class="container-fluid checkout-confirmation-template default-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
				{{global-nav}}
				{{explore-modal}}
        </div>
    </div>
	<div class="default-margin">
		{{checkout-order-complete}}
		<div class="row">
			<div class="col-md-12 col-no-padding">
				<hr />
			</div>
		</div>
		<div class="row checkout-sticky-top checkout-confirmation-content">
			<div class="col-md-9">
				<div class="row">
					<div class="col-md-5 payment-method">
						<span>payment method</span>
						<div>Master Card</div>
						<div>xxxx xxxx xxxx 1234</div>
					</div>
					<div class="col-md-5 billing-information">
						<span>billing information</span>
						<div>Aaron Cook</div>
						<div>534 Mt Vernon Rd</div>
						<div>Newark, OH 43055</div>
						<div>(614) 432-0778</div>
						<div>US</div>
					</div>
				</div>
				{{checkout-confirmation-items-shipping-to}}
				{{#iterate 2}}
					{{checkout-review-product-block "gift-wrapped purchased-protection-plan"}}
					<hr />
				{{/iterate}}
				{{checkout-review-product-block}}
				<br>
				<div class="row">
					{{checkout-review-items-pickup}}
				</div>
				<br>
				{{#iterate 2}}
					{{checkout-review-product-block "store-pickup"}}
					<hr />
				{{/iterate}}
				<div class="row">
					{{checkout-review-digital-items '{"className":"donation", "data":"donations"}'}}
				</div>
				<br>
				{{checkout-review-product-block '{"className":"partial", "data":"donation"}'}}
				<br>
				<div class="row">
					{{checkout-review-digital-items}}
				</div>
				<br>
				{{checkout-review-product-block '{"className":"partial egift-card", "data":"egift-card"}'}}
				{{h-divider}}
				{{checkout-review-product-block '{"className":"partial egift-card", "data":"egift-card"}'}}

			</div>
			<div class="col-md-3 pull-right">
				{{checkout-order-summary}}
			</div>
		</div>
	</div>
	{{footer-grid}}
</div>