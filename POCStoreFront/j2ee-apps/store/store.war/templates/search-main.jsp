<div class="container-fluid default-template search-template">
	{{global-nav}}
	{{explore-modal}}
    {{ad-zone-777-34}}
    <div class="lego-background">
        <div class="row-footer-margin">
            <div class="row">
                <div class="col-md-1">
                    <div class="lego-logo"></div>
                </div>    
                <div class="col-md-6">
                    <div class="brand-header"><a href="#">lego shop</a></div>
                </div>
            </div>    
            <div class="row">
                <div class="col-md-12">
                    {{brand-carousel}}
                </div>    
            </div>
        </div>
    </div>
    <hr />
    {{ad-zone-970-90}}
    <hr />
	<div class="row search-breadcrumb-block">
		{{search-breadcrumb}} 
	</div>
	<div class="suggested-search-block">
		{{suggested-search}}
	</div>
	<div class="row search-main-search-block">
		{{search-block}}
	</div>
    <div id="narrow-by-scroll" class="product-content">
        <div class="fixed-narrow-menu">
            <div class="row default-margin">
                <div class="col-md-6 col-no-padding col-narrow-by">
                    {{narrow-by-button}}{{showing-results}}
                </div>
                <div class="col-md-6 text-right col-narrow-by">    
                    {{sort-by}} 
                  
                </div>
            </div>
            <div class="row default-margin">
                <div class="col-md-12 col-no-padding">
                    {{narrow-by-filter-list}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12"><hr class="thick-hr"/></div>
            </div>

            <div class="offcanvas compare-menu sub-cat-container">
                {{sub-category-filter-window}}
            </div>
            <div class="offcanvas compare-overlay-menu">
                {{compare-overlay}}
            </div>
             <div class="compare-product-selection">
            <div class="row">
                <div class="col-md-3">
                    <div class="compare-header">
                        how do they compare?
                    </div>
                    <div class="small-compare-text">
                        Weigh product features to help decide which item fits you and your little one.
                    </div>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-10-percent no-compare-image image-1">
                    <img src="toolkit/images/product-compare-3.jpg" />
                </div>
                <div class="col-md-10-percent no-compare-image image-2">
                    <img src="toolkit/images/product-compare-3.jpg" />
                </div>
                <div class="col-md-10-percent no-compare-image image-3">
                    <img src="toolkit/images/product-compare-3.jpg" />
                </div>
                <div class="col-md-10-percent no-compare-image image-4">
                    <img src="toolkit/images/product-compare-3.jpg" />
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-2">
                    <button id="compare-products-button" disabled>compare now</button>
                </div>
            </div>
        </div>
    </div>

    <div class="sticky-menu-placeholder"></div>

    <div id="filtered-products">
        <div class="fade-to-black" id="product-disabled-modal"></div>
        <div class="row-footer-margin">
            <div class="row row-no-padding">
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block '{"className":"price-range no-select new-flag-mwodt", "data":"price-range"}'}}</div>
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "has-swatch-options top-seller-flag-mwodt on-sale"}}</div>
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "exclusive-flag-mwodt"}}</div>
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "sponsored-flag-mwodt"}}</div>
              
            </div>

            <div class="row row-no-padding">
                
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "no-select"}}</div>
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "no-select"}}</div>
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "no-select"}}</div>
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "no-select"}}</div>
             
            </div>
            <div class="row row-no-padding">
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block '{"className":"price-range no-select", "data":"price-range"}'}}</div>
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "no-select"}}</div>
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "no-select"}}</div>
                <div class="col-md-3 col-no-padding product-block-padding">{{product-block "no-select"}}</div>
            </div>
    
            <div class="load-more-products">
                {{#iterate 8}}
                <div class="row row-no-padding">
                    {{#iterate 4}}
                    <div class="col-md-3 col-no-padding product-block-padding">{{product-block "no-select"}}</div>
                    {{/iterate}}
                </div>
                {{/iterate}}
            </div>
            {{family-show-more}}
          </div>
    </div>
</div>
    <div class="search-main-footer row row-no-padding default-margin">
        <div class="row">
            {{ad-zone-970-90}}
        </div>
    </div>
    {{footer-grid}}
</div>