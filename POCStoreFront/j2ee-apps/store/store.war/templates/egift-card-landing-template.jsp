<div class="container-fluid egift-card-landing-template default-template">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
            <div class="fixed-nav">
				{{global-nav}}
				{{explore-modal}}
			</div>
        </div>
    </div>
    <div class="template-content">
        {{egift-card-breadcrumb}}
        {{gift-card-landing-description '{"data": "egiftcardheader"}'}}        
        <hr />
        <div class="egift-content">
            <h4>step 1 of 3: egift card selection</h4>
            <p>You can select up to 10 Gift Cards with any design. Once you've made your selections, click "Continue"</p>
            <hr />
            <div class="row">
                <div class="col-md-4">
                    {{egift-card-block '{"data": "card1"}'}}
                </div>
                <div class="col-md-4">
                    {{egift-card-block '{"data": "card2"}'}}
                </div>
                <div class="col-md-4">
                    {{egift-card-block '{"data": "card3"}'}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    {{egift-card-block '{"data": "card4"}'}}
                </div>
                <div class="col-md-4">
                    {{egift-card-block '{"data": "card5"}'}}
                </div>
                <div class="col-md-4">
                    {{egift-card-block '{"data": "card6"}'}}
                </div>
            </div>
            {{gift-card-continue-button}}
        </div>
        {{terms-conditions}}
	</div>
    {{footer-grid}}
</div>