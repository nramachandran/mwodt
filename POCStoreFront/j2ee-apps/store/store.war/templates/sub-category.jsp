<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="offcanvas baby-registry-menu sub-category">
	<c:import url="/structures/baby-registry.jsp"></c:import>
</div>
<div class="fade-to-black-2"></div>
<div class="sub-category-template default-template container-fluid">
	<div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
				<c:import url="/structures/global-nav.jsp"></c:import>
				<c:import url="/structures/explore-modal.jsp"></c:import>
        </div>
    </div>
    <div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
			<c:import url="/components/category-hero-zone.jspf"></c:import>
			<c:import url="/components/baby-registry-tab.jspf"></c:import>
        </div>
    </div>
    <div class="row-footer-margin">
	    <div class="row row-no-padding">
	        <div class="col-md-12 col-no-padding">
				<c:import url="/components/ad-zone-970-90.jspf"></c:import>
				<c:import url="/components/category-breadcrumb.jspf"></c:import>
                <div id="scroll-detection"></div>
			</div>
		</div>
	</div>
    <div class="row-footer-margin">
        <div class="row row-no-padding">
            <div class="col-md-12 col-no-padding">
            <div>browse by type</div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-12">
				<c:import url="/structures/carousel-image-block.jsp"></c:import>
        	</div>
    	</div>
    </div>
    <div id="narrow-by-scroll" class="product-content">
        <div class=" fixed-narrow-menu">
            <div class="row narrow-by-row">
                <div class="col-md-6 col-no-padding col-narrow-by">
					<c:import url="/components/narrow-by-button.jspf"></c:import>
					<c:import url="/components/showing-results.jspf"></c:import>
                </div>
                <div class="col-md-6 text-right col-narrow-by">
					<c:import url="/components/sort-by.jspf"></c:import>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-no-padding">
					<c:import url="/components/narrow-by-filter-list.jspf"></c:import>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12"><hr class="thick-hr"/></div>
            </div>

            <div class="offcanvas compare-menu sub-cat-container">
				<c:import url="/components/sub-category-filter-window.jspf"></c:import>
            </div>
            <div class="compare-overlay-menu">
				<c:import url="/structures/compare-overlay.jsp"></c:import>
            </div>
             <div class="compare-product-selection">
            <div class="row">
                <div class="col-md-3">
                    <div class="compare-header">
                        how do they compare?
                    </div>
                    <div class="small-compare-text">
                        Weigh product features to help decide which item fits you and your little one.
                    </div>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-10-percent no-compare-image image-1">
                    <img src="/assets/images/product-compare-3.jpg" />
                    <img class="close-compare-image" src="/assets/images/close-icon-sm.png"/>
                </div>
                <div class="col-md-10-percent no-compare-image image-2">
                    <img src="/assets/images/product-compare-3.jpg" />
                    <img class="close-compare-image" src="/assets/images/close-icon-sm.png"/>
                </div>
                <div class="col-md-10-percent no-compare-image image-3">
                    <img src="/assets/images/product-compare-3.jpg" />
                    <img class="close-compare-image" src="/assets/images/close-icon-sm.png"/>
                </div>
                <div class="col-md-10-percent no-compare-image image-4">
                    <img src="/assets/images/product-compare-3.jpg" />
                    <img class="close-compare-image" src="/assets/images/close-icon-sm.png"/>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-2">
                    <button id="compare-products-button" disabled>compare now</button>
                </div>
            </div>
        </div>
        </div>
        <div class="sticky-menu-placeholder">
        </div>

       
      <div id="filtered-products">
            <div class="fade-to-black" id="product-disabled-modal"></div>
            <div class="row-footer-margin">
                <div class="row row-no-padding">
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="new-flag-mwodt choose-product" />
						</c:import>					
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="new-flag-mwodt choose-product" />
						</c:import>	
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="exclusive-flag-mwodt choose-product" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="choose-product" />
						</c:import>
					</div>
                  
                </div>

                <div class="row row-no-padding">    
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select choose-product promo-block" />
						</c:import>					
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select choose-product" />
						</c:import>					
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select choose-product" />
						</c:import>						
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select choose-product" />
						</c:import>	
					</div>
                </div>
                <div class="row row-no-padding">
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select choose-product" />
						</c:import>						
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select choose-product" />
						</c:import>						
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select choose-product" />
						</c:import>						
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select choose-product" />
						</c:import>						
					</div>
                </div>
            
                <div class="load-more-products">
                    <div class="row row-no-padding">
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                    </div>
                    <div class="row row-no-padding">
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                    </div>
                    <div class="row row-no-padding">
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                    </div>
                    <div class="row row-no-padding">
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                    </div>
                    <div class="row row-no-padding">
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                    </div>
                    <div class="row row-no-padding">
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                    </div>
                    <div class="row row-no-padding">
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                    </div>
                    <div class="row row-no-padding">
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                        <div class="col-md-3 col-no-padding product-block-padding">
							<c:import url="/structures/product-block.jsp">
								<c:param name="classnames" value="no-select choose-product" />
							</c:import>
						</div>
                    </div>
                </div>
				<c:import url="/components/family-show-more.jspf"></c:import>
              </div>
        </div>
    </div>
	
	<div class="row">
        <div class="col-md-6 right-vr">
			<c:import url="/components/category-content-column.jspf"></c:import>
		</div>
        <div class="col-md-6">
			<c:import url="/components/category-content-column.jspf"></c:import>
		</div>
	</div>
    <div class="row">
        <div class="col-md-6 right-vr">
			<c:import url="/components/category-content-column.jspf"></c:import>
		</div>
        <div class="col-md-6">
			<c:import url="/components/category-content-column.jspf"></c:import>
		</div>
    </div>
	<div class="default-margin">
        <div class="row category-ad-row">
            <div class="col-md-3">
				<c:import url="/components/seo-block.jspf"></c:import>			
			</div>
            <div class="col-md-9">
				<c:import url="/components/ad-zone-728-90.jspf"></c:import>
            </div>
        </div>
    </div>
	<c:import url="/structures/footer-grid.jsp"></c:import>	
</div>
</dsp:page>