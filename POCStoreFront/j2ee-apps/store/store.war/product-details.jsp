<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	<c:import url="/global/jspf/mwodt-framework.jspf">
		<c:param name="layout" value="/templates/product-details-template.jsp"></c:param>
    	<c:param name="dataController" value="productDetail" />
    	<c:param name="dataAction" value="addToCart" />
	</c:import>
</dsp:page>