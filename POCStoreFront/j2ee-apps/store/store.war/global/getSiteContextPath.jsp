<dsp:page>
  <dsp:importbean bean="/atg/multisite/Site" var="currentSite"/>

  <c:set var="siteBaseURL" value="${currentSite.productionURL}" scope="request"/>
  <c:if test="${empty siteBaseURL}">
    <c:set var="siteBaseURL" value="${pageContext.request.contextPath}" scope="request"/>
  </c:if>
  <c:set var="siteBaseURLLength" value="${fn:length(siteBaseURL)}"/>
  <c:set var="siteBaseURLLastChar" value="${fn:substring(siteBaseURL, siteBaseURLLength-1, siteBaseURLLength)}"/>
  <c:if test="${siteBaseURLLastChar == '/'}">
    <c:set var="siteBaseURL" value="${fn:substring(siteBaseURL, 0, siteBaseURLLength - 1)}" scope="request"/>
  </c:if>

  <dsp:getvalueof var="siteContextPath" value="${siteBaseURL}" scope="request"/>
</dsp:page>
