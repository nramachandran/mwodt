<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="tab-pane" id="layawayMyAccount">
	<h3>my layaway account</h3>
	<p>View the status of my order(s), payments and schedule.</p>
	<div id="titleRow" class="row">
		<div class="col-md-3">
			layaway #
		</div>
		<div class="col-md-3">
			order status
		</div>
		<div class="col-md-3">
			date created
		</div>
		<div class="col-md-3">
			outstanding balance
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<p>45756187</p>
		</div>
		<div class="col-md-3">
			<img src="/assets/images/layaway/layaway-order-status.png" />
			<p class="inline">Pending</p>
		</div>
		<div class="col-md-3">
			<p>5-22-2014</p>
		</div>
		<div class="col-md-3">
			<p>$123.45</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<p><a href="#">41112356</a></p>
		</div>
		<div class="col-md-3">
			<img src="/assets/images/layaway/layaway-order-status.png" />
			<p class="inline">Active</p>
		</div>
		<div class="col-md-3">
			<p>3-23-2014</p>
		</div>
		<div class="col-md-3">
			<p>$212.45</p>
			<a href="#">make a payment</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<p><a href="#">15649857</a></p>
		</div>
		<div class="col-md-3">
			<img src="/assets/images/layaway/layaway-order-status.png" />
			<p class="inline">Paid in Full</p>
		</div>
		<div class="col-md-3">
			<p>2-18-2014</p>
		</div>
		<div class="col-md-3">
			<p>$0.00</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<p><a href="#">8675309</a></p>
		</div>
		<div class="col-md-3">
			<img src="/assets/images/layaway/layaway-order-status.png" />
			<p class="inline">Complete</p>
		</div>
		<div class="col-md-3">
			<p>2-14-2014</p>
		</div>
		<div class="col-md-3">
			<p>$0.00</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<p><a href="#">15649857</a></p>
		</div>
		<div class="col-md-3">
			<img src="/assets/images/layaway/layaway-order-status.png" />
			<p class="inline">Complete</p>
		</div>
		<div class="col-md-3">
			<p>3-06-2014</p>
		</div>
		<div class="col-md-3">
			<p>$0.00</p>
		</div>
	</div>
</div>
</dsp:page>