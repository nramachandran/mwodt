<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="cart-edit-popup">
	<div class="row">
		<div class="col-md-5">
			<div class="cart-edit-popup-image">
				<img src="/assets/images/checkout/Gifting_koala-product-Thumb-2.jpg"/>
			</div>
		</div>
		<div class="col-md-7">
			<h4>Koala Baby Girls' Short Sleeve Tiered Tunic</h4>
			<div class="price">$7.99</div>
			<div class="color-selector">
				Color: Black
				<div class='color-blocks'>
					<c:import url="/components/color-block.jspf">
						<c:param name="classnames" value="red color-selected" />
					</c:import>
					<c:import url="/components/color-block.jspf">
						<c:param name="classnames" value="orange" />
					</c:import>
				</div>
			</div>
			<div class="size-selector">
				Size: Medium
				<c:import url="/structures/sticky-sizes.jsp">
				</c:import>
			</div>
			<button class="cart-edit-update">update</button>
			<div class="not-available small-grey">
				<div class="grey-list-dot inline"></div>
				not available
				<div class="small-blue inline">
					<button class="learn-more" data-toggle="modal"
				data-target="#notifyMeModal">learn more</button>
				</div>
			</div>
			<div class="layaway small-grey">
				<div class="grey-list-dot inline"></div>
				layaway not available 
				<div class="small-blue inline">
					<a href="#" class="small-blue">learn more</a>
				</div>
			</div>
		</div>
	</div>
</div>
</dsp:page>