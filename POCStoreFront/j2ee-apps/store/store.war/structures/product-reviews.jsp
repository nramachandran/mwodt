<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="product-reviews">
	<c:import url="/components/product-review-tabs-header.jspf"></c:import>
	<div class="tab-content">
		<div id="customer-review-summary"class="tab-pane active">
			<c:import url="/components/product-review-average-rating.jspf"></c:import>
			<c:import url="/components/h-divider.jspf"></c:import>
			<c:import url="/components/product-review-helpful.jspf"></c:import>
			<c:import url="/components/product-reviews-section-header.jspf"></c:import>
			<c:import url="/components/h-divider.jspf"></c:import>
			<c:import url="/components/product-review-single.jspf"></c:import>
			<br />
			<br />
			<c:import url="/components/product-review-single.jspf"></c:import>
			<br />
			<br />
			<c:import url="/components/product-review-single.jspf"></c:import>
			<div class="show-more-reviews">
				<br />
				<br />
				<c:import url="/components/product-review-single.jspf"></c:import>
				<br />
				<br />
				<c:import url="/components/product-review-single.jspf"></c:import>
				<br />
				<br />
				<c:import url="/components/product-review-single.jspf"></c:import>
				<br />
			</div>
			<c:import url="/components/show-more-reviews.jspf"></c:import>
		</div>
		<div id="product-questions" class="tab-pane">
			<c:import url="/structures/product-review-product-questions-tab.jsp"></c:import>
		</div>
	</div>
</div>
</dsp:page>