<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="shipping-saved-address">
	<header>saved address</header>
	<div class="shipping-saved-address-content">
		<div class="avenir-heavy">select an address</div>
		<div>
            <select class="ship-to-dropdown">
                <option>Home</option>
                <option>Work</option>
            </select>
        </div>
		<header>
			Aaron Cook
		</header>
		<p>
			534 Mt Vernon Rd.
		</p>
		<p>
			Newark, OH 43055
		</p>
		<p>
			USA
		</p>
		<p>
			614-410-3122
		</p>
		<p>
			acook@resourceammirati.com
		</p>
	</div>
	<a href="#">add another address</a>
</div>
</dsp:page>