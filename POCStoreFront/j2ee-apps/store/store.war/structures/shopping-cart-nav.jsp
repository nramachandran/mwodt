<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="shopping-cart-nav">
	<c:import url="/components/shopping-cart-top-nav.jspf"></c:import>
	<nav class="shopping-cart-bottom-nav navbar navbar-default">
	  <div class="container-fluid">
		<ul class="nav navbar-nav navbar-left">
			<li>
				<div class="shopping-cart-header-text">
					shopping cart
				</div>
			</li>	
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li>
			<c:import url="/components/we-can-help-dropdown.jspf"></c:import>
			</li>
			<li class="signin-block">
				<button>sign in</button>
				<p>if you have an account</p>
			</li>
		</ul>
	  </div>
	</nav>
	<div>
	<c:import url="/components/ad-zone-777-34.jspf"></c:import>
	</div>
</div>
</dsp:page>