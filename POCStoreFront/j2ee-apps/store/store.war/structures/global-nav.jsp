<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="nav-bar-fixed global-nav">
	<c:import url="/components/global-navigation-top.jspf"></c:import>
	<c:import url="/components/global-navigation-bottom.jspf"></c:import>
</div>
<c:import url="/structures/global-nav-store-locator.jsp"></c:import>
<c:import url="/structures/my-account-popover.jsp"></c:import>
</dsp:page>