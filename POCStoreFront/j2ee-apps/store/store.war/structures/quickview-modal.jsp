<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal fade" id="quickviewModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-lg modal-dialog">
        <div class="modal-content sharp-border">
	        <img class="close-modal" data-dismiss="modal" src="/assets/images/my-account/close.png" alt="close modal" />
			<div class="mwodt-new-product-title"></div>
			<div class="row">
				<c:import url="/components/product-title.jspf"></c:import>
				<c:import url="/components/star-rating.jspf"></c:import>
			</div>
			<hr />
			<div class="row">
				<div class="col-md-7">
					<c:import url="/structures/quickview-product-hero-selector.jsp"></c:import>
				</div>
				<div class="col-md-5">
					<dsp:include page="/structures/quickview-price.jsp">
						<dsp:param name="skuLimit" param="skuLimit"/>
					</dsp:include>
				</div>
			</div>
			<div class="row quickview-footer">
				<c:import url="/components/quickview-pdp-thumbnail-images.jspf"></c:import>
			</div>
		</div>
	</div>	
</div>
</dsp:page>