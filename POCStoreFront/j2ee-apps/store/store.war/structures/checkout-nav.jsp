<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="checkout-nav">
	<c:import url="/components/shopping-cart-top-nav.jspf"></c:import>
	<div class="checkout-nav-bottom">
		<div class="checkout-nav-bottom-row">
			<span href="#" class="active checkout-nav-bottom-tab checkout-tab">
				<div class="secure-checkout-lock inline"></div>
				checkout
			</span>			
			<button href="#" class="checkout-nav-bottom-tab shipping-tab">
				<img src="/assets/images/checkout/Blue-Checkmark.png"  />shipping
			</button>
			<button href="#" class="checkout-nav-bottom-tab gifting-tab">
				<img src="/assets/images/checkout/Blue-Checkmark.png"  />gifting
			</button>			
			<button href="#" class="checkout-nav-bottom-tab pickup-tab">
				<img src="/assets/images/checkout/Blue-Checkmark.png"  />pickup
			</button>
			<button href="#" class="checkout-nav-bottom-tab payment-tab">
				<img src="/assets/images/checkout/Blue-Checkmark.png"  />payment
			</button>
			<button style="display:none;" href="#" class="checkout-nav-bottom-tab review-tab">
				<img src="/assets/images/checkout/Blue-Checkmark.png"  />review
			</button>			
			<c:import url="/components/we-can-help-dropdown.jspf"></c:import>
		</div>
	</div>
</div>
</dsp:page>