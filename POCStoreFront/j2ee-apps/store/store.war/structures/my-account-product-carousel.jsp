<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="my-account-product-carousel">
	<div class="product-carousel-title">
		recommended for you
	</div>
	<div id="slider-product-block-container" class="slider-product-block-container">
	  	<div class="slider-product-block-loading-div" u="loading">
	    	<div></div>
	    </div>
		<div u="slides" id="slides-product-block" class="slides-product-block">
			<div class="product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
			<div class="product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
			<div class="product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
			<div class="product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
			<div class="product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
			<div class="product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
			<div class="product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
			<div class="product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
		</div>

		<div u="navigator" id="multi-slide-bullets" class="jssor-bullet">
	    	<div u="prototype"></div>
	  	</div>
		  
	  	<span id="multi-carousel-left-arrow" u="arrowleft" class="jssora11l">
	  	</span>

		<span id="multi-carousel-right-arrow" u="arrowright" class="jssora11r">
	  	</span>
  	</div>
</div>
</dsp:page>