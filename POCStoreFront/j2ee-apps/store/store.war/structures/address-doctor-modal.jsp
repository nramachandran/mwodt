<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal fade" id="addressDoctorModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content sharp-border">
			<dsp:include page="/components/address-doctor.jspf">
			</dsp:include>
        </div>
    </div>
</div>
</dsp:page>