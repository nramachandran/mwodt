﻿<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="store-pickup-items-accordion">
    <div class="row accordion-header">
        <div class="col-xs-11">
            <h3>show items</h3>
        </div>
        <div class="col-xs-1">
            <span class="accordion-expand-icon"></span>
            <span class="accordion-minimize-icon"></span>
        </div>
    </div>
    <div>
        <div class="row">
            <div class="col-xs-12">
				<c:import url="/components/checkout-confirmation-block.jspf"></c:import>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
				<c:import url="/components/checkout-confirmation-block.jspf"></c:import>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
				<c:import url="/components/checkout-confirmation-block.jspf"></c:import>
            </div>
        </div>
    </div>
</div>
</dsp:page>