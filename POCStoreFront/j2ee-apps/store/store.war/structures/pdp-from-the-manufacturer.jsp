<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="pdp-from-the-manufacturer">
	<div class="manufacturer-content">
		<h2>from the manufacturer</h2>
		<h3>features</h3>
		<span>just like in the movie!</span> 
		<div class="read-more-manufacturer">
			<p>
				In Disney&#39;s Frozen, Anna is a spirited daydreamer who ends a wintry spell with the help of her sister Elsa, Kristoff, Olaf the snowman and her own determination. Girls will love re-creating Anna&#39;s magical adventures in Arendelle with this delightful doll. Featuring true-to-movie details, Anna wears her signature outfit fans of the film will instantly recognize! Anna&#39;s beautiful gown incorporates the magic of winter and the character&#39;s own rich ancestry. Her enchanting outfit is a one-of-a-kind design inspired by the unforgettable Disney movie. The multi-colored bodice features sparkling pink swirls and floral designs. The removable skirt shimmers with a design inspired by traditional Norwegian rosemaling.
			</p>
			<img src="/mwodt-static/assets/images/manufacture-images/anna-doll.jpg" alt="image not found"/>
			<div class="row">
				<div class="col-md-6">
					<img src="/mwodt-static/assets/images/manufacture-images/anna-accessories.jpg" alt="image not found" />
				</div>
				<div class="col-md-6">
					<p><span>Winter Wonderland Accesories!</span><br />
						Because Anna hails from icy Arendelle, she wears accessories to keep her warm, like snow-friendly black boots and a high-collared pink cape with a golden clasp. Her beautiful hair is styled in two classic braids and topped with a sparkling pink tiara. In this lovely look, she is ready to brave the ice and snow and find happiness for the entire  kingdom - and herself!
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<p><span>Re-imagine the Movie Magic!</span><br />
						Girls will love playing out favorite scenes from the film or creating new adventures for this spirited and inspiring character. To expand the Frozen fun and adventure, look for Anna&#39;s sister, Elsa of Arendelle, too!
					</p>
				</div>
				<div class="col-md-6">
					<img src="/mwodt-static/assets/images/manufacture-images/anna-package.jpg" alt="image not found" />
				</div>
			</div>
		</div>
	</div>
	<div class="read-more text-center" id="readMoreManufacturer">read more</div>
</div>
</dsp:page>