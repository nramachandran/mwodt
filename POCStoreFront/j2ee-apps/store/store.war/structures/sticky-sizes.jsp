<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="sticky-sizes">
	<c:import url="/components/size-option.jspf">
		<c:param name="classname" value="size-selected" />
		<c:param name="contentData" value="newborn" />
	</c:import>
	<c:import url="/components/size-option.jspf">
		<c:param name="contentData" value="0-3-months" />
	</c:import>
	<c:import url="/components/size-option.jspf">
		<c:param name="contentData" value="3-6-months" />
	</c:import>	
	<c:import url="/components/size-option.jspf">
		<c:param name="contentData" value="6-9-months" />
	</c:import>
	<c:import url="/components/size-option.jspf">
		<c:param name="contentData" value="9-12-months" />
	</c:import>
	<c:import url="/components/size-option.jspf">
		<c:param name="contentData" value="12-18-months" />
	</c:import>
	<c:import url="/components/size-option.jspf">
		<c:param name="contentData" value="18-24-months" />
	</c:import>
	<c:import url="/components/size-option.jspf">
		<c:param name="contentData" value="24-months" />
	</c:import>	
	<c:import url="/components/size-option.jspf">
		<c:param name="contentData" value="2T" />
	</c:import>
	<c:import url="/components/size-option.jspf">
		<c:param name="contentData" value="3T" />
	</c:import>	
	<c:import url="/components/size-option.jspf">
		<c:param name="contentData" value="4T" />
	</c:import>
	<c:import url="/components/size-option.jspf">
		<c:param name="contentData" value="5T" />
	</c:import>
</div>
</dsp:page>