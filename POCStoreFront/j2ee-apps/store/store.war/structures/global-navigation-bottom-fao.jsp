<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="global-navigation-bottom-fao">
	<img src="/assets/images/fao/fao-logo.png" alt="fao logo" />
	<c:import url="/components/global-nav-fao-shop-by.jspf"></c:import>
	<c:import url="/components/global-nav-fao-link.jspf">
		<c:param name="classnames" value="fao-shipping" />
	</c:import>
	<c:import url="/components/global-nav-fao-link-with-menu.jspf"></c:import>
	<c:import url="/components/global-nav-my-account.jspf"></c:import>
	<c:import url="/components/global-nav-fao-link.jspf"></c:import>
	<c:import url="/components/global-nav-fao-search.jspf"></c:import>
</div>
<!--[if IE]>
    <style>
    	.fao-shipping a{
			font-size:10px;
    	}
    	.global-nav-fao-link-with-menu a {
			font-size:11px;
    	}
    </style>
<![endif]-->
</dsp:page>