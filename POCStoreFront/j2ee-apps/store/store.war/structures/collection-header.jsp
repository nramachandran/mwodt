<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="collection-header">
    <h2>Sugar Plum</h2>
    <hr/>
	<c:import url="/components/collections-breadcrumb.jspf"></c:import>
</div>
</dsp:page>