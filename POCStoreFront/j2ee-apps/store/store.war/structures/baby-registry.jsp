<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="tse-scrollable gift-finder-wrapper registry">
	<div class="tse-content">
		<div class="baby-registry">
			<div class="baby-registry-content">
				<dsp:include page="/components/baby-registry-hero.jspf">
				</dsp:include>
				<dsp:include page="/components/h-divider.jspf">
				</dsp:include>
				<div class="baby-registry-container">
					<div class="row">
						<div class="col-md-4">
							<dsp:include page="/components/baby-registry-find.jspf">
							</dsp:include>
						</div>
						<div class="col-md-4">
							<dsp:include page="/components/baby-registry-create.jspf">
							</dsp:include>
						</div>
						<div class="col-md-4">
							<dsp:include page="/components/baby-registry-update.jspf">
							</dsp:include>
						</div>
					</div>
				</div>
				<dsp:include page="/components/h-divider.jspf">
				</dsp:include>
				<div class="baby-registry-footer">
				<a href="./template-sub-category.html">
					<div class="continue-shopping-left-arrow inline"></div>back to registry
				</a>
				</div>
			</div>
		</div>
	</div>
</div>
</dsp:page>