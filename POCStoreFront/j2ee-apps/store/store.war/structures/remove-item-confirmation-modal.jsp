<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal fade" id="removeItemConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content sharp-border">
        	<span class="clickable"><img data-dismiss="modal" src="/assets/images/my-account/close.png" /></span>
            <div>
            	<h2>are you sure you want to remove this item?</h2>
            	<a href="#">no, back to review</a>
            	<a href="#">yes, remove this item</a>
            </div>
        </div>
	</div>
</div>
</dsp:page>