<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="global-nav-store-locator-tooltip popup-marker">
	<div class="global-nav-store-locator popover" role="tooltip">
		<div class="arrow"></div>
		<c:import url="/components/global-nav-find-a-store.jspf"></c:import>
		<c:import url="/components/global-nav-nearby-stores.jspf"></c:import>		
		<c:import url="/components/global-nav-store-details.jspf"></c:import>		
	</div>
</div>
</dsp:page>