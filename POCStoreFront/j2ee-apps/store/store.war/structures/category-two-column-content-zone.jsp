﻿<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container">
    <div class="row">
        <div class="col-md-6 right-vr">
			<c:import url="/components/category-content-column.jspf"></c:import>
		</div>
        <div class="col-md-6">
			<c:import url="/components/category-content-column.jspf"></c:import>
		</div>
    </div>
</div>
</dsp:page>