<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="customers-also-purchased">
	<h2>customers also purchased</h2>
	<div class="row row-no-padding">
			<div class="col-md-3 col-no-padding product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
			<div class="col-md-3 col-no-padding product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
			<div class="col-md-3 col-no-padding product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
			<div class="col-md-3 col-no-padding product-block-padding">
				<c:import url="/structures/product-block.jsp">
					<c:param name="classnames" value="no-select" />
				</c:import>
			</div>
	</div>
</div>
</dsp:page>