<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal fade" id="signInModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content sharp-border">
			<c:import url="/components/sign-in-overlay.jspf"></c:import>
        </div>
    </div>
</div>	
</dsp:page>