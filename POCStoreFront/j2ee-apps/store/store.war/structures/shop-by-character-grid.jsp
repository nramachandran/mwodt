<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="shop-by-character-grid">
	<div class="row">
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="angry-birds.png" />
				</c:import>
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="barbie.png" />
				</c:import>
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="cars.png" />
				</c:import>
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="angry-birds.png" />
				</c:import>
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="barbie.png" />
				</c:import>
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="cars.png" />
				</c:import>
			</div>
	</div>
	<div class="row">
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="disney-princess.png" />
				</c:import>				
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="dream-lites.png" />
				</c:import>						
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="frozen.png" />
				</c:import>						
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="disney-princess.png" />
				</c:import>				
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="dream-lites.png" />
				</c:import>						
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="frozen.png" />
				</c:import>						
			</div>
	</div>
	<div class="row">
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="hello-kitty.png" />
				</c:import>					
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="jake.png" />
				</c:import>					
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="marvel.png" />
				</c:import>
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="hello-kitty.png" />
				</c:import>					
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="jake.png" />
				</c:import>					
			</div>
			<div class="col-md-2">
				<c:import url="/components/shop-by-character-block.jspf">
					<c:param name="imgName" value="marvel.png" />
				</c:import>
			</div>
	</div>
</div>
</dsp:page>