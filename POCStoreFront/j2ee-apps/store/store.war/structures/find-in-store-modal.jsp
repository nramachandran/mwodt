<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal fade" id="findInStoreModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content sharp-border">
            <div class="find-in-store-overlay">
                <div class="row">
                    <img class="clickable close-modal" data-dismiss="modal" src="toolkit/images/find-in-store/close.png" />
                    <div class="col-md-6 border-right product-side">
                        <h3>Disney Frozen Sparkle Anna of Arendelle Doll</h3>
                        <img class="product-thumb" src="toolkit/images/find-in-store/find-in-store-product-thumb.png" />
                        <div class="options-text">
                            <span>Color:</span><span class="selected-color"> Red</span>
                        </div>
                        <div class="colors">
							<c:import url="/components/color-block.jspf"></c:import>
                        </div>
                        <div class="options-text">
                            <span>Size:</span><span class="selected-size"></span>
                        </div>
                        <br/>
                        <div class="sticky-quantity">
                            <div class="options-text">
                                Qty: <span class="amount"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="find-in-store-results">
                            <div class="store-locator-results">
                                <header>
                                    8 nearby stores
                                    <p>for 43215</p>
                                    <p><a class="change-location">change location</a></p>
                                    <div class="find-in-store-search">
                                        <img src="toolkit/images/find-in-store/close.png"/>
                                        <h2>find a store</h2>
                                        <p>enter an address city, state or ZIP</p>
                                        <div class="find-input-group">
                                            <input type="text"/>
                                            <button>find</button>
                                        </div>
                                    </div>
                                </header>
                                <ul id="store-locator-results-tabs" class="store-locator-results-tabs nav nav-tabs">
                                    <li class="active">
                                        <a class="product-review-tab-text" href="#customer-review-summary" data-toggle="tab">All</a>
                                    </li>
                                    <li>
                                        <a class="product-review-tab-text" href="#customer-review-summary" data-toggle="tab">Toys "R" Us</a>
                                    </li>
                                    <li>
                                        <a class="product-review-tab-text" href="#customer-review-summary" data-toggle="tab">Babies "R" Us</a>
                                    </li>
                                </ul>
                                <div id="store-locator-results-scrollable" class="tse-scrollable">
                                    <div class="tse-content">
                                        <ul class>
                                            <li>
											<c:import url="/components/find-in-store-results-single.jspf">
												<c:param name="classnames" value="mwodt-result" />
											</c:import>
											</li>
                                            <hr class="mwodt-result">
                                            <li>
											<c:import url="/components/find-in-store-results-single-unavailable.jspf">
												<c:param name="classnames" value="mwodt-result" />
											</c:import>
											</li>
                                            <hr class="mwodt-result">
                                            <li>
											<c:import url="/components/find-in-store-results-single.jspf">
												<c:param name="classnames" value="mwodt-result" />
											</c:import>
											</li>
                                            <hr class="mwodt-bru-border">
                                            <li>
											<c:import url="/components/find-in-store-results-single.jspf">
												<c:param name="classnames" value="bru-result" />
											</c:import>											
											</li>
                                            <hr class="bru-result">
                                            <li>
											<c:import url="/components/find-in-store-results-single.jspf">
												<c:param name="classnames" value="bru-result" />
											</c:import>	
											</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="store-locator-tooltip-store-details" class="tse-scrollable">
					<div class="tse-content">
						<section class="store-address">
							<p>3301 Coral Way</p>
							<p>Miami, FL 33145</p>
							<p>(305) 443-9004</p>
							<a>get directions</a>
						</section>
						<section class="store-hours">
							<div>
								<p>Mon-Fri:</p>
								<p>Sat:</p>
								<p>Sun:</p>
							</div>
							<div>
								<p>10:00am-9:00pm</p>
								<p>9:00am-9:00pm</p>
								<p>10:00am-7:00pm</p>
							</div>
						</section>
						<br/>
						<header>store services</header>
						<ul>
							<li>
								layaway
								<p>Shop today and take time to pay!</p>
								<a>learn more</a>
							</li>
							<li>
								parenting classes
								<p>Informative classes on everything baby from breastfeeding to infant CPR.</p>
								<a>learn more</a>
							</li>
							<li>
								personal registry advisor
								<p>Schedule a one-on-one appointment with a Personal Registry Advisor to help you create the perfect registry.</p>
								<a>learn more</a>
							</li>
						</ul>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
</dsp:page>