<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="main-toy-grid">
    <div class="row row-no-padding">
        <div class="col-md-3 col-no-padding toy-grid-padding">
            <c:import url="/components/toy-grid-medium-vertical.jspf"></c:import>
        </div>
        <div class="col-md-6 col-no-padding toy-grid-padding">
            <c:import url="/components/toy-grid-large.jspf"></c:import>
        </div>
        <div class="col-md-3 col-no-padding toy-grid-padding">  
            <div>
                <c:import url="/components/toy-grid-small.jspf"></c:import>
            </div>
             <div>   
                <c:import url="/components/toy-grid-small.jspf"></c:import>
             </div>
        </div>
    </div>

    <div class="row row-no-padding">
        <div class="col-md-3 col-no-padding toy-grid-padding">
            <c:import url="/components/toy-grid-small.jspf"></c:import>
        </div>
        <div class="col-md-3 col-no-padding toy-grid-padding">
            <c:import url="/components/toy-grid-small.jspf"></c:import>
        </div>
        <div class="col-md-6 col-no-padding toy-grid-padding">
            <c:import url="/components/toy-grid-medium-horizontal.jspf"></c:import>
        </div>
    </div>
</div>
</dsp:page>