<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<dsp:getvalueof var="prodItem" param="product"/>
<dsp:getvalueof var="skuItem" param="sku"/>

<div class="product-details">
	<div>
		<c:import url="/components/pdp-thumbnail-images.jspf"></c:import>
		<div class="product-details-caption">
			<c:if test="${not empty prodItem.brand}">By:<span class="blue-txt fix-right">${prodItem.brand}</span></c:if>
			<c:if test="${not empty mfgAgeMsg}"><span class="fix-left">&#xB7;</span>MFG Age ${mfgAgeMsg}</c:if>
		</div>
		<div class="top-product-details-content">
			<div>
				<c:import url="/components/ad-zone-300-250.jspf"></c:import>
				<c:import url="/components/open-r-us-card.jspf"></c:import>
			</div>
			<div class="why-we-love-it">
				<h2>why we love it </h2>
				<p>Anna&#39;s a brave, honest character who strives to rescue her sister, save her family and protect the kingdom. I love that children can use her to recreate their favorite scenes from the &#8220;Frozen&#8221; movie and imagine new adventures built around a spirited and faithful female character.</p>
				<span class="signature">&mdash;Julianne Bisnaire</span>
			</div>
			<h2>product description</h2>
			<p>
			 	Bring the &#8220;Frozen&#8221; movie to life with this Anna of Arendelle doll. Wearing her signature outfit with multi-colored bodice, rich pink swirls and beautiful flowers at the hem of her dress, Anna reminds children of her character&#39;s spirit and determination to save the kingdom from eternal winter. 
			</p>
		</div>
		<div class="product-details-content">				
			<h2>things to know</h2>
			
			<ul>
				<li><span>Don&#39;t forget the batteries.<span></li>
				<li><span>Gift wrap available for this item.<span></li>
				<li><span>Usually leaves warehouse in 1-2 business days.<span></li>
				<li><span>Product is available for international shipping.<span></li>
			</ul>	
			<h2>what's included</h2>
			<div class="read-more-content">
				<ul>
					<li><span>Don&#39;t forget the batteries</span></li>
					<li><span>Elegant gown with cape</span></li>
					<li><span>Multicolored bodice</span></li>
					<li><span>Skirt</span></li>
					<li><span>Accessories: black boots and a pink tiara</span></li>
				</ul>
				<h2>features</h2>
				<ul>
					<li>Inspired by the hit new Disney animated film, Frozen</li>
					<li>Anna is a free-spirited daydreamer determined to save her kingdom</li>
					<li>Elaborate gown features a rich, multi-colored bodice decorated with vibrant pink swirl, sparkles and flowers</li>
					<li>Her bright blue skirt is accented with traditional Norwegian inspired design</li>
					<li>Girls will love reenacting their favorite scenes from the movie</li>
				</ul>
				<h2>additional info</h2>
				<ul class="additional-info">
					<li>&#8220;R&#8221;Web#:929610</li>
					<li>SKU:128F9516</li>
					<li>UPC/EAN/ISBN:746775263980</li>
					<li>Manufacturer #: Y9958</li>
					<li>Shipping Weight: 0</li>
					<li>Product Weight:0.4 pounds</li>
					<li>Product Dimensions (in inches):12.5 x 4.5 x 2.3</li>
				</ul>
				<h2>how to get it</h2>
				<p><span>shipping info:</span></p>
				<ul>
					<li>This item can be shipped to the entire United States including Alaska, Hawaii, and all U.S. territories including Puerto Rico</li>
					<li>This item can also be shipped to APO/FPO addresses and to P.O. Boxes in all 50 states</li>
				</ul>
				<p><span>shipping method:</span></p>
				<ul>
					<li>This item may be shipped via Standard Shipping, Expedited Shipping or Express</li>
				</ul>
			</div>
			<div class="read-more text-center" id="readmore">read more</div> 
		</div>
	</div>
	<c:import url="/components/product-video-modal.jspf"></c:import>
</div>
</dsp:page>