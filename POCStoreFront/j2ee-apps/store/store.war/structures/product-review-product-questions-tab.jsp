<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="product-questions-tab-content">
	<c:import url="/components/product-review-product-questions-header.jspf"></c:import>
	<c:import url="/components/h-divider.jspf"></c:import>
	<div class="product-questions">
		<c:import url="/components/product-review-product-question-single.jspf"></c:import>		
		<c:import url="/components/h-divider.jspf"></c:import>
		<c:import url="/components/product-review-product-question-single.jspf"></c:import>
		<c:import url="/components/h-divider.jspf"></c:import>
		<c:import url="/components/product-review-product-question-single.jspf"></c:import>
		<div class="show-more-questions">
			<c:import url="/components/h-divider.jspf"></c:import>
			<c:import url="/components/product-review-product-question-single.jspf"></c:import>
		</div>
		<c:import url="/components/show-more-questions.jspf"></c:import>
	</div>
</div>
</dsp:page>