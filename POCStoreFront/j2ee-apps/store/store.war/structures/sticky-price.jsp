<dsp:page>

	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:importbean bean="/atg/commerce/pricing/priceLists/PriceDroplet"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>

	<dsp:getvalueof var="prodItem" param="product"/>
	<div class="sticky-price">
		<div class="sticky-price-contents">
			<dsp:droplet name="ErrorMessageForEach">
			  <dsp:param param="CartModifierFormHandler.formExceptions" name="exceptions"/>        
			  <dsp:oparam name="output">
			  	<div>
			      <font color="red"><dsp:valueof param="message" valueishtml="true"/></font>
			   </div>
			  </dsp:oparam>
			</dsp:droplet>
			<div class="buy-1">
				Buy 1 get 1 FREE select Disney items<span>&#xB7;</span><a href="#">see terms</a> 
			</div>
			<div class="prices">
				<c:if test="${fn:length(prodItem.childSKUs) eq 1}">
					<dsp:droplet name="PriceDroplet">
					  <dsp:param name="product" param="product"/>
					  <dsp:param name="sku" param="product.childSKUs[0]"/>
					
					  <dsp:oparam name="output">
					    <dsp:setvalue param="theListPrice" paramvalue="price"/>
					    <%-- check whether there is sale price? --%>
					    <dsp:getvalueof var="profileSalePriceList" bean="Profile.salePriceList"/>
					    <c:choose>
					      <c:when test="${not empty profileSalePriceList}">
					        <dsp:droplet name="PriceDroplet">
					          <dsp:param name="priceList" bean="Profile.salePriceList"/>
					
					          <dsp:oparam name="output">
					            <dsp:getvalueof var="salePrice" vartype="java.lang.Double" param="price.listPrice"/>
					            <dsp:getvalueof var="price" vartype="java.lang.Double" param="theListPrice.listPrice"/>
					            <span class="crossed-out-price">
					            	$20.99
					            	<!-- Uncomment this if dynamic value needs to be displayed -->
					              <%-- <dsp:include page="/global/formattedPrice.jsp">
					                <dsp:param name="price" value="${price}"/>
					              </dsp:include> --%>
					            </span>
					            <span class="sale-price">
					              <dsp:include page="/global/formattedPrice.jsp">
					                <dsp:param name="price" value="${salePrice}"/>
					              </dsp:include>
					            </span>
					          </dsp:oparam>
					
					          <dsp:oparam name="empty">
					            <dsp:getvalueof var="price" vartype="java.lang.Double" param="theListPrice.listPrice"/>
					            <span class="sale-price">
					              <dsp:include page="/global/formattedPrice.jsp">
					                <dsp:param name="price" value="${price}"/>
					              </dsp:include>
					            </span>
					          </dsp:oparam>
					        </dsp:droplet>
					      </c:when>
					      <c:otherwise>
					        <c:out value="otherwise"/>
					        <span class="sale-price">
					          <dsp:include page="/global/formattedPrice.jsp">
					            <dsp:param name="price" value="${price}"/>
					          </dsp:include>
					        </span>
					      </c:otherwise>
					    </c:choose>
					  </dsp:oparam>
					</dsp:droplet>
				</c:if>
			</div>
			<div class="member-price">
				You save $11 (48%)
			</div>
			<div class="btn-group special-offer">
				<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				<div class="special-offer-text">
				<div class="plus-sign"></div>
					3 special offers
				</div>
				</button>
				<ul class="dropdown-menu" role="menu">
					<img class="special-offer-dropdown-arrow" src="toolkit/images/arrow.svg">
					<li class="small-grey">
						<div class="inline blue-bullet"></div>
						FREE Shipping on ANY purchase of $49 or more. Surcharges may apply on heavy/large items.
						<div class="small-blue inline"><a href="#">details</a></div>
					</li>
					<li class="small-grey">
						<div class="inline blue-bullet"></div>
						Limited time only! FREE Shipping on LEGO. 
						<div class="small-blue inline"><a href="#">see all items</a></div>
					</li>
				</ul>
			</div>
		<div class="color-text">
			<span>Color:</span> Red
		</div>
		<div class="colors">
			<c:import url="/components/color-block.jspf">
				<c:param name="classnames" value="red color-selected" />
			</c:import>
			<c:import url="/components/color-block.jspf">
				<c:param name="classnames" value="yellow" />
			</c:import>
			<c:import url="/components/color-block.jspf">
				<c:param name="classnames" value="orange" />
			</c:import>
		</div>
		<div class="color-text">
			<span>Size:</span> newborn
		</div>
			<div class="sizes">
				<c:import url="/structures/sticky-sizes.jsp"></c:import>
				<span class="size-chart">
					<a href="#" data-toggle="modal" data-target="#sizeChartModal">size chart</a>
				</span>
			</div>
		<div class="sticky-quantity">
			<div class="color-text">
				Qty:
			</div>
			<c:import url="/components/stepper.jspf">
				<c:param name="markerClass" value="sticky-section"/>
			</c:import>
			<dsp:getvalueof var="skuLimit" param="skuLimit" />
			<input type="hidden" id="skuLimit" value="${skuLimit}">
			<div class="limit-1 inline">
				<c:if test="${not empty skuLimit}">
				       limit ${skuLimit} items per customer
				</c:if>
			</div>
		</div>
		<div class="add-to-cart-section">
			<button id="btnAddToCart" class="add-to-cart">add to cart </button>
		</div>	
		<a href="javascript:void(0);" id="invokerSCModal" data-toggle="modal" data-target="#shoppingCartModal"></a>
		<div class="add-to">
			<div class="add-to-text inline">
				add to
			</div>
			<div class="inline">
				<a href="#" class="small-orange">baby registry</a>
			</div>
			<div class="grey-arrow-icon inline"></div>
			<div class="inline">
				<a href="#" class="small-orange">wish list</a>
			</div>
			<div class="grey-arrow-icon inline"></div>
		</div>
		<div class="product-availability">
			<div class="available-online">
				<div class="green-list-dot inline"></div>
				available online & in store  <span>&#xB7;</span>
				<div class="small-blue inline">
					<button class="learn-more" data-toggle="modal"
				data-target="#notifyMeModal">learn more</button>
				</div>
			</div>
			<c:if test="${prodItem.shipWindowMax.matches('^[1-9][0-9]*$') && prodItem.shipWindowMin.matches('^[1-9][0-9]*$')}">
				<div class="leaves-warehouse">
					<div class="green-list-dot inline"></div>
					    <fmt:parseNumber var="shipWindowMax" integerOnly="true" type="number" value="${(prodItem.shipWindowMax / 24)}" />
					    <fmt:parseNumber var="shipWindowMin" integerOnly="true" type="number" value="${(prodItem.shipWindowMin / 24)}" />
					usually leaves warehouse in ${shipWindowMin} - ${shipWindowMax} full business days &#xB7;
					<div class="inline">
						<a href="#">details</a>
					</div>
				</div>
			</c:if>
			<div class="find-in-store">
				<div class="green-location-icon inline"></div>
				available at 3619 W Dublin-Granville Rd. &#xB7;
				<div class="inline">
					<a data-toggle="modal" data-target="#findInStoreModal" href="#">find in store</a>
				</div>
			</div>
		</div>
		
		<c:import url="/components/warning-modal.jspf"></c:import>
		<c:import url="/components/size-chart-modal.jspf"></c:import>
		<c:import url="/components/notify-me-modal.jspf"></c:import>
	</div>
	<c:import url="/components/social-banner.jspf"></c:import>
	</div>
	
	<dsp:form id="frmAddToCart" formid="addToCart" action="${siteContextPath}/product-details.jsp" method="post" >
	    <dsp:input bean="CartModifierFormHandler.addItemToOrderSuccessURL" type="hidden"
	               value="${siteContextPath}/product-details.jsp?addToCart=true&productId=${prodItem.repositoryId}"/>
	    <dsp:input bean="CartModifierFormHandler.addItemToOrderErrorURL" type="hidden"
	               value="${siteContextPath}/product-details.jsp?addToCart=false&productId=${prodItem.repositoryId}"/>
      <dsp:input bean="CartModifierFormHandler.productId" paramvalue="product.repositoryId" type="hidden"/>
      <dsp:input bean="CartModifierFormHandler.quantity" type="hidden" value="1" id="txtQuantity"/>
      <dsp:input bean="CartModifierFormHandler.catalogRefIds" paramvalue="product.childSKUs[0].repositoryId" type="hidden"/>
	  <dsp:input bean="CartModifierFormHandler.addItemToOrder" type="hidden" value="Add to cart" priority="-100"/>		
	  <%-- <dsp:input bean="CartModifierFormHandler.addItemToOrder" type="submit" value="Add to cart"/>	 --%>	
	</dsp:form>
	
<script type="text/javascript">
	var isAddtoCart = '${param.addToCart}';
	var addedProductId = '<dsp:valueof bean="CartModifierFormHandler.productId"/>';
</script>
	
</dsp:page>
