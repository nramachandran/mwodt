<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="store-locator-struct">
	<div class="googleMapGroup">
		<c:import url="/components/store-locator-find-store.jspf"></c:import>
		<c:import url="/structures/store-locator-results.jsp"></c:import>
		<c:import url="/components/store-locator-store-details.jspf"></c:import>
		<div id="googleMap"></div>
	</div>
</div>
</dsp:page>