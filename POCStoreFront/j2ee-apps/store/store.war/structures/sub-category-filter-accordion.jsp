﻿<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="sub-category-filter-accordion">
    <ul>
        <li>
           <div>
                <div class="sub-category-filter-header">
                    category
                    <div class="sub-category-filter-header-button-section">
                        <button class="sub-category-filter-header-button" type="button">
                            <label class="sub-category-filter-header-button-label"><span class="expanded"></span><span class="minimized"></span></label>
                        </button>
                    </div>
                </div>
                <ul>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-category-1" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-category-1">standard-size (58)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-category-2" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-category-2">lightweight (99)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-category-3" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-category-3">jogging (70)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-category-4" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-category-4">double (63)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-category-5" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-category-5">triple (46)</label>
                    </li>
                </ul>
            </div>
            <div class="sub-category-filter-accordion-hr"></div>
        </li>
        <li>                      
            <div>
                <div class="sub-category-filter-header">
                    price
                    <div class="sub-category-filter-header-button-section">
                        <button class="sub-category-filter-header-button" type="button">
                            <label class="sub-category-filter-header-button-label"><span class="expanded"></span><span class="minimized"></span></label>
                        </button>
                    </div>
                </div>
                <ul>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-price-1" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-price-1">under $10 (58)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-price-2" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-price-2">$10-20 (99)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-price-3" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-price-3">$20-$30 (70)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-price-4" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-price-4">$30-$40 (63)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-price-5" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-price-5">$40-$50 (46)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-price-6" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-price-6">$50-$75 (84)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-price-7" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-price-7">$75-$100 (589)</label>
                    </li>
                </ul>
            </div>
            <div class="sub-category-filter-accordion-hr"></div>
        </li>
        <li>
           <div>
                <div class="sub-category-filter-header">
                    brand
                    <div class="sub-category-filter-header-button-section">
                        <button class="sub-category-filter-header-button" type="button">
                            <label class="sub-category-filter-header-button-label"><span class="expanded"></span><span class="minimized"></span></label>
                        </button>
                    </div>
                </div>
                <ul>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-brand-1" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-brand-1">GB (58)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-brand-2" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-brand-2">Stokke (99)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-brand-3" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-brand-3">Bugaboo (70)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-brand-4" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-brand-4">Baby (63)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-brand-5" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-brand-5">Maclaren (46)</label>
                    </li>
                </ul>
            </div>
            <div class="sub-category-filter-accordion-hr"></div>
        </li>
        <li>
           <div>
                <div class="sub-category-filter-header">
                    color
                    <div class="sub-category-filter-header-button-section">
                        <button class="sub-category-filter-header-button" type="button">
                            <label class="sub-category-filter-header-button-label"><span class="expanded"></span><span class="minimized"></span></label>
                        </button>
                    </div>
                </div>
                <ul>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-color-1" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-color-1">red (52)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-color-2" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-color-2">blue (99)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-color-3" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-color-3">green (70)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-color-4" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-color-4">yellow (63)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-color-5" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-color-5">purple (46)</label>
                    </li>
                </ul>
            </div>
            <div class="sub-category-filter-accordion-hr"></div>
        </li>
        <li>
           <div>
                <div class="sub-category-filter-header">
                    max. weight
                    <div class="sub-category-filter-header-button-section">
                        <button class="sub-category-filter-header-button" type="button">
                            <label class="sub-category-filter-header-button-label"><span class="expanded"></span><span class="minimized"></span></label>
                        </button>
                    </div>
                </div>
                <ul>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-maxweight-1" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-maxweight-1">under 10 lbs (58)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-maxweight-2" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-maxweight-2">10-20 lbs (99)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-maxweight-3" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-maxweight-3">20-30 lbs (70)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-maxweight-4" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-maxweight-4">30-40 lbs (63)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-maxweight-5" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-maxweight-5">over 40 lbs (46)</label>
                    </li>
                </ul>
            </div>
            <div class="sub-category-filter-accordion-hr"></div>
        </li>
        <li>
           <div>
                <div class="sub-category-filter-header">
                    weight
                    <div class="sub-category-filter-header-button-section">
                        <button class="sub-category-filter-header-button" type="button">
                            <label class="sub-category-filter-header-button-label"><span class="expanded"></span><span class="minimized"></span></label>
                        </button>
                    </div>
                </div>
                <ul>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-weight-1" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-weight-1">under 5 lbs (58)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-weight-2" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-weight-2">5-10 lbs (99)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-weight-3" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-weight-3">10-20 lbs (70)</label>
                    </li>
                    <li>
                        <input class="sub-category-filter-option-checkbox" id="sub-cat-weight-4" type="checkbox" />
                        <label class="sub-category-filter-option-checkbox-label" for="sub-cat-weight-4">over 20 lbs (63)</label>
                    </li>
                </ul>
            </div>
            <div class="sub-category-filter-accordion-hr"></div>
        </li>
    </ul>
</div>
</dsp:page>