<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="quickview-sticky-price">
	<div class="sticky-price-contents">
		<div class="buy-1">
			Buy 1 get 1 FREE select Disney items<span>&#xB7;</span><a href="#">see terms</a> 
		</div>
		<div class="prices">
			<span class="crossed-out-price">
				$20.99
			</span>
			<span class="sale-price">
				$9.99
			</span>
		</div>
		<div class="member-price">
			Rewards&#8220;R&#8221;Us members:  $10.99
		</div>
		<div class="btn-group special-offer">
	  		<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<div class="special-offer-text">
			<div class="plus-sign"></div>
				3 special offers
			</div>
	  		</button>
			<ul class="dropdown-menu" role="menu">
			  	<img class="special-offer-dropdown-arrow" src="/assets/images/arrow.svg">
			    <li class="small-grey">
			    	<div class="inline blue-bullet"></div>
			    	FREE Shipping on ANY purchase of $49 or more. Surcharges may apply on heavy/large items.
			    	<div class="small-blue inline"><a href="#">details</a></div>
		    	</li>
			    <li class="small-grey">
			    	<div class="inline blue-bullet"></div>
			    	Limited time only! FREE Shipping on LEGO. 
			    	<div class="small-blue inline"><a href="#">see all items</a></div>
			    </li>
	  		</ul>
		</div>
	<div class="color-text">
		<span>Color:</span> Red
	</div>
	<div class="colors">
		<c:import url="/components/color-block.jspf">
			<c:param name="classnames" value="red color-selected" />
		</c:import>
		<c:import url="/components/color-block.jspf">
			<c:param name="classnames" value="yellow" />
		</c:import>
		<c:import url="/components/color-block.jspf">
			<c:param name="classnames" value="orange" />
		</c:import>
	</div>
	<div class="color-text">
		<span>Size:</span>
		<span class="size-chart">
			<a href="#" data-toggle="modal" data-target="#quickviewSizeChartModal">size chart</a>
		</span>
	</div>
	<div class="sizes">
		<c:import url="/structures/sticky-sizes.jsp"></c:import>
	</div>
	<div class="sticky-quantity">
		<div class="color-text">
			Qty:
		</div>
		<c:import url="/components/stepper.jspf"></c:import>
		<div class="limit-1 inline">
		<dsp:getvalueof var="skuLimit" param="skuLimit" />
			limit ${skuLimit} items per customer
		</div>
	</div>
	<div class="add-to-cart-section">
		<button class="add-to-cart"  data-toggle="modal" data-target="#quickviewWarningModal">add to cart </button>
	</div>	
	<div class="add-to">
		<div class="add-to-text inline">
			add to
		</div>
		<div class="inline">
			<a href="#" class="small-orange">baby registry</a>
		</div>
		<div class="grey-arrow-icon inline"></div>
		<div class="inline">
			<a href="#" class="small-orange">wish list</a>
		</div>
		<div class="grey-arrow-icon inline"></div>
	</div>
	<div class="product-availability">
		<div class="available-online">
			<div class="green-list-dot inline"></div>
			available online & in store  <span>&#xB7;</span>
			<div class="small-blue inline">
				<button class="learn-more" data-toggle="modal"
			data-target="#quickviewNotifyMeModal">learn more</button>
			</div>
		</div>
		<div class="leaves-warehouse">
			<div class="green-list-dot inline"></div>
			usually leaves warehouse in 1-2 full business days  &#xB7;
			<div class="inline">
				<a href="#">details</a>
			</div>
		</div>
		<div class="find-in-store">
			<div class="green-location-icon inline"></div>
			available at 3619 W Dublin-Granville Rd.  &#xB7;
			<div class="inline">
				<a href="#">find in store</a>
			</div>
		</div>
	</div>
</div>
<c:import url="/components/social-banner.jspf"></c:import>
</div>
</dsp:page>


