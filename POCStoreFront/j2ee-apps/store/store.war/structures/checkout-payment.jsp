<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="checkout-payment">
	<div class="row">
		<div class="col-md-8">
			<c:import url="/components/shopping-cart-error-state.jspf">
				<c:param name="classnames" value="checkout-payment-error-state" />
			</c:import>
			<c:import url="/components/checkout-payment-header.jspf"></c:import>
			<c:import url="/components/checkout-credit-component.jspf"></c:import>
			<c:import url="/components/h-divider.jspf"></c:import>
			<c:import url="/components/checkout-payment-paypal.jspf"></c:import>
			<c:import url="/components/h-divider.jspf"></c:import>
			<c:import url="/components/checkout-payment-pay-in-store.jspf"></c:import>
			<c:import url="/components/h-divider.jspf"></c:import>
			<c:import url="/components/checkout-payment-giftcard.jspf"></c:import>
			<c:import url="/components/h-divider.jspf"></c:import>
			<c:import url="/components/checkout-payment-promo-code.jspf"></c:import>
			<c:import url="/components/h-divider.jspf"></c:import>
			<c:import url="/components/checkout-payment-rewardsrus.jspf"></c:import>
			<c:import url="/components/h-divider.jspf"></c:import>
		</div>
		<div class="col-md-3 pull-right">
			<c:import url="/components/checkout-order-summary.jspf"></c:import>
		</div>
	</div>
</div>
</dsp:page>