<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="add-gift-options">
	<header>
		add gift options to other items in your order
	</header>
	<c:import url="/components/checkout-registry-product.jspf"></c:import>
</div>
</dsp:page>