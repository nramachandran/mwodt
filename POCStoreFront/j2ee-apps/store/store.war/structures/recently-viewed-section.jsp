﻿<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="recently-viewed-section-header">
    <h3>recently viewed items</h3>
</div>
<div class="row row-no-padding recently-viewed-section-content">
	<div class="col-md-3 col-no-padding product-block-padding"><c:import url="/structures/product-block.jsp"></c:import></div>
	<div class="col-md-3 col-no-padding product-block-padding"><c:import url="/structures/product-block.jsp"></c:import></div>
	<div class="col-md-3 col-no-padding product-block-padding"><c:import url="/structures/product-block.jsp"></c:import></div>
	<div class="col-md-3 col-no-padding product-block-padding"><c:import url="/structures/product-block.jsp"></c:import></div>
</div>
</dsp:page>