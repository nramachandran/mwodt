<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="toy-grid-2">
    <div class="row row-no-padding">
        <div class="col-md-3 col-no-padding">
			<c:import url="/components/toy-grid-medium-vertical.jspf"></c:import>
        </div>
        <div class="col-md-6 col-no-padding">
			<c:import url="/components/toy-grid-large.jspf"></c:import>
        </div>
        <div class="col-md-3 col-no-padding">  
            <div>
				<c:import url="/components/toy-grid-small.jspf"></c:import>
            </div>
             <div>
				<c:import url="/components/toy-grid-small.jspf"></c:import>
            </div>
        </div>
    </div>

    <div class="row row-no-padding">
        <div class="col-md-3 col-no-padding">
			<c:import url="/components/toy-grid-small.jspf"></c:import>
        </div>
        <div class="col-md-3 col-no-padding">
			<c:import url="/components/toy-grid-small.jspf"></c:import>
        </div>
        <div class="col-md-6 col-no-padding">
            <c:import url="/components/toy-grid-medium-horizontal.jspf"></c:import>
        </div>
    </div>
</div>
</dsp:page>