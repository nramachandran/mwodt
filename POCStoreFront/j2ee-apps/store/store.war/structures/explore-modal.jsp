<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/multisite/Site" />
<dsp:importbean bean="/com/store/browse/droplet/ExtractDimensionValueCacheIdDroplet" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/userprofiling/Profile" />

	
<div id="overlay" class="clearfix"></div>
<div id="exploreBox" class="clearfix">
    <img id="arrow" src="/mwodt-static/assets/images/arrow-grey.svg" />
    <div id="shopByContainer">
    
    
    <dsp:getvalueof var="vContextPath" vartype="java.lang.String" value="${originatingRequest.contextPath}" />
	<dsp:getvalueof var="vProductionUrl" vartype="java.lang.String" bean="Site.productionURL" />
	
	<div class="searchByCategories">
            <ul>
			    <dsp:droplet name="ForEach">
			  		<dsp:param bean="Profile.catalog.rootNavigationCategory.childCategories" name="array"/>
				  	<dsp:oparam name="output">
				  		<dsp:getvalueof var="rootCatName" param="element.displayName" />
				  		<dsp:getvalueof var="rootCatId" param="element.id" />
				  		<dsp:getvalueof var="vCatType" param="element.categoryItemType" />
				  		<dsp:droplet name="ExtractDimensionValueCacheIdDroplet">
			                 <dsp:param name="categoryId" param="element.id" />
			                 <dsp:oparam name="output">
			                 
			                 <dsp:getvalueof var="rootDimValue" param="dimensionValueCacheId" />
			                 <li><dsp:a class="product-image" href="${vProductionUrl}/browse?N=${rootDimValue}">${rootCatName}</dsp:a></li>
			                 </dsp:oparam>
			            </dsp:droplet>	
				  	</dsp:oparam>
			  	</dsp:droplet>
  			</ul>
        </div>
        
        <div id="recentProducts">
            <div id="recentProductsTSE" class="tse-scrollable">
                <div class="tse-content">
                    <div class="recently-viewed-header">
                        <img src="/mwodt-static/assets/images/clock-icon.png" alt="clock-icon" /><header>recently viewed</header><span>&#xB7;</span>
                        <a href="#">clear</a>
                    </div>
					<c:import url="/components/explore-recently-viewed-single.jspf"></c:import>
					<c:import url="/components/explore-recently-viewed-single.jspf"></c:import>
					<c:import url="/components/explore-recently-viewed-single.jspf"></c:import>
					<c:import url="/components/explore-recently-viewed-single.jspf"></c:import>
					<c:import url="/components/explore-recently-viewed-single.jspf"></c:import>
					<c:import url="/components/explore-recently-viewed-single.jspf"></c:import>
                </div>
            </div>
        </div>
        <div id="build-learn-page">
			<c:import url="/components/shopby-build-learn.jspf"></c:import>
        </div>
    </div>
</div>
</dsp:page>