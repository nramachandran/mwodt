<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:choose>
	<c:when test="${not empty param.classnames}" >
		<c:set var="classNames" value="${param.classnames}" />
	</c:when>
	<c:otherwise>
		<c:set var="classNames" value="" />
	</c:otherwise>
</c:choose>

<div class="shopping-cart-product-information <c:out value="${classNames}"/>">
	<div class="product-information-header">
		<div class="shopping-cart-product-title inline">
		    <a href="#">Disney Frozen Sparkle Anna of Arendalle Doll</a>
		</div>
		<div class="shopping-cart-product-price inline">
			$16.99
		</div>
	</div>
	<div class="shopping-cart-product-content">
		<div class="row row-no-padding">
			<div class="col-md-2">
				<div>
					<img class="shopping-cart-product-image" src="/mwodt-static/assets/images/cart/shopping-anna-doll.jpg" />
				</div>
			</div>
			<div class="col-md-5 product-cart-description">
				<div class="shopping-cart-price">$16.99</div>
				<div class="shopping-cart-item-number">Item#: 019985</div>
				<div class="shopping-cart-stock">
					<div class="green-bullet inline"></div>
					<div class="in-stock inline">in stock</div>
				</div>
				<div class="shopping-cart-color">Color: Finland</div>
				<div class="shopping-cart-size">Size: M</div>
				<a class="edit-product-button closed" href="javascript:;">edit</a>
				<c:import url="/structures/shopping-cart-edit-product-popover.jsp"></c:import>
				<div class="quantity">
					<div>Qty:</div>
					<c:import url="/components/stepper.jspf"></c:import>
				</div>
				<div class="shopping-cart-gift">
					<div class="checkbox-sm-off"></div>
					<div class="shopping-cart-gift-image inline"></div>
					<div class="inline">
						this is a gift
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<label for="ship-to-home">
					<div class="ship-to-home">
						<input id="ship-to-home" class="shipping-radio-btn inline" type="radio" name="shipping-type" value="ship-to-home" />
						Ship-To-Home
					</div>
					<div class="padded-details">
						<div class="shipping-details">
							Leaves warehouse in 1 - 2 full business days.
						</div>
						<a href="#" class="see-terms" data-toggle="popover" tabindex="0">see terms</a>
					</div>
				</label>
				<br />
				<label for="free-store-pickup">
					<div class="free-store-pickup">
						<input id="free-store-pickup" class="shipping-radio-btn inline" type="radio" name="shipping-type" value="free-store-pickup" /> Free Store Pickup
					</div>
					<div class="padded-details">
						<span>Toys "R" Us - Columbus [8910]</span>
						<a href="#">select a store</a>
					</div>
				</label>
			</div>
		</div>
		<div class="row row-no-paddidng shopping-cart-surprise">
			<div class="col-md-6 col-md-offset-2">
				<div>
					<div class="shopping-cart-surprise-image inline"></div>
					<div class="shopping-cart-remove inline">don't spoil the surprise <span>&#xB7;</span><a href="#">learn more</a></div>
				</div>
			</div>
			<div class="col-md-4 text-right">
				<a class="remove-cart-item" href="#">remove</a>
			</div>
		</div>
	</div>
	<c:import url="/components/protection-plan.jspf"></c:import>
</div>
</dsp:page>