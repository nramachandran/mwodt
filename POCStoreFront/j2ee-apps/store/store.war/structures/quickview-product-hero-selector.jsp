<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	<div class="quickview-block">
		<div class="our-exclusive">
			<p>our</p>
			<p>exclusive</p>
		</div>
		<img id="quickview-hero-image" class="hero-image-zoom" src="/assets/images/product-1-hero.jpg" class="blocks">
	</div>
	<c:import url="/components/quickview-gallery-overlay-modal.jspf"></c:import>
</dsp:page>