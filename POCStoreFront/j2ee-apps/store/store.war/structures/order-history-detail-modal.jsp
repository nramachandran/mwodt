<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal fade" id="orderHistoryDetailModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog order-history-detail-modal">
        <div class="modal-content sharp-border">
        	<div class="modal-title">
	        	order detail
	        	<img data-dismiss="modal" src="/assets/images/my-account/close.png" />
	        	<div>
	        		ordered on january 16, 2015 order #1259654523
	        	</div>
	        </div>
	        <div class="tse-scrollable">
                <div class="tse-scroll-content tse-order-history-content">
                    <div class="tse-content order-history-detail-modal-content">
						<div class="row">
							<div class="col-md-8 col-no-padding">
								<div class="row">
									<div class="col-md-5 col-no-padding">
										<div class="order-detail-small-header">billing address</div>
										<br>
										<div class="address">
											<span>Aaron Cook</span>
											<div>534 Mt Vernon Rd</div>
											<div>Newark, OH 43055</div>
										</div>
										<br>
										<p class="returns-warranties">
											help with returns & warranties
										</p>
									</div>
									<div class="col-md-5 col-no-padding">
										<div class="order-detail-small-header">payment method</div>
										<br>
										<div class="payment-method">
											<div class="card-number">
												<div class="mastercard-logo-sm inline"></div>
												<div class="inline">xxxxxxxxxxxx4567</div>
											</div>
											<div>$1,048.95 balance was added to this card</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-no-padding pull-right">
								<c:import url="/components/order-history-order-summary.jspf"></c:import>
							</div>
						</div>
								<c:import url="/components/h-divider.jspf"></c:import>
						<div class="pull-right">
							<div class="norton-secured">
								<img src="/assets/images/checkout/Global_Norton-Logo.jpg" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-9 col-no-padding">
								<c:import url="/components/checkout-review-items-shipping-to.jspf">
									<c:param name="classnames" value="items-shipping-to-no-edit" />
								</c:import>
								<c:import url="/components/checkout-review-product-block.jspf">
									<c:param name="classnames" value="product-block-no-edit gift-wrapped purchased-protection-plan" />
								</c:import>
								<c:import url="/components/h-divider.jspf"></c:import>
								<c:import url="/components/checkout-review-product-block.jspf">
									<c:param name="classnames" value="product-block-no-edit" />
								</c:import>
								<c:import url="/components/h-divider.jspf"></c:import>
								<c:import url="/components/checkout-review-product-block.jspf">
									<c:param name="classnames" value="product-block-no-edit" />
								</c:import>
								<c:import url="/components/h-divider.jspf"></c:import>
								<c:import url="/components/checkout-review-product-block.jspf">
									<c:param name="classnames" value="product-block-no-edit gift-wrapped" />
								</c:import>
								<c:import url="/components/h-divider.jspf"></c:import>
								<c:import url="/components/checkout-review-shipping-no-tracking-number.jspf"></c:import>
								<c:import url="/components/h-divider.jspf"></c:import>
								<c:import url="/components/checkout-review-product-block.jspf">
									<c:param name="classnames" value="product-block-no-edit has-shipping-surcharge" />
								</c:import>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</dsp:page>