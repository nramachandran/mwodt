<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:getvalueof var="items" bean="ShoppingCart.current.commerceItems"/> 

<div class="modal fade" id="shoppingCartModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content sharp-border">
            <div class="shopping-cart-overlay">
				<dsp:include page="/components/shopping-cart-overlay-header.jspf">
				 	<dsp:param name="itemCount" bean="ShoppingCart.current.CommerceItemCount"/>
				</dsp:include>
                <hr class="shopping-cart-overlay-hr" />
                <div class="cart-content">
                    <div class="row">
                        <div class="left-section col-md-8">
                            <div class="tse-scrollable">
                                <div class="tse-content">
                                	<c:forEach var="currentItem" items="${items}" varStatus="status">
										<dsp:include page="/structures/cart-overlay-product-info.jsp">
											<dsp:param name="currentItem" value="${currentItem}"/>
										</dsp:include>
									</c:forEach>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="margin-buffer">
								<dsp:include page="/components/shopping-cart-overlay-summary.jspf">
									<dsp:param name="order" bean="ShoppingCart.current"/>
								</dsp:include>
                            </div>
                            <div class="margin-buffer">
								<c:import url="/components/shopping-cart-overlay-checkout-button.jspf"></c:import>
                            </div>
                            <div class="margin-buffer">
								<c:import url="/components/shopping-cart-overlay-shipping-bar.jspf"></c:import>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</dsp:page>