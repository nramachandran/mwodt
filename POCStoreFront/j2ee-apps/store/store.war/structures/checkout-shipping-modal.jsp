<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal fade" id="checkoutShippingModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog warning-modal">
        <div class="modal-content sharp-border">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                <div>
					<c:import url="/components/checkout-shipping-address-form.jspf"></c:import>
            	</div>
        	</div>
    	</div>	
	</div>
</div>
</dsp:page>