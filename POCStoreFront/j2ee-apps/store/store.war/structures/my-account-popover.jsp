<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div style="display:none; position:absolute;" id="my-account-popover-struct" class="my-account-popover-struct popover">
	<div class="arrow"></div>

	<c:import url="/components/my-account-popover-logged-in.jspf"></c:import>
	<c:import url="/components/my-account-logged-out.jspf"></c:import>

</div>
</dsp:page>