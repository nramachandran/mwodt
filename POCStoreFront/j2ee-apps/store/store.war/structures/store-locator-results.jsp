<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="store-locator-results" class="store-locator-results">
	<header>
		13 stores nearby
		<p>NW North River Dr. Miami, FL 33136</p>
		<p><a id="change-location">change location</a></p>
		<c:import url="/components/store-locator-find-store-dismiss.jspf"></c:import>
	</header>
	<ul id="store-locator-results-tabs" class="store-locator-results-tabs nav nav-tabs">
		<li id="filterAll"class="active">
			<a class="product-review-tab-text" href="#customer-review-summary" data-toggle="tab">All</a>
		</li>
		<li id="filterTRU">
			<a class="product-review-tab-text" href="#customer-review-summary" data-toggle="tab">Toys "R" Us</a>
		</li>
		<li id="filterBRU">
			<a class="product-review-tab-text" href="#customer-review-summary" data-toggle="tab">Babies "R" Us</a>
		</li>
	</ul>
	<div id="store-locator-results-scrollable" class="tse-scrollable">
		<div class="tse-content">
			<ul>
			</ul>
			<div id="show-more-stores" class="show-more"><span>show more</span></div>
		</div>
	</div>
</div>
</dsp:page>