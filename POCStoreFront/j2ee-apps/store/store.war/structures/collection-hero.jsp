﻿<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="collection-hero">
    <div class="row">
        <div class="pull-left">
            <img src="/assets/images/collections/collection-image.png" />
            <div class="zoom-icon" data-toggle="modal" data-target="#galleryOverlayModal"><img src="/assets/images/zoom-icon.png" /></div>
        </div>
        <div class="col-md-7">
            
            <p class="collections-hero-description">
                Sugar Plum brought to you by CoCaLo baby is a stunning collection inspired by the beauty of nature features
                 a unique combination of textured fabrics including corduroy, sherpa, felt, plaids, stripes, and beautiful
                 floral prints in plum, pink, green and ivory. It is embellished with adorable dimensional butterflies and
                 flower appliques and accented with organza and plum & green ribbon details.
            </p>
        </div>
    </div>
</div>
</dsp:page>