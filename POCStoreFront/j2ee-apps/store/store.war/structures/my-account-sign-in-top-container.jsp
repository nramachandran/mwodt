<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="my-account-sign-in-top-container">
	<c:import url="/components/my-account-sign-in-main-heading.jspf"></c:import>
	<div class="sign-in-columns-container">
		<div class="row">
			<div class="col-md-4">
				<c:import url="/components/my-account-sign-in-col.jspf">
					<c:param name="classnames" value="returning-customers" />
					<c:param name="data" value="returning-customers" />
				</c:import>
			</div>
			<div class="col-md-4">
				<c:import url="/components/my-account-sign-in-col.jspf">
					<c:param name="classnames" value="create-account" />
					<c:param name="data" value="create-account" />
				</c:import>
			</div>
			<div class="col-md-4">
				<c:import url="/components/my-account-sign-in-col.jspf">
					<c:param name="classnames" value="order-status" />
					<c:param name="data" value="order-status" />
				</c:import>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:import url="/components/shop-with-confidence.jspf"></c:import>
			</div>
		</div>
	</div>
</div>
</dsp:page>