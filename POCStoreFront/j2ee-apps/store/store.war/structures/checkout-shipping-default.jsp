<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="checkout-shipping-default">
	<c:import url="/components/checkout-shipping-default-header.jspf"></c:import>
	<c:import url="/components/h-divider.jspf"></c:import>
	<c:import url="/components/checkout-shipping-address-form.jspf"></c:import>
	<c:import url="/components/h-divider.jspf"></c:import>
	<c:import url="/components/checkout-select-shipping-method.jspf"></c:import>
	<c:import url="/components/h-divider.jspf"></c:import>
	<c:import url="/components/checkout-shipping-gifting.jspf"></c:import>
</div>
</dsp:page>