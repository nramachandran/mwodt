<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:choose>
	<c:when test="${not empty param.classnames}" >
		<c:set var="classNames" value="${param.classnames}" />
	</c:when>
	<c:otherwise>
		<c:set var="classNames" value="" />
	</c:otherwise>
</c:choose>
<div class="product-block product-block-unselected <c:out value="${classNames}"/>">
	<div class="product-flag"></div>
	<div class="product-choose">
		<c:import url="/components/product-compare.jspf"></c:import>
	</div>
	<div class="product-selection">
		<c:import url="/components/selection.jspf"></c:import>
	</div>	
	<div class="product-block-image-container">
		<c:import url="/components/product-block-image.jspf"></c:import>
	</div>
	<div class="colors">
		<c:import url="/components/color-block.jspf">
			<c:param name="classnames" value="red color-selected" />
		</c:import>
		<c:import url="/components/color-block.jspf">
			<c:param name="classnames" value="yellow" />
		</c:import>
		<c:import url="/components/color-block.jspf">
			<c:param name="classnames" value="orange" />
		</c:import>
	</div>
	<div class="wish-list-heart-block">
		<c:import url="/components/wish-list-heart.jspf"></c:import>
	</div>
	<div class="star-rating-block">
		<c:import url="/components/star-rating.jspf"></c:import>
	</div>
	<div class="product-block-information">
		<c:import url="/components/product-block-information.jspf"></c:import>
	</div>
	<div class="product-block-incentive">
		ships free with purchase of $49 or more!
	</div>
	<div class="promo-block-container">
		<c:import url="/structures/promotion-block.jsp"></c:import>
	</div>
	<div class="product-block-border"></div>
</div>
</dsp:page>