<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="shop-by-character-list">
	<h4>all characters/themes</h4>
	<c:import url="/components/shop-by-tabs.jspf"></c:import>
	<div class="tab-content">
		<c:import url="/components/shop-by-character-options.jspf"></c:import>
		<c:import url="/components/shop-by-character-options-k.jspf"></c:import>
	</div>
</div>
</dsp:page>