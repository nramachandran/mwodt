<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal fade" id="myAccountDeleteCancelModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content sharp-border">
			<c:import url="/components/my-account-delete-cancel-overlay.jspf"></c:import>
        </div>
    </div>
</div>
</dsp:page>