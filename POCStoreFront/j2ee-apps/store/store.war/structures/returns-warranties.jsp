<div class="returns-warranties-struct">
	<c:import url="/components/returns-warranties-breadcrumb.jspf"></c:import>
	<c:import url="/components/returns-warranties-header.jspf"></c:import>
	<hr>
	<c:import url="/components/returns-warranties-content.jspf"></c:import>
</div>