<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="egift-card-block">
    <div class="gift-card-title">
        <div>Geoffrey -- giggles & grins galore!</div>
        <hr />
    </div>
    <div class="block-content">
        <img class="inline" src="/assets/images/gift-card/card-1.jpg" />
        <div class="inline quantity-stepper">
            <div>
                <div class="options-text">
                    Qty:
                </div>
				<c:import url="/components/stepper.jspf">
					<c:param name="classnames" value="red color-selected" />
				</c:import>
            </div>
        </div>
    </div>
</div>
</dsp:page>