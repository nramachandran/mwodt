<div class="modal fade" id="checkoutExitConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content sharp-border">
            <span class="clickable"><img data-dismiss="modal" src="/assets/images/my-account/close.png" /></span>
            <div>
            	<h2>are you sure you want to exit?</h2>
            	<p>your information will not be saved and the availability of your items cannot be guaranteed.</p>
            	<a href="#">no, continue checkout</a>
            	<a href="#">yes, exit checkout</a>
            </div>
        </div>
    </div>	
</div>