<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="compare-overlay">
	<div class="compare-overlay-locked">
		<div class="row">
			<div class="col-md-3">
				<div class="compare-header">
					how do they compare?
				</div>
				<div class="small-compare-text">
					Weigh product features to help decide which item fits you and your little one.
				</div>
			</div>

				<div class="col-md-2">
					<img src="/mwodt-static/assets/images/product-compare-3.jpg" />
				</div>
				<div class="col-md-2">
					<img src="/mwodt-static/assets/images/product-compare-3.jpg" />
				</div>
				<div class="col-md-2">
					<img src="/mwodt-static/assets/images/product-compare-3.jpg" />
				</div>
				<div class="col-md-2">
					<img src="/mwodt-static/assets/images/product-compare-3.jpg" />
				</div>

	        <img class="compare-overlay-close" data-target=".compare-overlay-menu" src="/mwodt-static/assets/images/my-account/close.png"/>
		</div>
		<c:import url="/components/h-divider.jspf"></c:import>		
		<div class="row">
			<div class="col-md-3">
			</div>

			<div class="col-md-2">
				<div class="column-header-text">
					Stokke Crusi Stroller
				</div>
				<div class="add-to-cart-section">
					<button class="add-to-cart">add to cart </button>
				</div>
				<div class="add-to-registry">
					<div class="inline">
					add to registry
					</div>
					<img class="inline" src="/mwodt-static/assets/images/breadcrumb-arrow-right.png" />
				</div>
			</div>
			<div class="col-md-2">
				<div class="column-header-text">
					Stokke Crusi Stroller
				</div>
				<div class="add-to-cart-section">
					<button class="add-to-cart">add to cart </button>
				</div>
				<div class="add-to-registry">
					<div class="inline">
					add to registry
					</div>
					<img class="inline" src="/mwodt-static/assets/images/breadcrumb-arrow-right.png" />
				</div>
			</div>
			<div class="col-md-2">
				<div class="column-header-text">
					Stokke Crusi Stroller
				</div>
				<div class="add-to-cart-section">
					<button class="add-to-cart">add to cart </button>
				</div>
				<div class="add-to-registry">
					<div class="inline">
					add to registry
					</div>
					<img class="inline" src="/mwodt-static/assets/images/breadcrumb-arrow-right.png" />
				</div>
			</div>
			<div class="col-md-2">
				<div class="column-header-text">
					Stokke Crusi Stroller
				</div>
				<div class="add-to-cart-section">
					<button class="add-to-cart">add to cart </button>
				</div>
				<div class="add-to-registry">
					<div class="inline">
					add to registry
					</div>
					<img class="inline" src="/mwodt-static/assets/images/breadcrumb-arrow-right.png" />
				</div>
			</div>

		</div>
		<hr />
	</div>
	<div class="compare-overlay-scroll">
		<div class="row">
			<div class="col-md-3">
				price
			</div>

			<div class="col-md-2">
				$1249.99
			</div>
			<div class="col-md-2">
				$1249.99
			</div>
			<div class="col-md-2">
				$1249.99
			</div>
			<div class="col-md-2">
				$1249.99
			</div>

		</div>
		<hr />
		<div class="row">
			<div class="col-md-3">
				type
			</div>

			<div class="col-md-2">
				full size
			</div>
			<div class="col-md-2">
				full size
			</div>
			<div class="col-md-2">
				full size
			</div>
			<div class="col-md-2">
				full size
			</div>

		</div>
		<hr />
		<div class="row">
			<div class="col-md-3">
				best uses
			</div>

			<div class="col-md-2 best-uses">
				walking, travel, shopping
			</div>
			<div class="col-md-2 best-uses">
				walking, travel, shopping
			</div>
			<div class="col-md-2 best-uses">
				walking, travel, shopping
			</div>
			<div class="col-md-2 best-uses">
				walking, travel, shopping
			</div>			

		</div>
		<hr />
		<div class="row">
			<div class="col-md-3">
				# of children capacity
			</div>

			<div class="col-md-2">
				1
			</div>
			<div class="col-md-2">
				1
			</div>
			<div class="col-md-2">
				1
			</div>
			<div class="col-md-2">
				1
			</div>			

		</div>
		<hr />
		<div class="row">
			<div class="col-md-3">
				car seat compatible
			</div>

			<div class="col-md-2">
				yes
			</div>
			<div class="col-md-2">
				yes
			</div>
			<div class="col-md-2">
				yes
			</div>
			<div class="col-md-2">
				yes
			</div>			

		</div>
		<hr />
		<div class="row">
			<div class="col-md-3">
				weight
			</div>

			<div class="col-md-2">
				19 lbs chassis
				<br />
				8 lbs seat
			</div>
			<div class="col-md-2">
				19 lbs chassis
				<br />
				8 lbs seat
			</div>
			<div class="col-md-2">
				19 lbs chassis
				<br />
				8 lbs seat
			</div>
			<div class="col-md-2">
				19 lbs chassis
				<br />
				8 lbs seat
			</div>			

		</div>
		<hr />
		<div class="row">
			<div class="col-md-3">
				weight capacity
			</div>

			<div class="col-md-2">
				45 lbs
			</div>
			<div class="col-md-2">
				45 lbs
			</div>
			<div class="col-md-2">
				45 lbs
			</div>
			<div class="col-md-2">
				45 lbs
			</div>			

		</div>
		<hr />
		<div class="row">
			<div class="col-md-3">
				extras
			</div>

			<div class="col-md-2 extras">
			    weather protection, reversible handle/seat, adjustable head support, adjustable handle, adjustable canopy
			</div>
			<div class="col-md-2 extras">
			    weather protection, reversible handle/seat, adjustable head support, adjustable handle, adjustable canopy
			</div>
			<div class="col-md-2 extras">
			    weather protection, reversible handle/seat, adjustable head support, adjustable handle, adjustable canopy
			</div>
			<div class="col-md-2 extras">
			    weather protection, reversible handle/seat, adjustable head support, adjustable handle, adjustable canopy
			</div>			

		</div>
	</div>	
</div>
</dsp:page>