<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="shopping-cart-product-information error">
	<div class="product-information-header">
		<div class="shopping-cart-product-title inline">
			<div class="shopping-cart-error-arrow"></div>
		    <a href="#">Disney Frozen Sparkle Anna of Arendalle Doll</a>
			<p class="two-for-two">
				2 for $12 select Koala Baby items<span>&#xB7;</span><a href="#">see terms</a>
			</p>
		</div>
		<div class="shopping-cart-product-price inline">
			$16.99
		</div>
	</div>
	<div class="shopping-cart-product-content">
		<div class="row row-no-padding">
			<div class="col-md-2">
				<div>
					<img class="shopping-cart-product-image" src="/mwodt-static/assets/images/cart/shopping-anna-doll.jpg" />
				</div>
			</div>
			<div class="col-md-5 product-cart-description">
				<div class="shopping-cart-color">Color: Finland</div>
				<div class="shopping-cart-size">Size: M</div>
				<div class="shopping-cart-price">$16.99</div>
				<div class="shopping-cart-item-number">Item#: 019985</div>
				<div class="shopping-cart-stock">
					<div class="green-bullet inline"></div>
					<div class="in-stock inline">in stock</div>
				</div>
				<div class="quantity">
					<div>Qty:</div>
					<c:import url="/components/stepper.jspf"></c:import>
					<span class="cart-error-message">We've adjusted the quantity below to reflect the number we have available.</span>
				</div>
				<div class="shopping-cart-gift">
					<div class="checkbox-sm-off"></div>
					<div class="shopping-cart-gift-image inline"></div>
					<div class="inline">
						this is a gift
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<label for="ship-to-home-error">
					<div class="ship-to-home">
						<input id="ship-to-home-error" class="shipping-radio-btn inline" type="radio" name="shipping-type" checked value="ship-to-home" />
						Ship-To-Home
					</div>
					<div class="padded-details">
						<div class="shipping-details">
							Standard Shipping arrives 1/12/15 - 1/26/15
						</div>
						<div class="layaway-shipping-details">
							Ships after final layaway payment is made
						</div>
					</div>
				</label>
				<br />
				<label for="free-store-pickup-error">
					<div class="free-store-pickup">
						<input id="free-store-pickup-error" class="shipping-radio-btn inline" type="radio" name="shipping-type" value="free-store-pickup" /> Free Store Pickup
					</div>
					<div class="padded-details">
						Toys "R" Us - Columbus [8910]
					</div>
				</label>
			</div>
		</div>
		<div class="row row-no-paddidng shopping-cart-surprise">
			<div class="col-md-6 col-md-offset-2">
				<div>
					<div class="shopping-cart-surprise-image inline"></div>
					<div class="shopping-cart-remove inline">don't spoil the surprise <span>&#xB7;</span><a href="#">learn more</a></div>
				</div>
			</div>
			<div class="col-md-4 text-right">
				<a class="remove-cart-item" href="#">remove</a>
			</div>
		</div>
	</div>
</div>
</dsp:page>