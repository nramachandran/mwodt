<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="gift-finder-menu">
	<c:import url="/components/gift-finder-intro.jspf"></c:import>
    <div class="gift-finder-hero"><a href="">back</a></div>
    <div class="tse-scrollable gift-finder-wrapper">
        <div class='tse-content'>
            
            <div class="gift-finder-container">
                <div>
                    <div class="gift-finder-spacer">
						<c:import url="/components/gift-finder-name-zone.jspf"></c:import>
                    </div>
                    <div class="gift-finder-spacer">
						<c:import url="/components/gift-finder-shopping-for-zone.jspf"></c:import>
                    </div>
					<c:import url="/components/gift-finder-interest-zone.jspf"></c:import>
                </div>
            </div>
        </div>
    </div>
   
    <div class="gift-finder-footer"> <c:import url="/components/gift-finder-button.jspf"></c:import> <button id="clearGiftFinder">clear all</button></div>
</div>

</dsp:page>