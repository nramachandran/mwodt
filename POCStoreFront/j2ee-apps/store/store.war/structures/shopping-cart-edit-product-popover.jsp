<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="shopping-cart-edit-product-content">
	<div class="row">
		<img class="close-modal close-edit" data-dismiss="modal" src="/assets/images/my-account/close.png" alt="close modal" />
		<div class="header">
			<h2>Koala Baby Girls' Short Sleeve Tiered Tunic</h2>
			<c:import url="/components/star-rating.jspf"></c:import>
		</div>
		<hr />
		<div class="col-md-6">
			<img src="/assets/images/cart/koala-385.jpg" alt="" />
		</div>
		<div class="col-md-6 product-information">
			<p><span>$7.99</span></p>
			<div class="color-text">
				<span>Color:</span> Red
			</div>
			<div class="colors">
				<c:import url="/components/color-block.jspf">
					<c:param name="classnames" value="red color-selected" />
				</c:import>
				<c:import url="/components/color-block.jspf">
					<c:param name="classnames" value="yellow" />
				</c:import>
				<c:import url="/components/color-block.jspf">
					<c:param name="classnames" value="orange" />
				</c:import>
			</div>
			<div class="color-text">
				<span>Size:</span> newborn
			</div>
			<div class="sizes">
				<c:import url="/structures/sticky-sizes.jsp"></c:import>
			</div>
			<div class="sticky-quantity">
				<div class="color-text">
					Qty:
				</div>
				<c:import url="/components/stepper.jspf"></c:import>
				<div class="limit-1 inline">
					limit 2 items per customer
				</div>
			</div>
			<button type="submit" class="update">update</button>
			<div class="available-online">
				<div class="green-list-dot inline"></div>
				available online & in store  <span>·</span>
				<div class="small-blue inline">
					<button class="learn-more" data-toggle="modal"
				data-target="#notifyMeModal">learn more</button>
				</div>
			</div>
		</div>
	</div>
</div>
</dsp:page>