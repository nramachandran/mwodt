<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="slider-image-block-container" class="slider-image-block-container">
  <div class="slider-image-block-loading-div" u="loading">
    <div></div>
  </div>

  <div u="slides" class="slides-image-block">
    <div>
      <a class="product-image" href="#">
	      <img class ="product-block-img" src="/assets/images/product-carousel-image-1.png" />
      </a>
      <div class="product-label">
	       Lorem ipsum dolor
      </div>
    </div>
    <div>
      <a class="product-image" href="#">
	<img class ="product-block-img" src="/assets/images/product-carousel-image-2.png" />
      </a>
      <div class="product-label">
	sit amet, consectetur
      </div>
    </div>
    <div>
      <a class="product-image" href="#">
	<img class ="product-block-img" src="/assets/images/product-carousel-image-3.png" />
      </a>
      <div class="product-label">
	adipiscing elit
      </div>
    </div>
    <div>
      <a class="product-image" href="#">
	<img class ="product-block-img" src="/assets/images/product-carousel-image-4.png" />
      </a>
      <div class="product-label">
	Ut enim ad minim
      </div>
    </div>
      <div>
          <a class="product-image" href="#">
              <img class ="product-block-img" src="/assets/images/product-carousel-image-5.png" />
          </a>
          <div class="product-label">
              veniam, quis nostrud exercitation
          </div>
      </div>
      <div>
          <a class="product-image" href="#">
              <img class ="product-block-img" src="/assets/images/product-carousel-image-6.png" />
          </a>
          <div class="product-label">
              veniam, quis nostrud exercitation
          </div>
      </div>
          <div>
      <a class="product-image" href="#">
	<img class ="product-block-img" src="/assets/images/product-carousel-image-1.png" />
      </a>
      <div class="product-label">
	Lorem ipsum dolor
      </div>
    </div>
    <div>
      <a class="product-image" href="#">
	<img class ="product-block-img" src="/assets/images/product-carousel-image-2.png" />
      </a>
      <div class="product-label">
	sit amet, consectetur
      </div>
    </div>
    <div>
      <a class="product-image" href="#">
	<img class ="product-block-img" src="/assets/images/product-carousel-image-3.png" />
      </a>
      <div class="product-label">
	adipiscing elit
      </div>
    </div>
    <div>
      <a class="product-image" href="#">
	<img class ="product-block-img" src="/assets/images/product-carousel-image-4.png" />
      </a>
      <div class="product-label">
	Ut enim ad minim
      </div>
    </div>
      <div>
          <a class="product-image" href="#">
              <img class ="product-block-img" src="/assets/images/product-carousel-image-5.png" />
          </a>
          <div class="product-label">
              veniam, quis nostrud exercitation
          </div>
      </div>
          <div>
      <a class="product-image" href="#">
	<img class ="product-block-img" src="/assets/images/product-carousel-image-6.png" />
      </a>
      <div class="product-label">
	veniam, quis nostrud exercitation
      </div>
    </div>
  </div>
  
  <div u="navigator" id="multi-slide-bullets" class="jssor-bullet">
    <div u="prototype"></div>
  </div>

  
  <span id="multi-carousel-left-arrow" u="arrowleft" class="jssora11l">
      </span>

  <span id="multi-carousel-right-arrow" u="arrowright" class="jssora11r">
  </span>
</div>
</dsp:page>