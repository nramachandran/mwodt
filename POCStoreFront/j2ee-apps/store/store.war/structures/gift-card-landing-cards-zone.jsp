<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="gift-card-landing-cards-zone row">
	<c:import url="/components/gift-card-landing-single-card.jspf">
		<c:param name="imgName" value="standard-card.jpg" />
	</c:import>
	<c:import url="/components/gift-card-landing-single-card.jspf">
		<c:param name="imgName" value="e-card.jpg" />
	</c:import>
	<c:import url="/components/gift-card-landing-single-card.jspf">
		<c:param name="imgName" value="design-your-card.jpg" />
	</c:import>
</div>
</dsp:page>