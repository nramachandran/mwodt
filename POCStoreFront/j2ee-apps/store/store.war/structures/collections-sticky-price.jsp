<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="collections-sticky-price">

	<div class="sticky-price-contents">
<div class="collections-items-selected">0 items selected</div>
		<div class="add-to-cart-section">
			<button class="add-to-cart"   data-toggle="modal" data-target="#shoppingCartModal" disabled>add to cart </button>
		</div>	
		<div class="add-to-button">

				<div class="inline">
				<a href="#" class="small-orange" disabled><img src="toolkit/images/global-nav/wish-list.png" alt="wish list" class="add-to-image">add to wish list</a>
			</div>
			
			<div class="inline">
				<a href="#" class="small-orange" disabled><img src="toolkit/images/global-nav/baby-registry.png" alt="baby registry" class="add-to-image">add to registry</a>
			</div>
		
		
			
		</div>
	</div>
	<c:import url="/components/social-banner.jspf"></c:import>
	<c:import url="/components/warning-modal.jspf"></c:import>
</div>
</dsp:page>