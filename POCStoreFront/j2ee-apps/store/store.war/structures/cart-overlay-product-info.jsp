<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<dsp:getvalueof var="productDisplayName" param="currentItem.auxiliaryData.catalogRef.displayName"/>
<c:if test="${empty productDisplayName}">
  <dsp:getvalueof var="productDisplayName" param="currentItem.auxiliaryData.productRef.displayName"/>
</c:if> 
<dsp:getvalueof var="amount" vartype="java.lang.Double" param="currentItem.priceInfo.amount"/>  
<div id='cart-overlay-product-info'>
	<header><strong><a href="/template-product-details-template.html">${productDisplayName}</a></strong></header>
	<img class='purchase-image' src='/assets/images/train.jpg'>
	<div class='purchase-image-block'></div>
	<div class='cart-info'>
		<div class='description'>part of the Sophia Lolita's Collection</div>
		<div class='price'>
			<strong>
				<dsp:include page="/global/formattedPrice.jsp">
					<dsp:param name="price" value="${amount}"/>
				</dsp:include>
			</strong>
		</div>
		<div class='item_number'>Item # <dsp:valueof param='currentItem.auxiliaryData.productRef.id'/></div>
		<li class='stock_indicator'>in stock</li>
		<div class='stepper_label'>Qty:</div>
			<dsp:include page="/components/stepper.jspf">
				<dsp:param name="currentItemQty" param='currentItem.quantity'/>"> 
			</dsp:include>
		<div class='addon-cta'>
			<input class='compare-checkbox' id='cart-checkbox' type='checkbox'/>
			<label class='compare-checkbox-label' for='cart-checkbox'><strong>Protect it </strong>with a 24 Month Service Plan: $59.99</label>
			<a href="#" class='addon-info'>learn more</a>
		</div>
	</div>
</div>
</dsp:page>