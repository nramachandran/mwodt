﻿<dsp:page>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row collection-product-block">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <p class="deal-text">10% off your coordinating accessories when you buy 1 or more pieces of nursery furniture</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <a href="#"><img class="collection-image" src="/assets/images/collections/collections-product-thumb-1.png" /></a>
                <p><a class="image-gallery-link" href="#/" data-toggle="modal" data-target="#galleryOverlayModal">see more images</a></p>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <h4><a href="#">CoCaLo Sugar Plum Soft and Cozy Blanket</a></h4>
                    </div>
                </div>
                <div class="row">
                   <div class="col-md-12">
						<c:import url="/components/star-rating.jspf"></c:import>
                   </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <p class="price"><span class="strikethrough">$17.99</span> <span class="sale-price">$9.98</span></p>
                        <p><span class="savings">you save $8.01 (45%)</span></p>
                    </div>
                    <div class="col-md-8">
                        <p class="summary-text">The perfect blanket for your baby to snuggle, this CoCaLo Sugar Plum Soft & Cozy Blanket is warm, cozy, and light. Great for playtime or ... <span>·</span> <a href="#">show more</a></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
	                    <div class="options-text">
		                    <span>Color:</span><span class="selected-color"> Red</span>
	                    </div>
	                    <div class="colors">
							<c:import url="/components/color-block.jspf">
								<c:param name="classnames" value="red color-selected" />
							</c:import>
							<c:import url="/components/color-block.jspf">
								<c:param name="classnames" value="yellow" />
							</c:import>
							<c:import url="/components/color-block.jspf">
								<c:param name="classnames" value="orange" />
							</c:import>
	                    </div>
                        <div class="small-spacer"></div>
	                    <div class="options-text">
		                    <span>Size:</span><span class="selected-size"> Small</span>
	                    </div>
                        <div class="sizes">
							<c:import url="/structures/sticky-sizes.jsp"></c:import>
                            <span class="size-chart">
                                <a href="#" data-toggle="modal" data-target="#sizeChartModal">size chart</a>
                            </span>
                        </div>
	                    <div class="sticky-quantity">
		                    <div class="options-text">
			                    Qty:
		                    </div>
							<c:import url="/components/stepper.jspf"></c:import>
	                    </div>
                    </div>
                    <div class="col-md-8 collections-availability">
                        <div class="small-spacer"></div>
                        <p class="summary-text"><img src="/assets/images/collections/green-bullet.png" /> available online & in store <span>·</span> <a class="learn-more-tooltip" href="#">learn more</a></p>
                        <p class="summary-text"><img src="/assets/images/collections/green-bullet.png" />
                             usually leaves warehouse in 1-2 full business
                            <br />
                            <img src="/assets/images/collections/green-bullet.png" class="bullet-spacer"/> days <span>·</span> <a class="details-tooltip" href="#">details</a>
                        </p>
                        <p class="summary-text"><img src="/assets/images/collections/green-location-bullet.png" /> available at 3619 W Dublin-Granville Rd. <span>·</span> <a href="#">find in store</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</dsp:page>