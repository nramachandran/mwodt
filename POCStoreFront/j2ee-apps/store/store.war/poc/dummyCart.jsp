<dsp:page>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/userprofiling/ProfileErrorMessageForEach"/>
<dsp:importbean bean="/mwodt/poc/bean/CartData"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	         
              
              
<html>
<head></head>
<body>

<h1>mwodt Cart Page - DUmmy</h1>
	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<dsp:param name="array" bean="CartData.cartItems"/>
                  <dsp:param name="elementName" value="CommerceItem"/>
                  <dsp:oparam name="output">
                  		<dsp:getvalueof param="CommerceItem.skuName" var="sku"></dsp:getvalueof>
                  		<dsp:getvalueof param="CommerceItem.quantity" var="qty"></dsp:getvalueof>
                  		Name : ${sku }
                  		
                  		Qty : ${qty}
                  		<br/>
                   </dsp:oparam>               
	</dsp:droplet>
   
   
   <dsp:valueof bean="CartData.cartItemsString"></dsp:valueof>
</body>
</html>
</dsp:page>