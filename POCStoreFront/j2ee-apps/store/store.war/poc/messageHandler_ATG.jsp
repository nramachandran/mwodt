<dsp:page>
<html>
<head>
<%--
	THIS JSP CONTAINS A SET OF AJAX OPERATIONS USING ATG REST. CURRENTLY TWO CUSTOM REST APIS AND 2 ATG OOTB REST APIS HAVE BEEN PROVIED. THIS JSP CAN BE USED AS A TEMPLATE FOR DFINING OTHER OPERATIONS WHATEVER EXPOSED BY ATG.
	

 --%>
 <%-- 
 messageHandlerScript_ATG.jspf INCLUDED WHICH EXPOSES METHODS TO SEND/RECIEVE DATA FROM /TO PARENT 
  --%>
<jsp:include page="messageHandlerScript_ATG.jspf"></jsp:include>
<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript">

/**
 * 
 Define Rest urls for each operation
 */
 
 var REST_URL_UPDATE_CART= "/rest/bean/mwodt/poc/bean/CartData/savecartDataList";
 var REST_URL_GET_CART="/rest/bean/mwodt/poc/bean/CartData/retreiveCartData";
 var REST_URL_UPDATE_CART_OOTB= "/rest/model/atg/commerce/order/purchase/CartModifierActor/addItemToOrder?";
 var REST_URL_GET_CART_OOTB="/rest/model/atg/commerce/ShoppingCartActor/detailed";
 
/**
 * Handle possible messages that may come from parent window. This works based on a set of predefined operations (message.mode) set from
 parent window. This page needs to provide implementations for each mode (ie each operations that the parent window requests)
 */
function processMessage(message){
	
	if(message.mode == "update-cart"){
		processMessage_CartUpdate(message.payload);
	}else if(message.mode == "get-cart"){
		processMessage_CartGet(message.callback);
	}else if(message.mode == "get-cart-ootb"){
		processMessage_CartGet_ootb(message.callback);
	}else if(message.mode == "update-cart-ootb"){
		processMessage_CartUpdate_ootb(message.payload);
	}
}

/** ATG Custom API Service calls  
 *  These functions invokes ATG Rest  dummy functions operating on ome session scoped beans. Not the actual ATG cart
 */
function processMessage_CartUpdate(message){
	//alert(message);
	///alert(JSON.stringify(message));

		var url = REST_URL_UPDATE_CART;
		var data = JSON.stringify(message);
		var settings = {url:url,data:data,type:'POST',async:false,contentType:"application/json",success:
   			function(response) 
   			{
   				//alert(response);
   			}
   		};
		jQuery.ajax(
				settings
		);	
	
}

function processMessage_CartGet(callback_mode){
	
		var url = REST_URL_GET_CART;
	
		var settings = {url:url,data:null,type:'POST',async:false,success:
   			function(response) 
   			{
   				var responseText = response.atgResponse;
   				var message = {};
   				message.payload= responseText;
   				message.mode=callback_mode;
   				sendOutgoingMessage(top,"http://registry.mwodt.com:9011",message);
   			}
   		};
		jQuery.ajax(
				settings
		);	
	
}

/**** ATG ootb API calls ***
 *  These functions call ATG rest services that operate on actual cart
 */
 
 function processMessage_CartUpdate_ootb(message){
	
	
		var sessionConfUrlPart= "_dynSessConf="+sesion_conf_number;
		var url = REST_URL_UPDATE_CART_OOTB+sessionConfUrlPart;
		var data =JSON.stringify(message);
		var settings = {url:url,data:data,type:'POST',contentType:"application/json",async:false,success:
   			function(response) 
   			{
   				//alert(response);
   			}
   		};
		jQuery.ajax(
				settings
		);	
	
}
/**
 * OOTB get cart operation
 */
 function processMessage_CartGet_ootb(callback_mode){
					var url = REST_URL_GET_CART_OOTB;
			//var data = JSON.stringify(message);
			var settings = {url:url,data:null,type:'GET',async:false,success:
	   			function(response) 
	   			{
				//** once the cart data is recieved, pass the cart data to parent window (bru) 
	   				var responseText = response;
	   				var message = {};
	   				message.payload= responseText;
	   				/// Pass the response recieved back to parent
	   				message.mode=callback_mode;
	   				sendOutgoingMessage(top,"http://registry.mwodt.com:9011",message);
	   			}
	   		};
			jQuery.ajax(
					settings
			);	
		
	}
 
 var sesion_conf_number=
"<dsp:valueof bean="/OriginatingRequest.sessionConfirmationNumber"></dsp:valueof>";
  
</script>
</head>
<body>
This JSP is hosted in an iframe. This jsp passes / recieves data to/from parent window 
Currently this jsp retrieves data from server using ATG rest requests. 
This jsp acts as a container for the same - domain Rest calls and passes the data to parent window which is in a different domain
</body>
</html>
</dsp:page>