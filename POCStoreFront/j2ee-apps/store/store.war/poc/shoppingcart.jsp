<dsp:page>

<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/atg/commerce/gifts/GiftShippingGroups"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/droplet/ComponentExists"/>
<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:setvalue bean="Profile.currentLocation" value="shopping_cart"/>

<HTML>
<HEAD>
<TITLE>Shopping Cart</TITLE>
</HEAD>

<BODY>
                    
<h3>My Shopping Cart</h3>
Site: <dsp:valueof bean="ShoppingCart.current.siteId"/><br>
Creation site: <dsp:valueof bean="ShoppingCart.current.creationSiteId"/><br>

<p>

<dsp:droplet name="/atg/dynamo/droplet/Switch">
<dsp:param bean="CartModifierFormHandler.formError" name="value"/>
<dsp:oparam name="true">
  <font color=cc0000><STRONG><UL>
    <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
      <dsp:param bean="CartModifierFormHandler.formExceptions" name="exceptions"/>
      <dsp:oparam name="output">
	<LI> <dsp:valueof param="message"/>
      </dsp:oparam>
    </dsp:droplet>
    </UL></STRONG></font>
</dsp:oparam>
</dsp:droplet>

<p>

<dsp:form action="shoppingcart.jsp" method="post">

<dsp:input bean="CartModifierFormHandler.moveToPurchaseInfoByRelIdSuccessURL" type="hidden" value="shipping.jsp?init=true"/>
<dsp:input bean="CartModifierFormHandler.sessionExpirationURL" type="hidden" value="session_expired.jsp"/>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
<tr valign=top><td><table cellspacing=2 cellpadding=0 border=0>
<tr>
<td><b>Delete</b></td>
<td><b>Quantity</b></td>
<td></td>
<td>&nbsp;&nbsp;</td>
<td><b>Product</b></td>
<td>&nbsp;&nbsp;</td>
<td><b>SKU</b></td>
<td>&nbsp;&nbsp;</td>
<td><b>Site</b></td>
<td>&nbsp;&nbsp;</td>
<td align=right><b>List Price</b></td>
<td>&nbsp;&nbsp;</td>
<td align=right><b>Sale Price</b></td>
<td>&nbsp;&nbsp;</td>
<td align=right><b>Total Price</b></td>
</tr>

<tr><td colspan=15><hr size=0></td></tr>

<dsp:droplet name="ForEach">
	<dsp:param bean="CartModifierFormHandler.order.ShippingGroups" name="array"/>
	<dsp:param name="elementName" value="ShippingGroup"/>
	<dsp:param name="indexName" value="shippingGroupIndex"/>

	<dsp:oparam name="output">   			
	<dsp:droplet name="ForEach">
		<dsp:param name="array" param="ShippingGroup.CommerceItemRelationships"/>
		<dsp:param name="elementName" value="CiRelationship"/>
		<dsp:param name="indexName" value="index"/>
		<dsp:oparam name="output">		
			<tr valign=top>
			<td>
				<dsp:input bean="CartModifierFormHandler.removalRelationshipIds" paramvalue="CiRelationship.Id" type="checkbox" checked="<%=false%>"/>
			</td>
			<td>
				<input name='<dsp:valueof param="CiRelationship.Id"/>' size="4" value='<dsp:valueof param="CiRelationship.quantity"/>'>
			</td>
			<td></td>
			<td></td>
			<td>
				<dsp:getvalueof id="pval0" param="CiRelationship.commerceItem.auxiliaryData.productRef"><dsp:include page="product_fragment.jsp"><dsp:param name="childProduct" value="<%=pval0%>"/></dsp:include></dsp:getvalueof>
			</td>
			<td>&nbsp;&nbsp;</td>
			<td>
				<dsp:valueof param="CiRelationship.commerceItem.auxiliaryData.catalogRef.displayName"/>
			</td>
			<td>&nbsp;&nbsp;</td>
      <td>
        <dsp:valueof param="CiRelationship.commerceItem.auxiliaryData.siteId"/>
      </td>
      <td>&nbsp;&nbsp;</td>
			<td align=right>
				<dsp:valueof converter="currency" param="CiRelationship.commerceItem.priceInfo.listPrice">no price</dsp:valueof>
			</td>
			<td>&nbsp;&nbsp;</td>
			<td align=right>
				<dsp:droplet name="Switch">
					<dsp:param name="value" param="CiRelationship.commerceItem.priceInfo.onSale"/>
					<dsp:oparam name="true"><dsp:valueof converter="currency" param="CiRelationship.commerceItem.priceInfo.salePrice"/></dsp:oparam>
				</dsp:droplet>
			</td>
			<td>&nbsp;&nbsp;</td>
			<td align=right>
				<dsp:valueof converter="currency" param="CiRelationship.commerceItem.priceInfo.amount">no price</dsp:valueof>
			</td>
			</tr>
		</dsp:oparam>
		<dsp:oparam name="empty">
			<tr valign=top>
			<td>No Items</td>
			</tr>
		</dsp:oparam>
	</dsp:droplet>				
	</dsp:oparam>
</dsp:droplet>

<tr><td colspan=15><hr size=0></td></tr>
<tr>
<td colspan=4 align=right>Subtotal</td>
<td>
<dsp:valueof bean="ShoppingCart.current.priceInfo.amount" converter="currency">no price</dsp:valueof>
</td>
<td align=right></td>
</tr>

<tr>
<td colspan=2 align=right></td>
<td></td>
<td align=right>Shipping</td>
<td>
<dsp:valueof bean="ShoppingCart.current.priceInfo.shipping" converter="currency">no price</dsp:valueof>
</td>
<td align=right></td>
</tr>

<tr>
<td colspan=4 align=right>Tax</td>
<td>
<dsp:valueof bean="ShoppingCart.current.priceInfo.tax" converter="currency">no price</dsp:valueof>
</td>
<td align=right></td>
</tr>
        
<tr>
<td colspan=4 align=right>Total</td>
<td>
<dsp:valueof bean="ShoppingCart.current.priceInfo.total" converter="currency">no price</dsp:valueof>
</td>
<td align=right><b></b></td>
</tr>
</table>

<tr>
<td>
<dsp:input bean="CartModifierFormHandler.setOrderByRelationshipIdErrorURL" type="hidden" value="shoppingcart.jsp"/>
<dsp:input bean="CartModifierFormHandler.moveToPurchaseInfoByRelIdErrorURL" type="hidden" value="shoppingcart.jsp"/>
<dsp:input bean="CartModifierFormHandler.setOrderByRelationshipId" type="submit" value="Recalculate"/> &nbsp; &nbsp;
<dsp:input bean="CartModifierFormHandler.moveToPurchaseInfoByRelId" type="submit" value="Checkout"/>
</td>
</tr>

</table>

</dsp:form>

</BODY>
</HTML>

</dsp:page>
