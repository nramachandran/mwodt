<dsp:page>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/userprofiling/ProfileErrorMessageForEach"/>
<dsp:importbean bean="/mwodt/poc/TruSessionInfoDroplet"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
THIS PAGE will be loaded when BRU is loaded first. BRU page will have an iframe that points to this page

--%>
<html>
<head></head>
<body>
	<dsp:droplet name="TruSessionInfoDroplet">
		<dsp:oparam name="output">			
			<dsp:getvalueof var ="jsesionId" param="encrval"/>
		</dsp:oparam>
	</dsp:droplet>	
	<%--
		Use an Iframe to pass the key to BRU. This can be done using an ajax in the parent window. 
		Pass the key to parent window using window.postMessage script then pass to server using an ajax
	 --%>
	<iframe src="http://baby.bru.com:9011/babyweb/handshake.jsp?key=${jsesionId}"></iframe>

</body>
</html>
</dsp:page>