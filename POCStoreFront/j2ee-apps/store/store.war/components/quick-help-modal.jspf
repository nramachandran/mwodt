<div class="modal fade" id="quickHelpModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog quick-help-modal">
        <div class="modal-content sharp-border">
	        <div class="modal-title">
	        	emails about your order
	        	<img data-dismiss="modal" src="/mwodt-static/assets/images/my-account/close.png" />
	        </div>
	        <div class="tse-scrollable">
		        <div class="tse-scroll-content tse-quickhelp-scroll-content">
		        	<div class="quick-help-modal-content tse-content">
				        <div class="modal-static-content">
				        	<header>After you place your order, you will receive emails about your order status. Below are some examples of emails you might receive:
				        	</header>
				        	<p>
							<span>Order Acknowledgment.</span> This email acknowledges that we have received your order, which includes Store Pickup items. Keep this email for your records and refer to it for next steps.
							</p>
							<p>
							<span>Order Confirmation.</span> This email confirms that we have received your order, and includes your order number. We recommend that you keep this email for your records. Order Confirmation will be sent for 'Ship It' items only. For Pay in Store orders, your order confirmation will contain a barcode and receipt of this will be your indication that you should go a Toys”R”Us or Babies”R”Us store to pay for your order.
							</p>
							<p>
							<span>Payment Acknowledgement.</span> This email confirms receipt of your Pay in Store payment and release of your order for processing and shipment.
							</p>
							<p>
							<span>You Have Cancelled Your Order.</span> This email confirms that your order has been cancelled at your request. Keep this email for your records.
							</p>
							<p>
							<span>Order Cancelled.</span> Store Pickup orders will automatically be cancelled if not picked up within the designated timeframe shown in the 'Ready for Store Pickup' email. If you are unable to meet the timeframe, the order will be cancelled and you will receive the Order Cancelled email.
							</p>
							<p>
							<span>Unable to Process Your Cancel Request.</span> This email is sent when we are unable to process your cancel request. 
							This email will be sent for 'Ship It' items only.
							</p>
							<p>
							<span>Distribution Center Confirmation.</span> This email confirms inventory is available and item is being shipped from our warehouse to your selected Ship to Store pickup location. This is not an indication that you should go to the store to pick up your order, it is simply letting you know that your order is on its way to your selected store.
							</p>
							<p>
							<span>Shipment Confirmation.</span> This email confirms that your order, or part of your order, has been shipped. If any items in your order were shipped separately, you will receive an email confirming the shipment of each item. The arrival time of your order depends on the shipping method selected, the item itself, and the destination address specified for the item.
							</p>
							<p>
							<span>Ready for Store Pickup.</span> This email confirms that your order, or part of your order is ready for pick up at your desired store location(s). You will receive one 'Ready for Store Pickup' email per store pickup location selected on your order.
							</p>
							<p>
							<span>Important Notice About Your Order.</span> There are a few reasons why you would receive this email, including: difficulty in processing your order, inability to ship your item(s) to the address provided, or cancellation of order. Should you receive a notice like this, please call our Customer Service Team at 1-800-TOYSRUS immediately for assistance.
							</p>
							<p>
							<span>Store Pickup Reminder.</span> This email is sent as a reminder that your order is waiting for you at the store and it will be cancelled if not picked up within the designated timeframe.
							</p>
							<p>
							<span>Store Pickup Order Completed.</span> This email is an indicator that you have picked up your order at the store.
							</p>
				        </div>
			        </div>
			    </div>
		    </div>
	    </div>
	</div>
</div>