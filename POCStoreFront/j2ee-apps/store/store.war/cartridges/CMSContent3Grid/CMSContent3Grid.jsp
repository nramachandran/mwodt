<dsp:page>
	<div class="container-fluid home-template min-width">
		<div class="default-margin">
			<div class="max-width">
				<div class="row-center">
					<div class="col-md-4 block-3-column">
						<c:import url="/components/content-block-3.jspf"></c:import>
					</div>
					<div class="col-md-4 block-3-column text-center">
						<c:import url="/components/content-block-3.jspf"></c:import>
					</div>
					<div class="col-md-4 block-3-column text-right">
						<c:import url="/components/content-block-3.jspf"></c:import>
					</div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>