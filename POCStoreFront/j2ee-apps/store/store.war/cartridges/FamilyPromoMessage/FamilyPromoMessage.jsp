<%--

  This renderer calls the renderContentItem for it's contents.
    
  Required Parameters:
    contentItem
      The page slot content item to render.
   
  Optional Parameters:

--%>
<dsp:page>
<br> 
   <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
  <dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/> 
    ${contentItem.content}
</dsp:page>
