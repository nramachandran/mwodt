<div class="row default-margin">
        <div class="col-mid-12 col-no-padding">
            <h2>her universe</h2>
        </div>
    </div>
    <div id="narrow-by-scroll" class="product-content">
         <div class=" fixed-narrow-menu">
            <div class="row default-margin">
                <div class="col-md-6 col-no-padding col-narrow-by">
					<c:import url="/components/narrow-by-button.jspf"></c:import>
					<c:import url="/components/showing-results.jspf"></c:import>
                </div>
                <div class="col-md-6 text-right col-narrow-by">    
					<c:import url="/components/sort-by.jspf"></c:import>
                </div>
            </div>
            <div class="row default-margin">
                <div class="col-md-12 col-no-padding">
					<c:import url="/components/narrow-by-filter-list.jspf"></c:import>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12"><hr class="thick-hr"/></div>
            </div>

            <div class="offcanvas compare-menu sub-cat-container">
				<c:import url="/components/sub-category-filter-window.jspf"></c:import>
            </div>
            <div class="offcanvas compare-overlay-menu sub-cat-container">
				<c:import url="/structures/compare-overlay.jsp"></c:import>
            </div>
             <div class="compare-product-selection">
            <div class="row">
                <div class="col-md-3">
                    <div class="compare-header">
                        how do they compare?
                    </div>
                    <div class="small-compare-text">
                        Weigh product features to help decide which item fits you and your little one.
                    </div>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-10-percent no-compare-image image-1">
                    <img src="/assets/images/product-compare-3.jpg" />
                </div>
                <div class="col-md-10-percent no-compare-image image-2">
                    <img src="/assets/images/product-compare-3.jpg" />
                </div>
                <div class="col-md-10-percent no-compare-image image-3">
                    <img src="/assets/images/product-compare-3.jpg" />
                </div>
                <div class="col-md-10-percent no-compare-image image-4">
                    <img src="/assets/images/product-compare-3.jpg" />
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-2">
                    <button id="compare-products-button" disabled>compare now</button>
                </div>
            </div>
        </div>
        </div>
          <div class="sticky-menu-placeholder">
        </div>

    <div id="filtered-products">
        <div class="fade-to-black" id="product-disabled-modal"></div>
        <div class="row-footer-margin">
            <div class="row row-no-padding filtered-products-padding-top">
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="new-flag-mwodt" />
					</c:import>
				</div>
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="top-seller-flag-mwodt" />
					</c:import>
				</div>
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="exclusive-flag-mwodt" />
					</c:import>
				</div>
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="" />
					</c:import>
				</div>
            </div>

            <div class="row row-no-padding filtered-products-padding">
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="no-select" />
					</c:import>
				</div>
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="no-select" />
					</c:import>
				</div>
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="no-select" />
					</c:import>
				</div>
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="no-select" />
					</c:import>
				</div>
            </div>
            <div class="row row-no-padding filtered-products-padding">
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="no-select" />
					</c:import>
				</div>
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="no-select" />
					</c:import>
				</div>
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="no-select" />
					</c:import>
				</div>
                <div class="col-md-3 col-no-padding product-block-padding">
					<c:import url="/structures/product-block.jsp">
						<c:param name="classnames" value="no-select" />
					</c:import>
				</div>
            </div>
	
            <div class="load-more-products">
                <div class="row row-no-padding filtered-products-padding">
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                </div>
                <div class="row row-no-padding filtered-products-padding">
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                </div>
                <div class="row row-no-padding filtered-products-padding">
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                </div>
                <div class="row row-no-padding filtered-products-padding">
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                </div>
                <div class="row row-no-padding filtered-products-padding">
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                </div>
                <div class="row row-no-padding filtered-products-padding">
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                </div>
                <div class="row row-no-padding filtered-products-padding">
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                </div>
                <div class="row row-no-padding filtered-products-padding">
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                    <div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
                </div>			
            </div>
			<c:import url="/components/family-show-more.jspf"></c:import>
        </div>
    </div>
</div>    
    