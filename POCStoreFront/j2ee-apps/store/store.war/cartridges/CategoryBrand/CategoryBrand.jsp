<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<dsp:page>
<%-- Below div is taken from CategorySEOAdSpotBlock.jsp and will be repeating in CategoryBrand.jsp 
	This adjustment is been made to remove the wrapper cartridge CategorySEOAdSpotBlock
	This needs to be watched out for any alignment issue later. As of now it is working fine.
--%>
<div class="row-max-width">
	<div class="brand-block-row">
		<c:import url="/components/category-brand-block.jspf"></c:import>
	</div>
</div>
</dsp:page>