    <div class="row row-no-padding">
        <div class="col-md-12 col-no-padding">
        	<c:if test='${vCallingPage eq "HomePage" || vCallingPage eq "CategoryPage"}'>
            	<div class="fixed-nav">
            </c:if>
				<c:import url="/structures/global-nav.jsp"></c:import>
				<c:import url="/structures/explore-modal.jsp"></c:import>
            <c:if test='${vCallingPage eq "HomePage" || vCallingPage eq "CategoryPage"}'>
            	</div>
            </c:if>
        </div>
    </div>