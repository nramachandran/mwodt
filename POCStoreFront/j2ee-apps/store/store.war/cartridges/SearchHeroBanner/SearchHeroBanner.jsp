<dsp:page>
	<div class="lego-background">
			<div class="row-footer-margin">
				<div class="row">
					<div class="col-md-1">
						<div class="lego-logo"></div>
					</div>
					<div class="col-md-6">
						<div class="brand-header">
							<a href="#">lego shop</a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="slider-image-block-container-brand"
							class="slider-image-block-container-brand">
							<div class="slider-image-block-loading-div" u="loading">
								<div></div>
							</div>

							<div id="slides-image-block" u="slides"
								class="slides-image-block">
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-1.jpg" />
									</a>
									<div class="product-label-brand ">the lego movie</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-2.jpg" />
									</a>
									<div class="product-label-brand ">duplo</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-3.jpg" />
									</a>
									<div class="product-label-brand ">city</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-4.jpg" /> 1
									</a>
									<div class="product-label-brand ">marvel super heroes</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-5.jpg" />
									</a>
									<div class="product-label-brand ">teenage mutant ninja
										turtles</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-1.jpg" />
									</a>
									<div class="product-label-brand ">the lego movie</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-2.jpg" />
									</a>
									<div class="product-label-brand ">duplo</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-3.jpg" />
									</a>
									<div class="product-label-brand ">city</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-4.jpg" />
									</a>
									<div class="product-label-brand ">marvel super heroes</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-5.jpg" />
									</a>
									<div class="product-label-brand ">teenage mutant ninja
										turtles</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-1.jpg" />
									</a>
									<div class="product-label-brand ">the lego movie</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-2.jpg" />
									</a>
									<div class="product-label-brand ">duplo</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-3.jpg" />
									</a>
									<div class="product-label-brand ">city</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-4.jpg" />
									</a>
									<div class="product-label-brand ">marvel super heroes</div>
								</div>
								<div class="hover-hover">
									<a class="product-image" href="#"> <img
										class="product-block-img"
										src="/mwodt-static/assets/images/Brand-Shop-5.jpg" />
									</a>
									<div class="product-label-brand ">teenage mutant ninja
										turtles</div>
								</div>
							</div>

							<div u="navigator" id="multi-slide-bullets" class="jssor-bullet">
								<div u="prototype"></div>
							</div>


							<span id="multi-carousel-left-arrow" u="arrowleft"
								class="jssora11l"> </span> <span id="multi-carousel-right-arrow"
								u="arrowright" class="jssora11r"> </span>
						</div>
					</div>
				</div>
			</div>
		</div>
</dsp:page>