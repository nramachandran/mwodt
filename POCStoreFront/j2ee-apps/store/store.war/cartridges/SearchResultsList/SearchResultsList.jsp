

<dsp:page>
   <!-- Home Page main div tag -->
  <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
  <dsp:include page="/global/jspf/header-reference.jspf"></dsp:include>
	<c:set var="vCallingPage" value="HomePage" scope="request"></c:set>
  <dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/> 
 <div class="container-fluid default-template search-template">
        <c:if test="${not empty content.HeaderContent}">
          <c:forEach var="element" items="${content.HeaderContent}">
            <dsp:renderContentItem contentItem="${element}"/>
          </c:forEach>
        </c:if>
     
        <%-- Render the main content  --%>
        <div class="container-main-content">
          <c:forEach var="element" items="${content.MainContent}">
             <dsp:renderContentItem contentItem="${element}"/>
           </c:forEach>
         </div>
           <c:if test="${not empty content.FooterContent}">
             <c:forEach var="element" items="${content.FooterContent}">
             <dsp:renderContentItem contentItem="${element}"/>
           </c:forEach>
           </c:if>
           <dsp:include page="/global/jspf/footer-reference.jspf"></dsp:include>
  </div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/11.1/Storefront/j2ee/store.war/cartridges/OneColumnPage/OneColumnPage.jsp#2 $$Change: 883241 $--%>


