
<dsp:page>
	<dsp:getvalueof var="contextPath" vartype="java.lang.String" bean="/OriginatingRequest.contextPath" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
	<div id="filter-accordion" class="filter-window-container">
		<h3 class="filter-window-header">
			<div class="filter-window-header-text">
				<c:set var="refinementHeader" value="${contentItem.name}"></c:set>
				<c:out value="${refinementHeader}" />
			</div>
		</h3>
		<div>
			<c:forEach var="refinement" items="${contentItem.refinements}" varStatus="status">
				<div>
					<c:set var="iterID" value="${refinement.label}-${status.index}" />
					<%-- <dsp:a href="${contextPath}${refinement.contentPath}${refinement.navigationState}">click here</dsp:a> --%>
					<dsp:form id="navForm-${iterID}" formid="navForm-${iterID}" method="post" action="${contextPath}${refinement.contentPath}${refinement.navigationState}">
						<input type="checkbox" class="sub-category-filter-option-checkbox" id="${iterID}">
						<label for="${iterID}">${refinement.label} (${refinement.count})</label>
					</dsp:form>
				</div>
			</c:forEach>
		</div>
	</div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/11.1/Storefront/j2ee/store.war/cartridges/RefinementMenu/RefinementMenu.jsp#2 $$Change: 884492 $--%>
