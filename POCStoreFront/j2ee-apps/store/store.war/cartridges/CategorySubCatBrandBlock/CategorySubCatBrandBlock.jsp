<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/> 
	      <%-- Render the main content --%>
    <div class="row-max-width">
	      <c:if test="${not empty content.MainContent}">
	        <c:forEach var="element" items="${content.MainContent}">
	           <dsp:renderContentItem contentItem="${element}"/>
	         </c:forEach>
	      </c:if>
    </div>	
</dsp:page>