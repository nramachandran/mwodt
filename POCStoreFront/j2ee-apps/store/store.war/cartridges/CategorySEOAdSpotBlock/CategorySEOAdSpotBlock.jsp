<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/> 
    <%-- Render the main content --%>
    <div class="default-margin">
        <div class="row category-ad-row">
	      <c:if test="${not empty content.SecondaryContent}">
	        <c:forEach var="element" items="${content.SecondaryContent}">
	           <dsp:renderContentItem contentItem="${element}"/>
	         </c:forEach>
	      </c:if>
        </div>
        <hr class="bottom-hr">
    </div>
</dsp:page>