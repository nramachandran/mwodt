<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<%-- Include common asset header.--%>
	<dsp:include page="/global/jspf/header-referenceNoMap.jspf"></dsp:include>

	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/> 
    <div class="sub-category-template default-template container-fluid">

		<%-- Render the header content --%>
	      <c:if test="${not empty content.HeaderContent}">
	        <c:forEach var="element" items="${content.HeaderContent}">
	          <dsp:renderContentItem contentItem="${element}"/>
	        </c:forEach>
	      </c:if>
	      <%-- Render the main content --%>
	
	      <c:if test="${not empty content.SecondaryContent}">
	        <c:forEach var="element" items="${content.SecondaryContent}">
	           <dsp:renderContentItem contentItem="${element}"/>
	         </c:forEach>
	      </c:if>
	    
	      <%-- Render the main content --%>
	      <c:if test="${not empty content.MainContent}">
	        <c:forEach var="element" items="${content.MainContent}">
	           <dsp:renderContentItem contentItem="${element}"/>
	         </c:forEach>
	      </c:if>
	     <c:if test="${not empty content.FooterContent}">
	     <div class="footerMarginClear">
             <c:forEach var="element" items="${content.FooterContent}">
             <dsp:renderContentItem contentItem="${element}"/>
           </c:forEach>
           </div>
           </c:if>
	</div>
	<%-- Include common asset footer.--%>
	<dsp:include page="/global/jspf/footer-reference.jspf"></dsp:include>
</dsp:page>
