<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/commerce/catalog/ProductLookup" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:getvalueof var="siteBaseUrl" bean="/atg/multisite/Site.productionUrl" />
	<dsp:getvalueof var="locale" bean="/atg/dynamo/servlet/RequestLocale.localeString" />
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
	<dsp:getvalueof var="contextPath" vartype="java.lang.String" bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="originatingRequestURL" bean="/OriginatingRequest.requestURI" />
	<dsp:getvalueof var="originatingRequest" bean="/OriginatingRequest" />

	<c:if test="${contentItem.totalNumRecs  >0}">
	
	
	<%-- Render the product list. --%>
	<fmt:parseNumber var="size" type="number" value="${contentItem.totalNumRecs}" />
	<fmt:parseNumber var="recsPerPage" type="number" value="${contentItem.recsPerPage}" />
	<fmt:parseNumber var="start" type="number" value="${contentItem.firstRecNum}" />
	<c:set var="question">
		<dsp:valueof param="Ntt" valueishtml="true" />
	</c:set>
	<div id="filtered-products">
		<div class="fade-to-black" id="product-disabled-modal"></div>
		<div class="row-footer-margin">
			<div class="row row-no-padding filtered-products-padding">
				<c:forEach var="record" items="${contentItem.records}" varStatus="loopStatus">
					<dsp:getvalueof var="productId" value="${record.attributes['product.repositoryId']}" />
					Product ${productId }
					<dsp:droplet name="ProductLookup">
						<dsp:param name="id" value="${productId}" />
						
						<dsp:oparam name="output">
							<div class="col-md-3 col-no-padding product-block-padding">
								<dsp:include page="/structures/product-block.jsp">
									<dsp:param name="classnames" value="no-select" />
									<dsp:param name="product" param="element" />
								</dsp:include>
							</div>
						</dsp:oparam>
						<dsp:oparam name="empty">
						No products found
						</dsp:oparam>
					</dsp:droplet>
				</c:forEach>
			</div>
			<%-- Load More Products Section --%>
			<div class="load-more-products">
				<div class="row row-no-padding filtered-products-padding">
					<div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
					<div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
					<div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
					<div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
				</div>
				<div class="row row-no-padding filtered-products-padding">
					<div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
					<div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
					<div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
					<div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
				</div>
				<div class="row row-no-padding filtered-products-padding">
					<div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
					<div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
					<div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
					<div class="col-md-3 col-no-padding product-block-padding">
						<c:import url="/structures/product-block.jsp">
							<c:param name="classnames" value="no-select" />
						</c:import>
					</div>
				</div>
			</div>
			<c:import url="/components/family-show-more.jspf"></c:import>
		</div>
	</div>
</c:if>
</dsp:page>