<%--
  ~ Copyright 2001, 2012, Oracle and/or its affiliates. All rights reserved.
  ~ Oracle and Java are registered trademarks of Oracle and/or its
  ~ affiliates. Other names may be trademarks of their respective owners.
  ~ UNIX is a registered trademark of The Open Group.

 
  This page lays out the elements that make up a two column page.
    
  Required Parameters:
    contentItem
      The two column page content item to render.
   
  Optional Parameters:

  NOTES:
    The "rootContentItem" request-scoped variable (request attribute), which is used here,
      is set in the "AssemblerPipelineServlet" Nucleus component.
--%>
<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
	<div id="narrow-by-scroll" class="product-content">
		<c:forEach var="element" items="${contentItem.SecondaryContent}">
			<dsp:renderContentItem contentItem="${element}" />
		</c:forEach>
		<%-- Render the main content --%>
		<c:forEach var="element" items="${contentItem.MainContent}">
			<dsp:renderContentItem contentItem="${element}" />
		</c:forEach>
	</div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/11.1/Storefront/j2ee/store.war/cartridges/TwoColumnPage/TwoColumnPage.jsp#2 $$Change: 883241 $--%>