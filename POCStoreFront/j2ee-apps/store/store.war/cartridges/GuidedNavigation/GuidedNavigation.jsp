<%-- ~ Copyright 2001, 2012, Oracle and/or its affiliates. All rights reserved. ~ Oracle and Java are registered trademarks of Oracle and/or its ~ affiliates. Other names may be trademarks of their respective owners. ~ UNIX is a registered trademark
of The Open Group. This page lays out the elements that make up the search results page. Required Parameters: contentItem The guided navigation content item to render. Optional Parameters: --%>
<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
	
	<div class="row default-margin">
		<div class="col-mid-12 col-no-padding">
			<h2>her universe</h2>
		</div>
	</div>
		
	<div class=" fixed-narrow-menu">
		<!-- this div will be closed inside RefinementMenuFragment.jspf-->
		<div class="row narrow-by-row">
			 
			<div class="col-md-6 col-no-padding col-narrow-by">
				<!-- narrow by button   added inline style  for temporary adjustment. When the style issues are resolved on XM templating, this can be removed-->
				<c:if test="${not empty contentItem.navigation}">  
				<div class="narrow-by-container" style="margin-left:5em;">
					<button class="narrow-by-button compare-toggle" id="sub-category-narrow-by-button" type="button" data-toggle="offcanvas" data-target=".compare-menu" />
					<label class="narrow-by-button-label" for="sub-category-narrow-by-button"><span class="expanded"></span><span class="minimized"></span>narrow by</label>
				</div>
				
				<!-- end narrow by button -->
				<!-- showing-results.html -->
				<span class="showing-results">Showing 2 of 2 results</span>
				<!-- end showing-results.html -->
				</c:if> 
				
			</div>
			 
			<div class="col-md-6 text-right col-narrow-by">
				<!-- sort by -->
				<div class="btn-group sort-by">
					<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<span class="sort-by-title">sort by</span> <span id="sort-by-arrow" class="sort-by-down-arrow"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<img class="sort-by-dropdown-arrow" src="/mwodt-static/assets/images/arrow-grey.svg">
						<li><a href="#">best selling</a></li>
						<li><a href="#">price: low to high</a></li>
						<li><a href="#">price: high to low</a></li>
						<li><a href="#">top rated</a></li>
						<li><a href="#">recently added</a></li>
					</ul>
				</div>
				<!-- end sort by -->
			</div>
		</div>
		<div class="compare-product-selection">
			<div class="row">
				<div class="col-md-3">
					<div class="compare-header">how do they compare?</div>
					<div class="small-compare-text">Weigh product features to help decide which item fits you and your little one.</div>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-10-percent no-compare-image image-1">
					<img src="/mwodt-static/assets/images/product-compare-3.jpg" /> <img class="close-compare-image" src="/mwodt-static/assets/images/close-icon-sm.png" />
				</div>
				<div class="col-md-10-percent no-compare-image image-2">
					<img src="/mwodt-static/assets/images/product-compare-3.jpg" /> <img class="close-compare-image" src="/mwodt-static/assets/images/close-icon-sm.png" />
				</div>
				<div class="col-md-10-percent no-compare-image image-3">
					<img src="/mwodt-static/assets/images/product-compare-3.jpg" /> <img class="close-compare-image" src="/mwodt-static/assets/images/close-icon-sm.png" />
				</div>
				<div class="col-md-10-percent no-compare-image image-4">
					<img src="/mwodt-static/assets/images/product-compare-3.jpg" /> <img class="close-compare-image" src="/mwodt-static/assets/images/close-icon-sm.png" />
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-2">
					<button id="compare-products-button" disabled>compare now</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-no-padding">
				<div class="sub-cat-category-1 sub-cat-filter-list hide inline">
					standard-size
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-category-2 sub-cat-filter-list hide inline">
					lightweight
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-category-3 sub-cat-filter-list hide inline">
					jogging
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-category-4 sub-cat-filter-list hide inline">
					double
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-category-5 sub-cat-filter-list hide inline">
					triple
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-category-6 sub-cat-filter-list hide inline">
					standard-size
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-category-7 sub-cat-filter-list hide inline">
					lightweight
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-category-8 sub-cat-filter-list hide inline">
					jogging
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-category-9 sub-cat-filter-list hide inline">
					double
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-category-10 sub-cat-filter-list hide inline">
					triple
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-price-1 sub-cat-filter-list hide inline">
					under $10
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-price-2 sub-cat-filter-list hide inline">
					$10-$20
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-price-3 sub-cat-filter-list hide inline">
					$20-$30
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-price-4 sub-cat-filter-list hide inline">
					$30-$40
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-price-5 sub-cat-filter-list hide inline">
					$40-$50
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-price-6 sub-cat-filter-list hide inline">
					$50-$75
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-price-7 sub-cat-filter-list hide inline">
					$75-$100
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-brand-1 sub-cat-filter-list hide inline">
					GB
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-brand-2 sub-cat-filter-list hide inline">
					Stokke
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-brand-3 sub-cat-filter-list hide inline">
					Bugaboo
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-brand-4 sub-cat-filter-list hide inline">
					Baby
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-brand-5 sub-cat-filter-list hide inline">
					Maclaren
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-color-1 sub-cat-filter-list hide inline">
					red
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-color-2 sub-cat-filter-list hide inline">
					blue
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-color-3 sub-cat-filter-list hide inline">
					green
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-color-4 sub-cat-filter-list hide inline">
					yellow
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-color-5 sub-cat-filter-list hide inline">
					purple
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-maxweight-1 sub-cat-filter-list hide inline">
					under 10 lbs
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-maxweight-2 sub-cat-filter-list hide inline">
					10-20 lbs
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-maxweight-3 sub-cat-filter-list hide inline">
					20-30 lbs
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-maxweight-4 sub-cat-filter-list hide inline">
					30-40 lbs
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-maxweight-5 sub-cat-filter-list hide inline">
					over 40 lbs
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-weight-1 sub-cat-filter-list hide inline">
					under 5 lbs
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-weight-2 sub-cat-filter-list hide inline">
					5-10 lbs
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-weight-3 sub-cat-filter-list hide inline">
					10-20 lbs
					<div class="dismiss-x inline"></div>
				</div>
				<div class="sub-cat-weight-4 sub-cat-filter-list hide inline">
					over 20 lbs
					<div class="dismiss-x inline"></div>
				</div>
				<a href="#" class="sub-cat-more hide">more... <a class="clear-all-filters hide" href="#"> clear all </a></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<hr class="thick-hr" />
			</div>
		</div>
		<div class="compare-overlay-menu">
			<div class="compare-overlay">
				<div class="compare-overlay-locked">
					<div class="row">
						<div class="col-md-3">
							<div class="compare-header">how do they compare?</div>
							<div class="small-compare-text">Weigh product features to help decide which item fits you and your little one.</div>
						</div>
						<div class="col-md-2">
							<img src="toolkit/images/product-compare-3.jpg">
						</div>
						<div class="col-md-2">
							<img src="toolkit/images/product-compare-3.jpg">
						</div>
						<div class="col-md-2">
							<img src="toolkit/images/product-compare-3.jpg">
						</div>
						<div class="col-md-2">
							<img src="toolkit/images/product-compare-3.jpg">
						</div>
						<img class="compare-overlay-close" data-target=".compare-overlay-menu" src="toolkit/images/my-account/close.png">
					</div>
					<hr class="horizontal-divider">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-2">
							<div class="column-header-text">Stokke Crusi Stroller</div>
							<div class="add-to-cart-section">
								<button class="add-to-cart">add to cart</button>
							</div>
							<div class="add-to-registry">
								<div class="inline">add to registry</div>
								<img class="inline" src="toolkit/images/breadcrumb-arrow-right.png">
							</div>
						</div>
						<div class="col-md-2">
							<div class="column-header-text">Stokke Crusi Stroller</div>
							<div class="add-to-cart-section">
								<button class="add-to-cart">add to cart</button>
							</div>
							<div class="add-to-registry">
								<div class="inline">add to registry</div>
								<img class="inline" src="toolkit/images/breadcrumb-arrow-right.png">
							</div>
						</div>
						<div class="col-md-2">
							<div class="column-header-text">Stokke Crusi Stroller</div>
							<div class="add-to-cart-section">
								<button class="add-to-cart">add to cart</button>
							</div>
							<div class="add-to-registry">
								<div class="inline">add to registry</div>
								<img class="inline" src="toolkit/images/breadcrumb-arrow-right.png">
							</div>
						</div>
						<div class="col-md-2">
							<div class="column-header-text">Stokke Crusi Stroller</div>
							<div class="add-to-cart-section">
								<button class="add-to-cart">add to cart</button>
							</div>
							<div class="add-to-registry">
								<div class="inline">add to registry</div>
								<img class="inline" src="toolkit/images/breadcrumb-arrow-right.png">
							</div>
						</div>
					</div>
					<hr>
				</div>
				<div class="compare-overlay-scroll">
					<div class="row">
						<div class="col-md-3">price</div>
						<div class="col-md-2">$1249.99</div>
						<div class="col-md-2">$1249.99</div>
						<div class="col-md-2">$1249.99</div>
						<div class="col-md-2">$1249.99</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-3">type</div>
						<div class="col-md-2">full size</div>
						<div class="col-md-2">full size</div>
						<div class="col-md-2">full size</div>
						<div class="col-md-2">full size</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-3">best uses</div>
						<div class="col-md-2 best-uses">walking, travel, shopping</div>
						<div class="col-md-2 best-uses">walking, travel, shopping</div>
						<div class="col-md-2 best-uses">walking, travel, shopping</div>
						<div class="col-md-2 best-uses">walking, travel, shopping</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-3"># of children capacity</div>
						<div class="col-md-2">1</div>
						<div class="col-md-2">1</div>
						<div class="col-md-2">1</div>
						<div class="col-md-2">1</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-3">car seat compatible</div>
						<div class="col-md-2">yes</div>
						<div class="col-md-2">yes</div>
						<div class="col-md-2">yes</div>
						<div class="col-md-2">yes</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-3">weight</div>
						<div class="col-md-2">
							19 lbs chassis <br> 8 lbs seat
						</div>
						<div class="col-md-2">
							19 lbs chassis <br> 8 lbs seat
						</div>
						<div class="col-md-2">
							19 lbs chassis <br> 8 lbs seat
						</div>
						<div class="col-md-2">
							19 lbs chassis <br> 8 lbs seat
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-3">weight capacity</div>
						<div class="col-md-2">45 lbs</div>
						<div class="col-md-2">45 lbs</div>
						<div class="col-md-2">45 lbs</div>
						<div class="col-md-2">45 lbs</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-3">extras</div>
						<div class="col-md-2 extras">weather protection, reversible handle/seat, adjustable head support, adjustable handle, adjustable canopy</div>
						<div class="col-md-2 extras">weather protection, reversible handle/seat, adjustable head support, adjustable handle, adjustable canopy</div>
						<div class="col-md-2 extras">weather protection, reversible handle/seat, adjustable head support, adjustable handle, adjustable canopy</div>
						<div class="col-md-2 extras">weather protection, reversible handle/seat, adjustable head support, adjustable handle, adjustable canopy</div>
					</div>
				</div>
			</div>
		</div>
		<div class="offcanvas compare-menu sub-cat-container">
			<div class="tse-scrollable filter-scroll-wrapper">
				<div class="tse-content">
					<%-- <c:if test="${not empty contentItem.navigation}">  --%>
					<c:forEach var="element" items="${contentItem.navigation}">
						<dsp:renderContentItem contentItem="${element}" />
					</c:forEach>
					<%--</c:if>  --%>
				</div>
			</div>
		</div>
	</div>
	<!-- this div will be opened inside guidedNavigation.jsp-->
	<div class="sticky-menu-placeholder"></div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/11.1/Storefront/j2ee/store.war/cartridges/GuidedNavigation/GuidedNavigation.jsp#1 $$Change: 875535 $--%>