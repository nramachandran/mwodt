<dsp:page>
	<div class="container-fluid home-template min-width">
		<div class="default-margin">
			<div class="max-width">
				<div class="row row-no-padding">
					<div class="col-md-12 col-no-padding">
						<c:import url="/components/MyStoreHeader.jspf"></c:import>
					</div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>