<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<dsp:page>
    <div class="row-footer-margin">
        <div class="row row-no-padding">
            <div class="col-md-12 col-no-padding">
                <h2 class="now-trending-header">now trending</h2>
            </div>
        </div>
        <div class="row block-2-row">
            <div class="col-md-3 block-2-column"><c:import url="/structures/product-block.jsp"></c:import></div>
            <div class="col-md-3 block-2-column"><c:import url="/structures/product-block.jsp"></c:import></div>
            <div class="col-md-3 block-2-column"><c:import url="/structures/product-block.jsp"></c:import></div>
            <div class="col-md-3 block-2-column"><c:import url="/structures/product-block.jsp"></c:import></div>
        </div>
    </div>
</dsp:page>