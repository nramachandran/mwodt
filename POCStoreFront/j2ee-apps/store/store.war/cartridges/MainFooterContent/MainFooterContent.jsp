<dsp:page>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<div class="footer-grid">
		<div class="footer-images">
			<div class="row row-no-padding">
				<div class="footer-image-row col-md-12">
					<c:import url="/components/footer-image.jspf"></c:import>
				</div>
			</div>
			<div class="default-margin second-footer-list">
				<div class="row row-no-padding">
					<div class="col-md-5ths col-no-padding">
						<c:import url="/components/footer-list-2.jspf"></c:import>
					</div>
					<div class="col-md-5ths col-no-padding">
						<c:import url="/components/footer-list-2.jspf"></c:import>
					</div>
					<div class="col-md-5ths col-no-padding">
						<c:import url="/components/footer-list-2.jspf"></c:import>
					</div>
					<div class="col-md-5ths col-no-padding">
						<c:import url="/components/footer-list-2.jspf"></c:import>
					</div>
					<div class="col-md-5ths col-no-padding">
						<c:import url="/components/sign-up-for-savings.jspf"></c:import>
					</div>
				</div>
			</div>
		</div>

		<div class="row row-no-padding footer-social">
			<div class="col-md-12 col-no-padding">
				<c:import url="/components/social.jspf"></c:import>
			</div>
		</div>

		<div id="footer" class="mwodt-footer">
			<div class="row-footer-margin-1024">
				<div class="row row-no-padding">
					<div class="col-md-7">
						<c:import url="/components/copyright.jspf"></c:import>
					</div>
					<div class="col-md-2 col-no-padding">
						<c:import url="/components/internationalization.jspf"></c:import>
					</div>
					<div class="col-md-2 col-no-padding">
						<c:import url="/components/drop-down.jspf"></c:import>
					</div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>