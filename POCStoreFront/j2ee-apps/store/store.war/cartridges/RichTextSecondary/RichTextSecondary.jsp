<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />

  <dsp:getvalueof var="component"
        vartype="com.endeca.infront.assembler.ContentItem"
        bean="/OriginatingRequest.contentItem" />

	    <c:out value="${component.content}" escapeXml="false" />


</dsp:page>