<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="dsp" uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/multisite/Site" />
	<dsp:importbean bean="/com/store/browse/droplet/ExtractDimensionValueCacheIdDroplet" />
	<dsp:importbean bean="/com/store/browse/droplet/CategoryIdFromDimvalLookupDroplet" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/catalog/CategoryLookup" />

	<dsp:getvalueof var="vContextPath" vartype="java.lang.String" value="${originatingRequest.contextPath}" />
	<dsp:getvalueof var="vProductionUrl" vartype="java.lang.String" bean="Site.productionURL" />

	<%-- Below div is taken from CategorySEOAdSpotBlock.jsp and will be repeating in CategorySubCategoryImageLink.jsp 
	This adjustment is been made to remove the wrapper cartridge CategorySEOAdSpotBlock
	This needs to be watched out for any alignment issue later. As of now it is working fine.
--%>
	<div class="row-max-width">
		<div class="row block-3-row row-strollers" style="display:none;">


			<dsp:droplet name="CategoryIdFromDimvalLookupDroplet">
				<dsp:oparam name="output">
					<dsp:droplet name="CategoryLookup">
						<dsp:param name="id" param="categoryId" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="parentCategoryItem" param="element" />
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>

			<dsp:droplet name="ForEach">
				<dsp:param name="array" value="${parentCategoryItem.childCategories}" />
				<dsp:param name="elementName" value="childcategoryItem" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="vCatName" param="childcategoryItem.displayName" />
					<dsp:getvalueof var="vCatType" param="childcategoryItem.categoryItemType" />
					<dsp:getvalueof var="childCatChildCategories" param="childcategoryItem.childCategories" />
					<dsp:droplet name="ExtractDimensionValueCacheIdDroplet">
						<dsp:param name="categoryId" param="childcategoryItem.id" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="vDimensionValueCacheId" param="dimensionValueCacheId" />
							<div class="col-md-4 block-3-column">
								<div class="category-image-link-block">
									<a href="${vProductionUrl}/browse?N=${vDimensionValueCacheId}"><img src="${vContextPath}/assets/images/product-carousel-image-1.png" alt="category block image"></a>
									<ul class="category-image-link-block-list">
										<li><a href="${vProductionUrl}/browse?N=${vDimensionValueCacheId}">${vCatName}</a></li>
										<c:if test="${ vCatType == 'SUBCATEGORY'}">
											<dsp:include page="/components/category-image-link-block.jspf">
												<dsp:param value="${childCatChildCategories}" name="childCatChildCategories" />
											</dsp:include>
										</c:if>
										<li><a href="#"></a></li>
									</ul>
								</div>
							</div>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>

		</div>
	</div>
</dsp:page>