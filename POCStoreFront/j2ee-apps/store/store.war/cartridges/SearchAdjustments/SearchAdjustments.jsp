<%--
  "Search Adjustments" cartridge renderer.
 
  Required parameters:
    contentItem
      The "ResultsList" content item to render.
--%>
<%@ taglib prefix="dsp" uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1" %>
<dsp:page>
  <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
  <dsp:getvalueof var="contextPath" vartype="java.lang.String" value="${originatingRequest.contextPath}"/>
  <dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>
  <dsp:importbean bean="/atg/multisite/Site" />
  <dsp:getvalueof var="vProductionUrl" vartype="java.lang.String" bean="Site.productionURL" />
 
  
<c:if test="${not empty contentItem.adjustedSearches || not empty contentItem.suggestedSearches}">
      <c:forEach var="originalTerm" items="${contentItem.originalTerms}" varStatus="status">
        <c:if test="${not empty contentItem.suggestedSearches[originalTerm]}">
			<h2 class="search-breadcrumb-block no-results">
            		no results found for '<dsp:valueof param="Ntt"/>'</h2> <br>
          			<p class="search-breadcrumb-block did-you-mean">Did you mean 	 
               			 <c:forEach var="suggestion" items="${contentItem.suggestedSearches[originalTerm]}" varStatus="status"> 
			                      <span><a href="${vProductionUrl}${suggestion.contentPath}${suggestion.navigationState}">${suggestion.label} ?</a></span>
                		</c:forEach>
					</p>
					<hr>
             <c:forEach var="element" items="${contentItem.NoSearchContent}">
             <dsp:renderContentItem contentItem="${element}"/>
           </c:forEach>
        </c:if>
      </c:forEach>
 </c:if> 
  
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/11.1/Storefront/j2ee/store.war/cartridges/SearchAdjustments/SearchAdjustments.jsp#1 $$Change: 875535 $--%>